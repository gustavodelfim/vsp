let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/admin.js', 'public/js')
   .sass('resources/assets/sass/admin/admin.scss', 'public/css/admin')
   .sass('resources/assets/sass/admin/login.scss', 'public/css/admin')
   .sass('resources/assets/sass/app.scss', 'public/css/app')

   .sass('resources/assets/sass/app/sites/crz.scss', 'public/css/app/sites')
   .sass('resources/assets/sass/app/sites/direct.scss', 'public/css/app/sites')
   .sass('resources/assets/sass/app/sites/vibe.scss', 'public/css/app/sites')
   .sass('resources/assets/sass/app/sites/vsp.scss', 'public/css/app/sites');
