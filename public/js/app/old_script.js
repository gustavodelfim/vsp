(function($, window, document) {
    'use strict';
    window.Vsp = {
        template: {}
    };

    $(function() {
        Vsp.template.efeitos();
        Vsp.template.loadBanner();

        if("atendimento" == Vsp.template.currentPage) {
            Vsp.contato.init();
        }

        if("planos" == Vsp.template.currentPage) {
            Vsp.planos.init();
        }

        Vsp.template.modalEscolhaCidade();
    });

    Vsp.template = {
        url: 'http://agenciajungle.com.br/vsp/',
        currentPage: location.pathname.substring(1).split('/')[1], //mudar para 0 quando tirar do /site
        loadBanner: function() {
            imagesLoaded('#banner', function( instance ) {
                Vsp.template.loadPage();
            });
        },
        loadPage: function() {
            $('body').animate({
                opacity: 1
            }, 1000);
        },
        efeitos: function() {
            // $.each($('.menu li a'), function() {
            //     var el = $(this);
            //     var url = el.attr('href').substring(1).split('/')[1];
            //
            //     if(url == Vsp.template.currentPage) {
            //         el.addClass('selected');
            //     } else {
            //         el.removeClass('selected');
            //     }
            // });

            $(".hover-effect:not(.no-hover)").hover(
                function() {
                    TweenMax.to($(this).find("span"), 0.5, {opacity: 1});
                }, function() {
                    TweenMax.to($(this).find("span"), 0.5, {opacity: 0});
                }
            );

            $(".btn-boleto").hover(
                function() {
                    TweenMax.to($(this).find(".hover-effect").find("span"), 0.5, {opacity: 1});
                }, function() {
                    TweenMax.to($(this).find(".hover-effect").find("span"), 0.5, {opacity: 0});
                }
            );
        },
        modalEscolhaCidade: function() {
            $('#escolha-cidade').modal({
                backdrop: 'static',
                show: false,
                keyboard: false
            });

            Vsp.escolhaCidade.init();
        }
    };

    Vsp.escolhaCidade = {
        pathArquivo: 'seleciona-cidade.php',
        checkSession: function() {
            var _dados = { tipo: 'check' };

            $.ajax({
                type: "POST",
                url: Vsp.escolhaCidade.pathArquivo,
                dataType: 'json',
                data: _dados,
                success: function(data) {
                    if (data.result == true) {
                        $('#escolha-cidade').modal('hide');
                    } else {
                        $('#escolha-cidade').modal('show');

                        if('planos' == Vsp.template.currentPage) {
                            $('#velocidade').modal('hide');
                        }
                    }
                }
            });
        },
        send: function() {
            var _element = '#formCidade';
            var _dados = $(_element).serialize();

            $.ajax({
                type: "POST",
                url: Vsp.escolhaCidade.pathArquivo,
                dataType: 'json',
                data: _dados,
                success: function(data) {
                    if (data.result == true) {
                        $('#escolha-cidade').modal('hide');
                    }
                }
            });
        },
        init: function() {
            Vsp.escolhaCidade.checkSession();

            $("#formCidade button[type='button']").on('click', function() {
                Vsp.escolhaCidade.send();
            });
        }
    };

    Vsp.planos = {
        timerStatus: null,
        pathArquivo: Vsp.template.url + "salva_pedido.php",
        element: '#formVelocidade',
        setMasks: function() {
            $('.cep').mask('99999-999');

            $('.phone').focusout(function(){
                var phone, element;
                element = $(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if(phone.length > 10) {
                    element.mask("(99)99999-999?9");
                } else {
                    element.mask("(99)9999-9999?9");
                }
            }).trigger('focusout');
        },
        fadeStatus: function() {
            if (Vsp.planos.timerStatus !== null) {
                $(Vsp.planos.element).find('.status').stop().fadeOut(500);
                Vsp.planos.timerStatus = null;
            }
        },
        send: function() {
            var _dados = $(Vsp.planos.element).serialize();
            $(Vsp.planos.element).find('input').attr('disabled', 'disabled').css('opacity', '0.7');
            $(Vsp.planos.element).find('.status').html('Enviando dados...').stop().fadeIn(500);

            $.ajax({
                type: "POST",
                url: Vsp.planos.pathArquivo,
                dataType: 'json',
                data: _dados,
                success: function(data) {
                    if (data.result == true) {
                        $(Vsp.planos.element).find('.status').html(data.message).stop().fadeIn(500);
                        $(Vsp.planos.element).find('input').removeAttr('disabled').css('opacity', '1');
                        $(Vsp.planos.element).reset();

                        if(data.redirect != undefined) {
                            Vsp.planos.timerStatus = setTimeout(function() {
                                Vsp.planos.fadeStatus();
                                window.location.href = Vsp.template.url + data.redirect;
                            }, 3000);
                        } else {
                            Vsp.planos.timerStatus = setTimeout(Vsp.planos.fadeStatus, 3000);
                        }
                    } else {
                        $(Vsp.planos.element).find('.status').html(data.message).stop().fadeIn(500);
                        $(Vsp.planos.element).find('input').removeAttr('disabled').css('opacity', '1');

                        if(data.redirect != undefined) {
                            Vsp.planos.timerStatus = setTimeout(function() {
                                Vsp.planos.fadeStatus();
                                window.location.href = Vsp.template.url + data.redirect;
                            }, 3000);
                        } else {
                            Vsp.planos.timerStatus = setTimeout(Vsp.planos.fadeStatus, 3000);
                        }
                    }
                }
            });
        },
        modal: function() {
            $('#velocidade').on('show.bs.modal', function(e) {
                Vsp.escolhaCidade.checkSession();

                var id = $(e.relatedTarget).data('id');
                var nome = $(e.relatedTarget).data('nome');
                var planoId = $(e.relatedTarget).data('plano-id');

                $(e.currentTarget).find('input[name="velocidade_id"]').val(id);
                $(e.currentTarget).find('input[name="plano_id"]').val(planoId);
                $(e.currentTarget).find('h1').html(nome);
            });

            $('#velocidade').on('hide.bs.modal', function(e) {
                $(e.currentTarget).find('input[name="id"]').val('');
                $(e.currentTarget).find('h1').html('');
            });
        },
        init: function() {
            Vsp.planos.modal();
            Vsp.planos.setMasks();

            $(Vsp.planos.element).find("button[type='button']").on('click', function() {
                Vsp.planos.send();
            });
        }
    };

    Vsp.contato = {
        timerStatus: null,
        pathArquivo: Vsp.template.url + "envia_email_contato.php",
        setMasks: function() {
            $('.phone').focusout(function(){
                var phone, element;
                element = $(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if(phone.length > 10) {
                    element.mask("(99)99999-999?9");
                } else {
                    element.mask("(99)9999-9999?9");
                }
            }).trigger('focusout');
        },
        fadeStatus: function() {
            if (Vsp.contato.timerStatus !== null) {
                $('.status').stop().fadeOut(500);
                Vsp.contato.timerStatus = null;
            }
        },
        send: function() {
            var _element = '#formContato';
            var _dados = $(_element).serialize();
            $(_element).find('input').attr('disabled', 'disabled').css('opacity', '0.7');
            $('.status').html('Enviando mensagem...').stop().fadeIn(500);

            $.ajax({
                type: "POST",
                url: Vsp.contato.pathArquivo,
                dataType: 'json',
                data: _dados,
                success: function(data) {
                    if (data.result == true) {
                        $('.status').html(data.message).stop().fadeIn(500);
                        $(_element).find('input').removeAttr('disabled').css('opacity', '1');
                        $(_element).reset();

                        if(data.redirect != undefined) {
                            Vsp.contato.timerStatus = setTimeout(function() {
                                Vsp.contato.fadeStatus();
                                window.location.href = Vsp.template.url + data.redirect;
                            }, 3000);
                        } else {
                            Vsp.contato.timerStatus = setTimeout(Vsp.contato.fadeStatus, 3000);
                        }
                    } else {
                        $('.status').html(data.message).stop().fadeIn(500);
                        $(_element).find('input').removeAttr('disabled').css('opacity', '1');
                        grecaptcha.reset();

                        if(data.redirect != undefined) {
                            Vsp.contato.timerStatus = setTimeout(function() {
                                Vsp.contato.fadeStatus();
                                window.location.href = Vsp.template.url + data.redirect;
                            }, 3000);
                        } else {
                            Vsp.contato.timerStatus = setTimeout(Vsp.contato.fadeStatus, 3000);
                        }
                    }
                }
            });
        },
        init: function() {
            Vsp.contato.setMasks();

            $("#formContato button[type='button']").on('click', function() {
                Vsp.contato.send();
            });
        }
    };

}(window.jQuery, window, document));
