var Faq = {
  init: function() {
  },
  pergunta: {
    click: function (link) {
      link = $(link);
      var cont = link.parent();

      var data = cont.data('active');
      if (data == 'no-active') {
        Faq.pergunta.show(cont)
      } else {
        Faq.pergunta.hide()
      }
    },
    show: function (cont) {
      Faq.pergunta.hide();
      cont.data('active', 'active')
      cont.addClass('active')
      var height = cont.find('.height').height()
      var info = cont.find('.info')
      info.animate({
        height: height
      }, 200, function(){
        info.css({
          height: 'auto',
          overflow: 'visible'
        })
      })
    },
    hide: function () {
      var cont = $('.faq .cont');
      cont.data('active', 'no-active')
      cont.removeClass('active');
      var info = cont.find('.info')
      info.animate({
        height: 0
      }, 200, function(){
        info.css({
          overflow: 'hidden'
        })
      })
    }
  },
}

Main.init();
