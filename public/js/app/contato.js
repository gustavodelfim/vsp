Contato = {
  init: function () {

  },

  submit: function (form) {
    var self = this;
    form = $(form);

    var action = form.attr('action')
    $.ajax({
      url: action,
      type: 'POST',
      data: {
        _token: form.find('input[name="_token"]').val(),
        nome: form.find('input[name="nome"]').val(),
        sobrenome: form.find('input[name="sobrenome"]').val(),
        email: form.find('input[name="email"]').val(),
        telefone: form.find('input[name="telefone"]').val(),
        assunto: form.find('input[name="assunto"]').val(),
        mensagem: form.find('textarea[name="mensagem"]').val()
      },
      beforeSend: function () {
        form.find('button[type="submit"]').html('Enviando...').attr('disabled', true)
      },
      success: function(response) {
        if (response.success) {
          swal("Parabens!", "Deu tudo certo, logo entramos em contato com você", "success");
        } else {
          swal("Opps!", "Aconteceu algo de errado, tente mais tarde", "error");
        }
        self.clearInputs(form)
      },
      error: function (response) {
        swal("Opps!", "Aconteceu algo de errado, tente mais tarde", "error");
        self.clearInputs(form)
      }
    })
    return false;
  },
  clearInputs: function (form) {
    form.find('input[name="nome"]').val('')
    form.find('input[name="sobrenome"]').val('')
    form.find('input[name="email"]').val('')
    form.find('input[name="telefone"]').val('')
    form.find('input[name="assunto"]').val('')
    form.find('input[name="mensagem"]').val('')
    form.find('button[type="submit"]').html('ENVIAR MENSAGEM').attr('disabled', false)
  }
}

Contato.init();
