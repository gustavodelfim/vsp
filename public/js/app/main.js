var Main = {
  init: function() {
  },
  menu: {
    resize: function () {
      var _win = $(window);
      _win.resize(function(){
        if (Modernizr.mq('only all and (min-width: 576px)')) {
          Main.menu.hide()
        }
      }).trigger('resize')
    },
    click: function (link) {
      link = $(link);
      var nav = $('#header .nav');
      var data = nav.data('active');
      if (data == 'no-active') {
        Main.menu.show(nav)
      } else {
        Main.menu.hide()
      }
    },
    show: function (nav) {
      nav.data('active', 'active')
      nav.addClass('active')
      $('.load-menu').fadeIn(300);
    },
    hide: function () {
      var nav = $('#header .nav');
      nav.data('active', 'no-active')
      nav.removeClass('active');
      var load = $('.load-menu');
      load.fadeOut(300, function(){
        load.hide()
      });
    }
  },
}

Main.init();
Main.menu.resize();
