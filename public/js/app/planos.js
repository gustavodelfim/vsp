var Planos = {
  init: function () {
    $('[data-toggle="tooltip"]').tooltip();

    var slide = $('.slide-plano')

    slide.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: false,
      focusOnSelect: true,
      slidesToShow: 3,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            centerMode: true
          }
        }
      ]
    });

    var arrows = $('#planos .slide .arrows')
    arrows.on('click', function() {
      var data = $(this).data('arrow')
      if (data == 'next') {
        slide.slick('slickPrev')
      } else {
        slide.slick('slickNext')
      }
    })

  },

  sinais: {
    all: $('.sinais'),
    menos: $('.sinais.menos'),
    mais: $('.sinais.mais')
  },
  inputsPackages: $('input.packages'),
  packages: {},
  lastPlan: {},

  startPackages: function () {
    var self = this;
    this.getPackages();

    this.sinais.all.on('click', function() {
      var step = $(this).data('step');
      step = step.split(',')
      step[0] = parseInt(step[0])
      step[1] = parseInt(step[1])
      var parent = $(this).parent();
      var package = self.getPackage(step[0]);

      var plan = self.getPlan(package, step[1]);

      var sinal = parseInt( $(this).data('sinal') );
      if (typeof plan != 'undefined'){
        self.setDatas(parent, 'step', step, sinal)

        if (self.lastPlan[step[0]] == step[1]){
          var stringSinal = 'menor';
          if (sinal == 1) stringSinal = 'maior';
          // swal("Este é o "+ stringSinal +" plano", "");
          self.maxMb(step[0]);
        }

        if(typeof self.lastPlan[step[0]] == 'undefined') {

          // se nao existir historico e o primeiro click for na
          // posiçao zero, quer dizer que ele clicou no menos.
          if (step[1] == 0){
            // swal("Este é o maior plano", "");
            self.maxMb(step[0]);
          }

          self.lastPlan[step[0]] = step[1];
          self.setPlan(plan, step[0]);
        } else {
          if(self.lastPlan[step[0]] != step[1]) {
            self.lastPlan[step[0]] = step[1];
            self.setPlan(plan, step[0]);
          }
        }

      }


    })
  },
  getPackage: function(index) {
    return this.packages[index];
  },
  getPlan: function (package, planIndex) {
    return package.plansDesc[planIndex];
  },
  setDatas: function(element, data, value, sinal) {
    var self = this;
    var status = false;

    if (sinal == 1) {
      var plans = self.packages[value[0]].plansDesc;
      if (plans.length > value[1]+1 ) {
        var mais = value[1] + 1;
        var menos = value[1];
        status = true;
      }
    } else {
      if (value[1] > 0) {
        var mais = value[1];
        var menos = value[1] - 1;
        status = true;
      }
    }

    var btSolicitar = element.parent().find('.botao a')
    btSolicitar.data('step', value[0]+','+value[1])
    // console.log( btSolicitar )

    if (status) {
      var stringMais = value[0]+','+mais;
      var stringMenos = value[0]+','+menos;
      element.find('.mais').data(data, stringMais)
      element.find('.menos').data(data, stringMenos)
    }
  },
  maxMb: function (packageIndex) {
    var elemPackage = this.getElementPackage(packageIndex)
    var mb = elemPackage.find('.mb');
    mb.addClass('tremer')
      .addClass("tremer").delay(300).queue(function(next){
        $(this).removeClass("tremer");
        next();
    });
  },
  setPlan: function (plan, packageIndex) {
    var elemPackage = this.getElementPackage(packageIndex)

    elemPackage.find('.mb .price .number').html( plan.tablePrice.mb )
    elemPackage.find('.desc-price span').html( plan.tablePrice.price.replace('.', ',') )

    elemPackage.find('.infos .download i').html( plan.tablePrice.download + plan.tablePrice.desc_download )
    elemPackage.find('.infos .upload i').html( plan.tablePrice.upload + plan.tablePrice.desc_upload )
    elemPackage.find('.infos .limit b').html( plan.tablePrice.limit )

  },
  getElementPackage: function (index) {
    return $('#planos .plano[data-index-package="'+index+'"]');
  },

  getPackages: function () {
    var self = this;
    $.each(this.inputsPackages, function(index, value){
      self.packages[index] = JSON.parse($(value).val());
    })
  },

  solicitar: function (elem) {
    var self = this
    step = $(elem).data('step')
    step = step.split(',')
    step[0] = parseInt(step[0])
    step[1] = parseInt(step[1])

    var package = self.getPackage(step[0]);
    var plan = self.getPlan(package, step[1]);

    var elemPackage = this.getElementPackage(step[0])

    $('.modal-title').html('Solicitar plano de '+plan.tablePrice.mb+'MB')
    $('.plan_id').val(plan.id)
    $('.mb_day').val(plan.tablePrice.mb)
    $('.price_day').val(plan.tablePrice.price)
    $('.description_day').val( elemPackage.find('.infos').html() )
    $('.title_package_day').val(package.title)

    self.modal.show();

  },
  modal: {
    elem: $('.modal-planos'),
    show: function () {
      this.elem.modal('show')
    },
    hide: function () {
      this.elem.modal('hide')
    }
  },

  submit: function (form) {
    var self = this;
    form = $(form);

    var action = form.attr('action')
    $.ajax({
      url: action,
      type: 'POST',
      data: {
        _token: form.find('input[name="_token"]').val(),
        plan_id: form.find('input[name="plan_id"]').val(),
        name: form.find('input[name="name"]').val(),
        email: form.find('input[name="email"]').val(),
        phone: form.find('input[name="phone"]').val(),
        cep: form.find('input[name="cep"]').val(),
        mb_day: form.find('input[name="mb_day"]').val(),
        price_day: form.find('input[name="price_day"]').val(),
        description_day: form.find('input[name="description_day"]').val(),
        title_package_day: form.find('input[name="title_package_day"]').val()
      },
      beforeSend: function () {
        form.find('button[type="submit"]').html('Solicitando...').attr('disabled', true)
      },
      success: function(response) {
        if (response.success) {
          swal("Parabens!", "Deu tudo certo, logo entramos em contato com você", "success");
        } else {
          swal("Opps!", "Aconteceu algo de errado, tente mais tarde", "error");
        }
        self.modal.hide();
        self.clearInputs(form)
      },
      error: function (response) {
        swal("Opps!", "Aconteceu algo de errado, tente mais tarde", "error");
        self.modal.hide();
        self.clearInputs(form)
      }
    })
    return false;
  },
  clearInputs: function (form) {
    form.find('input[name="plan_id"]').val('')
    form.find('input[name="name"]').val('')
    form.find('input[name="email"]').val('')
    form.find('input[name="phone"]').val('')
    form.find('input[name="cep"]').val('')
    form.find('input[name="mb_day"]').val('')
    form.find('input[name="price_day"]').val('')
    form.find('input[name="description_day"]').val('')
    form.find('input[name="title_package_day"]').val('')
    form.find('button[type="submit"]').html('Solicitar').attr('disabled', false)
  }
}

Planos.init();
Planos.startPackages();
