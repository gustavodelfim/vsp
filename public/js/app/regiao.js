var Regiao = {
  init: function () {

    $(".chosen-select").chosen();

    var form = $("#form-regiao");

    form.find('select').on('change', function() {
        form.submit();
    })

  },
  fecharChamada: function () {
    var emissao = $('#regiao .emissao')
    emissao.fadeOut(300, function(){
      emissao.hide()
    })
  }
}

$(document).ready(function(){
  Regiao.init();
})
