var Home = {
  init: function () {
    this.banner.active();
  },
  banner: {
    active: function () {
      var pub = $('.banner .pub');
      var h2 = pub.find('h2');

      pub.addClass('active');
    }
  }
};

$(document).ready( Home.init() );
