-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 04-Set-2018 às 00:48
-- Versão do servidor: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vsp`
--
CREATE DATABASE IF NOT EXISTS `vsp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `vsp`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cities_site_id_foreign` (`site_id`),
  KEY `cities_state_id_foreign` (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cities`
--

INSERT INTO `cities` (`id`, `site_id`, `name`, `phone`, `state_id`, `status`, `created_at`, `updated_at`) VALUES
(14, 4, 'Cianorte', '(44) 99861-6032', 16, 1, '2018-05-01 06:07:51', '2018-05-02 19:38:30'),
(15, 4, 'Japurá', '(44) 99861-6032', 16, 1, '2018-05-02 19:42:08', '2018-05-02 19:42:08'),
(16, 4, 'São Tomé', '(44) 99861-6032', 16, 1, '2018-05-02 19:42:56', '2018-05-02 19:42:56'),
(17, 4, 'Indianópolis', '(44) 99861-6032', 16, 1, '2018-05-02 19:43:52', '2018-05-02 19:43:52'),
(18, 4, 'São Manoel do Paraná', '(44) 99861-6032', 16, 1, '2018-05-02 19:44:39', '2018-05-02 19:44:39'),
(19, 4, 'Vidigal', '(44) 99861-6032', 16, 1, '2018-05-02 19:46:14', '2018-05-02 19:46:14'),
(20, 4, 'São Lourenço', '(44) 99861-6032', 16, 1, '2018-05-02 19:46:41', '2018-05-02 19:46:41'),
(21, 4, 'Rondon', '(44) 99861-6032', 16, 1, '2018-05-02 19:47:30', '2018-05-02 19:47:30'),
(22, 4, 'Tapejara', '(44) 99861-6032', 16, 1, '2018-05-02 19:47:56', '2018-05-02 19:47:56'),
(23, 4, 'Ubiratã', '(44) 99861-6032', 16, 1, '2018-05-02 19:48:57', '2018-05-02 19:48:57'),
(24, 3, 'Campo Magro', '(41) 30120-002', 16, 1, '2018-05-08 19:32:09', '2018-05-08 19:32:09'),
(25, 3, 'Campo Largo', '(41) 30120-002', 16, 1, '2018-07-10 01:24:40', '2018-07-10 01:24:40'),
(26, 3, 'Balsa Nova', '(41) 30120-002', 16, 1, '2018-07-10 01:26:22', '2018-07-10 01:26:22'),
(27, 1, 'Maringá', '(44) 98839-7997', 16, 1, '2018-09-04 03:45:13', '2018-09-04 03:45:13');

-- --------------------------------------------------------

--
-- Estrutura da tabela `configurations`
--

DROP TABLE IF EXISTS `configurations`;
CREATE TABLE IF NOT EXISTS `configurations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` int(10) UNSIGNED NOT NULL,
  `logo_top` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_footer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `neighborhood` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(10) UNSIGNED NOT NULL,
  `institutional_texto1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `institutional_texto2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `institutional_texto3` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `configurations_site_id_foreign` (`site_id`),
  KEY `configurations_state_id_foreign` (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `configurations`
--

INSERT INTO `configurations` (`id`, `site_id`, `logo_top`, `logo_footer`, `email_contact`, `phone`, `cep`, `street`, `number`, `neighborhood`, `city`, `state_id`, `institutional_texto1`, `institutional_texto2`, `institutional_texto3`, `created_at`, `updated_at`) VALUES
(1, 1, 'uploads/admin/logos/LvUekj2QCIGaHLsIkLl3OWz4bBojh6mTzMXt6FMG.png', 'uploads/admin/logos/V4KJvb2bW8YdsKmMCDSXoW5aQdSxIWFZVLY7JGBM.png', 'contato@vspinternet.com', '(67) 33410-333', '79.050-112', 'Rua Quintino Bocaiúva', '1050', 'Jd. Paulista', 'Campo Grande', 12, 'A VSP INTERNET surgiu no início de 1999 da necessidade tecnológica do mercado, que buscava soluções acessíveis e proﬁssionais em acesso à Internet. A partir da aquisição de um pequeno provedor de acesso, uma equipe de grande experiência foi destacada para montar um sistema de parceria com empresas locais e que ao mesmo tempo pudesse concorrer com as grandes redes de provedores do Brasil.', 'O sucesso dependia de trabalho, e a nova equipe experimentou um dos maiores crescimentos já constatados em todo o mercado Internet, alcançando vários pontos de presença em apenas dois meses. A partir de então, foram adquiridos outros provedores e agregados mais proﬁssionais, que concluíram todo o conjunto de serviços que hoje a Vibe Internet pode oferecer, serviços estes que estão sendo lançados ao mesmo tempo senão antes mesmo de seus grandes concorrentes nacionais.', 'São números, fatos e sucesso conseguidos não só pelo ideal empreendedor da Vibe, mas também pela excelente equipe de proﬁssionais que a empresa possui não só em sua Central de Atendimento, mas em todos os parceiros locais. Trata-se de um quadro de proﬁssionais altamente qualiﬁcados e certiﬁcados. Além disso, o modelo de gestão setorizado possibilitou independência e trouxe resultados em números e produção.', '2018-02-01 23:12:37', '2018-02-01 23:12:37'),
(2, 2, 'uploads/admin/logos/logo2-top.png', 'uploads/admin/logos/logo2-footer.png', 'contato@vspinternet.com', '(67) 33410-333', '79.050-112', 'Rua Quintino Bocaiúva', '1050', 'Jd. Paulista', 'Campo Grande', 12, 'A VSP INTERNET surgiu no início de 1999 da necessidade tecnológica do mercado, que buscava soluções acessíveis e proﬁssionais em acesso à Internet. A partir da aquisição de um pequeno provedor de acesso, uma equipe de grande experiência foi destacada para montar um sistema de parceria com empresas locais e que ao mesmo tempo pudesse concorrer com as grandes redes de provedores do Brasil.', 'O sucesso dependia de trabalho, e a nova equipe experimentou um dos maiores crescimentos já constatados em todo o mercado Internet, alcançando vários pontos de presença em apenas dois meses. A partir de então, foram adquiridos outros provedores e agregados mais proﬁssionais, que concluíram todo o conjunto de serviços que hoje a Vibe Internet pode oferecer, serviços estes que estão sendo lançados ao mesmo tempo senão antes mesmo de seus grandes concorrentes nacionais.', 'São números, fatos e sucesso conseguidos não só pelo ideal empreendedor da Vibe, mas também pela excelente equipe de proﬁssionais que a empresa possui não só em sua Central de Atendimento, mas em todos os parceiros locais. Trata-se de um quadro de proﬁssionais altamente qualiﬁcados e certiﬁcados. Além disso, o modelo de gestão setorizado possibilitou independência e trouxe resultados em números e produção.', '2018-02-01 23:12:37', '2018-02-01 23:12:37'),
(3, 3, 'uploads/admin/logos/logo3-top.png', 'uploads/admin/logos/logo3-footer.png', 'contato@directwifi.com.br', '(41) 3012-0002', '83.535-000', 'Rua Rosa Sedoski Valenga', '62', 'Jd. Novos Horizontes', 'Campo Magro', 16, 'A DIRECT WIFI TELECOM surgiu no início de 2011 da necessidade tecnológica do mercado, que buscava soluções acessíveis e proﬁssionais em acesso à Internet. A partir da aquisição de um pequeno provedor de acesso, uma equipe de grande experiência foi destacada para montar um sistema de parceria com empresas locais e que ao mesmo tempo pudesse concorrer com as grandes redes de provedores do Brasil.', 'O sucesso dependia de trabalho, e a nova equipe experimentou um dos maiores crescimentos já constatados em todo o mercado Internet, alcançando vários pontos de presença em apenas dois meses. A partir de então, foram adquiridos outros provedores e agregados mais proﬁssionais, que concluíram todo o conjunto de serviços que hoje a Vibe Internet pode oferecer, serviços estes que estão sendo lançados ao mesmo tempo senão antes mesmo de seus grandes concorrentes nacionais.', 'São números, fatos e sucesso conseguidos não só pelo ideal empreendedor da Vibe, mas também pela excelente equipe de proﬁssionais que a empresa possui não só em sua Central de Atendimento, mas em todos os parceiros locais. Trata-se de um quadro de proﬁssionais altamente qualiﬁcados e certiﬁcados. Além disso, o modelo de gestão setorizado possibilitou independência e trouxe resultados em números e produção.', '2018-02-01 23:12:37', '2018-07-10 00:29:29'),
(4, 4, 'uploads/admin/logos/61uZvnhBy28n0bNiLIW0DHGYbMRsvGTEowFaM10s.png', 'uploads/admin/logos/logo4-footer.png', 'contato@vibenet.com.br', '(44) 3631-9188', '87.200-332', 'Rua Curitiba', '754', 'Zona 02', 'Campo Grande', 16, 'A ViIBE INTERNET surgiu no início de 2010, inicialmente com o nome fantasia de RAIDNET, surgiu da necessidade tecnológica do mercado, que buscava soluções acessíveis e proﬁssionais em acesso à Internet. A partir da aquisição de um pequeno provedor de acesso, uma equipe de grande experiência foi destacada para montar um sistema de parceria com empresas locais e que ao mesmo tempo pudesse concorrer com as grandes redes de provedores do Brasil.', 'O sucesso dependia de trabalho, e a nova equipe experimentou um dos maiores crescimentos já constatados em todo o mercado Internet, alcançando vários pontos de presença em apenas dois meses. A partir de então, foram adquiridos outros provedores e agregados mais proﬁssionais, que concluíram todo o conjunto de serviços que hoje a Vibe Internet pode oferecer, serviços estes que estão sendo lançados ao mesmo tempo senão antes mesmo de seus grandes concorrentes nacionais.', 'São números, fatos e sucesso conseguidos não só pelo ideal empreendedor da Vibe, mas também pela excelente equipe de proﬁssionais que a empresa possui não só em sua Central de Atendimento, mas em todos os parceiros locais. Trata-se de um quadro de proﬁssionais altamente qualiﬁcados e certiﬁcados. Além disso, o modelo de gestão setorizado possibilitou independência e trouxe resultados em números e produção.', '2018-02-01 23:12:38', '2018-07-30 21:56:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `matriz`
--

DROP TABLE IF EXISTS `matriz`;
CREATE TABLE IF NOT EXISTS `matriz` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `neighborhood` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uf` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `matriz_site_id_foreign` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `matriz`
--

INSERT INTO `matriz` (`id`, `site_id`, `phone`, `cep`, `street`, `number`, `neighborhood`, `city`, `uf`, `created_at`, `updated_at`) VALUES
(1, 1, '44988325611', '88034040', 'Teste', '123', '123', 'Maringá', 'PR', '2018-02-01 23:12:38', '2018-02-01 23:12:38'),
(2, 2, '44988325611', '88034040', 'Teste', '123', '123', 'Maringá', 'PR', '2018-02-01 23:12:38', '2018-02-01 23:12:38'),
(3, 3, '44988325611', '88034040', 'Teste', '123', '123', 'Maringá', 'PR', '2018-02-01 23:12:38', '2018-02-01 23:12:38'),
(4, 4, '44988325611', '88034040', 'Teste', '123', '123', 'Maringá', 'PR', '2018-02-01 23:12:38', '2018-02-01 23:12:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_09_02_022629_create_states_table', 1),
(2, '2014_10_01_100000_create_role_table', 1),
(3, '2014_10_02_100000_create_sites_table', 1),
(4, '2014_10_03_100000_create_users_table', 1),
(5, '2014_10_04_100000_create_configurations_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2017_11_23_1_create_packages_table', 1),
(8, '2017_11_23_2_create_cities_table', 1),
(9, '2017_11_23_3_create_plans_table', 1),
(10, '2017_11_23_4_create_plans_cities_table', 1),
(11, '2018_01_31_023137_create_matriz_table', 1),
(12, '2018_02_01_010512_create_solicitations_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `packages`
--

INSERT INTO `packages` (`id`, `title`, `description`, `created_at`, `updated_at`, `site_id`) VALUES
(1, 'Residencial', 'Navegue a vontade pela internet, sem precisar se preocupar com o limite da sua franquia. Assista vídeos, ouça músicas, faça download, pesquisas e muito mais!', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 1),
(2, 'Empresarial', 'Tenha total segurança em um serviço de internet que possui preço justo, que é veloz, ilimitado e conta com rápida manutenção.', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 1),
(3, 'Rural', 'Seja no campo ou na cidade, vamos até você! Na VSP não há desculpas, não há limite de dados e nem barreiras territoriais.', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 1),
(4, 'Residencial', 'Navegue a vontade pela internet, sem precisar se preocupar com o limite da sua franquia. Assista vídeos, ouça músicas, faça download, pesquisas e muito mais!', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 2),
(5, 'Empresarial', 'Tenha total segurança em um serviço de internet que possui preço justo, que é veloz, ilimitado e conta com rápida manutenção.', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 2),
(6, 'Rural', 'Seja no campo ou na cidade, vamos até você! Na CRZ não há desculpas, não há limite de dados e nem barreiras territoriais.', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 2),
(7, 'Residencial', 'Navegue a vontade pela internet, sem precisar se preocupar com o limite da sua franquia. Assista vídeos, ouça músicas, faça download, pesquisas e muito mais!', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 3),
(8, 'Empresarial', 'Tenha total segurança em um serviço de internet que possui preço justo, que é veloz, ilimitado e conta com rápida manutenção.', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 3),
(9, 'Rural', 'Seja no campo ou na cidade, vamos até você! Na Direct não há desculpas, não há limite de dados e nem barreiras territoriais.', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 3),
(10, 'Residencial', 'Navegue a vontade pela internet, sem precisar se preocupar com o limite da sua franquia. Assista vídeos, ouça músicas, faça download, pesquisas e muito mais!', '2018-04-29 17:06:33', '2018-04-29 17:06:33', 4),
(11, 'Empresarial', 'Tenha total segurança em um serviço de internet que possui preço justo, que é veloz, ilimitado e conta com rápida manutenção.', '2018-04-29 17:06:34', '2018-04-29 17:06:34', 4),
(12, 'Rural', 'Seja no campo ou na cidade, vamos até você! Na Vibe não há desculpas, não há limite de dados e nem barreiras territoriais.', '2018-04-29 17:06:34', '2018-04-29 17:06:34', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `plans`
--

DROP TABLE IF EXISTS `plans`;
CREATE TABLE IF NOT EXISTS `plans` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `package_id` int(10) UNSIGNED NOT NULL,
  `mb` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `rates_installation` decimal(5,2) NOT NULL,
  `installation_wifi` decimal(5,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  `upload` int(11) DEFAULT NULL,
  `limit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_download` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc_upload` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `plans_package_id_foreign` (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `plans`
--

INSERT INTO `plans` (`id`, `package_id`, `mb`, `price`, `rates_installation`, `installation_wifi`, `created_at`, `updated_at`, `download`, `upload`, `limit`, `desc_download`, `desc_upload`) VALUES
(74, 1, '1', '69.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:41:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(75, 1, '2', '89.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(76, 1, '3', '99.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(77, 1, '4', '129.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(78, 1, '5', '149.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(79, 2, '3', '129.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(80, 2, '4', '149.90', '99.90', '179.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(81, 3, '1', '99.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(82, 3, '2', '119.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(83, 4, '1', '69.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(84, 4, '2', '89.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(85, 4, '3', '99.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(86, 4, '4', '129.90', '99.90', '150.00', '2018-04-29 17:21:20', '2018-04-29 17:21:20', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(87, 4, '5', '149.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(88, 5, '3', '129.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(89, 5, '4', '149.90', '99.90', '179.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(90, 6, '1', '99.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(91, 6, '2', '119.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(92, 7, '10', '89.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-05-08 19:53:09', 10, 5, 'Sem limite', 'Mbps', 'Mbps'),
(93, 7, '15', '99.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-05-08 19:53:09', 15, 7, 'Sem limite', 'Mbps', 'Mbps'),
(94, 7, '20', '109.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-05-08 19:53:09', 20, 10, 'Sem limite', 'Mbps', 'Mbps'),
(95, 7, '30', '129.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-05-08 19:53:09', 30, 15, 'Sem limite', 'Mbps', 'Mbps'),
(96, 7, '50', '199.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-05-08 19:53:09', 50, 25, 'Sem limite', 'Mbps', 'Mbps'),
(97, 8, '3', '129.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(98, 8, '4', '149.90', '99.90', '179.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(99, 9, '5', '99.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-05-08 19:53:09', 5, 1, 'Sem limite', 'Mbps', 'Mbps'),
(100, 9, '10', '199.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-05-08 19:53:09', 10, 2, 'Sem limite', 'Mbps', 'Mbps'),
(101, 10, '1', '69.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-05-01 06:08:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(102, 10, '2', '89.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(103, 10, '3', '99.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(104, 10, '4', '129.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(105, 10, '5', '149.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(106, 11, '3', '129.90', '99.90', '150.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(107, 11, '4', '149.90', '99.90', '179.00', '2018-04-29 17:21:21', '2018-04-29 17:21:21', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(108, 12, '1', '99.90', '99.90', '150.00', '2018-04-29 17:21:22', '2018-04-29 17:21:22', 1, 1, 'Sem limite', 'Mbps', 'Mbps'),
(109, 12, '2', '119.90', '99.90', '150.00', '2018-04-29 17:21:22', '2018-04-29 17:21:22', 1, 1, 'Sem limite', 'Mbps', 'Mbps');

-- --------------------------------------------------------

--
-- Estrutura da tabela `plans_cities`
--

DROP TABLE IF EXISTS `plans_cities`;
CREATE TABLE IF NOT EXISTS `plans_cities` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plan_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `download` int(11) NOT NULL,
  `upload` int(11) NOT NULL,
  `limit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_download` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_upload` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plans_cities_plan_id_foreign` (`plan_id`),
  KEY `plans_cities_city_id_foreign` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `plans_cities`
--

INSERT INTO `plans_cities` (`id`, `plan_id`, `city_id`, `price`, `status`, `created_at`, `updated_at`, `download`, `upload`, `limit`, `desc_download`, `desc_upload`, `mb`) VALUES
(73, 101, 14, '49.90', 1, NULL, '2018-05-02 19:38:30', 2, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(74, 102, 14, '69.90', 1, NULL, '2018-05-02 19:38:30', 5, 2, 'Sem limite', 'Mbps', 'Mbps', 5),
(75, 103, 14, '89.90', 1, NULL, '2018-05-02 19:38:30', 8, 4, 'Sem limite', 'Mbps', 'Mbps', 8),
(76, 104, 14, '129.90', 0, NULL, '2018-05-02 19:38:30', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(77, 105, 14, '149.90', 0, NULL, '2018-05-02 19:38:30', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(78, 106, 14, '99.90', 1, NULL, '2018-05-02 19:38:30', 10, 5, 'Sem limite', 'Mbps', 'Mbps', 10),
(79, 107, 14, '109.90', 1, NULL, '2018-05-02 19:38:30', 15, 7, 'Sem limite', 'Mbps', 'Mbps', 15),
(80, 108, 14, '99.90', 1, NULL, '2018-05-02 19:38:30', 2, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(81, 109, 14, '149.90', 1, NULL, '2018-05-02 19:38:30', 5, 2, 'Sem limite', 'Mbps', 'Mbps', 5),
(82, 101, 15, '69.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(83, 102, 15, '89.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(84, 103, 15, '99.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(85, 104, 15, '129.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(86, 105, 15, '149.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(87, 106, 15, '129.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(88, 107, 15, '149.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(89, 108, 15, '99.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(90, 109, 15, '119.90', 1, NULL, '2018-05-02 19:42:18', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(91, 101, 16, '69.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(92, 102, 16, '89.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(93, 103, 16, '99.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(94, 104, 16, '129.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(95, 105, 16, '149.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(96, 106, 16, '129.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(97, 107, 16, '149.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(98, 108, 16, '99.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(99, 109, 16, '119.90', 1, NULL, '2018-05-02 19:43:01', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(100, 101, 17, '69.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(101, 102, 17, '89.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(102, 103, 17, '99.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(103, 104, 17, '129.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(104, 105, 17, '149.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(105, 106, 17, '129.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(106, 107, 17, '149.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(107, 108, 17, '99.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(108, 109, 17, '119.90', 1, NULL, '2018-05-02 19:43:58', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(109, 101, 18, '69.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(110, 102, 18, '89.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(111, 103, 18, '99.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(112, 104, 18, '129.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(113, 105, 18, '149.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(114, 106, 18, '129.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(115, 107, 18, '149.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(116, 108, 18, '99.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(117, 109, 18, '119.90', 1, NULL, '2018-05-02 19:44:44', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(118, 101, 19, '69.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(119, 102, 19, '89.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(120, 103, 19, '99.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(121, 104, 19, '129.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(122, 105, 19, '149.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(123, 106, 19, '129.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(124, 107, 19, '149.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(125, 108, 19, '99.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(126, 109, 19, '119.90', 1, NULL, '2018-05-02 19:46:19', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(127, 101, 20, '69.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(128, 102, 20, '89.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(129, 103, 20, '99.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(130, 104, 20, '129.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(131, 105, 20, '149.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(132, 106, 20, '129.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(133, 107, 20, '149.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(134, 108, 20, '99.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(135, 109, 20, '119.90', 1, NULL, '2018-05-02 19:46:47', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(136, 101, 21, '69.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(137, 102, 21, '89.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(138, 103, 21, '99.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(139, 104, 21, '129.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(140, 105, 21, '149.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(141, 106, 21, '129.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(142, 107, 21, '149.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(143, 108, 21, '99.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(144, 109, 21, '119.90', 1, NULL, '2018-05-02 19:47:35', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(145, 101, 22, '69.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(146, 102, 22, '89.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(147, 103, 22, '99.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(148, 104, 22, '129.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(149, 105, 22, '149.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(150, 106, 22, '129.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(151, 107, 22, '149.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(152, 108, 22, '99.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(153, 109, 22, '119.90', 1, NULL, '2018-05-02 19:48:14', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(154, 101, 23, '69.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(155, 102, 23, '89.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(156, 103, 23, '99.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(157, 104, 23, '129.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(158, 105, 23, '149.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(159, 106, 23, '129.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(160, 107, 23, '149.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(161, 108, 23, '99.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(162, 109, 23, '119.90', 1, NULL, '2018-05-02 19:49:05', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(163, 92, 24, '89.90', 1, NULL, '2018-05-08 19:50:05', 10, 5, 'Sem limite', 'Mbps', 'Mbps', 10),
(164, 93, 24, '99.90', 1, NULL, '2018-05-08 19:50:05', 15, 7, 'Sem limite', 'Mbps', 'Mbps', 15),
(165, 94, 24, '109.90', 1, NULL, '2018-05-08 19:50:05', 20, 10, 'Sem limite', 'Mbps', 'Mbps', 20),
(166, 95, 24, '129.90', 1, NULL, '2018-05-08 19:50:05', 30, 15, 'Sem limite', 'Mbps', 'Mbps', 30),
(167, 96, 24, '199.90', 1, NULL, '2018-05-08 19:50:05', 50, 25, 'Sem limite', 'Mbps', 'Mbps', 50),
(168, 97, 24, '129.90', 0, NULL, '2018-05-08 19:53:09', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(169, 98, 24, '149.90', 0, NULL, '2018-05-08 19:53:09', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(170, 99, 24, '99.90', 1, NULL, '2018-05-08 19:50:06', 5, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(171, 100, 24, '199.90', 1, NULL, '2018-05-08 19:50:06', 10, 2, 'Sem limite', 'Mbps', 'Mbps', 10),
(172, 92, 25, '89.90', 1, NULL, '2018-07-10 01:25:02', 10, 5, 'Sem limite', 'Mbps', 'Mbps', 10),
(173, 93, 25, '99.90', 1, NULL, '2018-07-10 01:25:02', 15, 7, 'Sem limite', 'Mbps', 'Mbps', 15),
(174, 94, 25, '109.90', 1, NULL, '2018-07-10 01:25:02', 20, 10, 'Sem limite', 'Mbps', 'Mbps', 20),
(175, 95, 25, '129.90', 1, NULL, '2018-07-10 01:25:02', 30, 15, 'Sem limite', 'Mbps', 'Mbps', 30),
(176, 96, 25, '199.90', 1, NULL, '2018-07-10 01:25:02', 50, 25, 'Sem limite', 'Mbps', 'Mbps', 50),
(177, 97, 25, '129.90', 1, NULL, '2018-07-10 01:25:02', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(178, 98, 25, '149.90', 1, NULL, '2018-07-10 01:25:02', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(179, 99, 25, '99.90', 1, NULL, '2018-07-10 01:25:02', 5, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(180, 100, 25, '199.90', 1, NULL, '2018-07-10 01:25:02', 10, 2, 'Sem limite', 'Mbps', 'Mbps', 10),
(181, 92, 26, '89.90', 1, NULL, '2018-07-10 01:26:36', 10, 5, 'Sem limite', 'Mbps', 'Mbps', 10),
(182, 93, 26, '99.90', 1, NULL, '2018-07-10 01:26:36', 15, 7, 'Sem limite', 'Mbps', 'Mbps', 15),
(183, 94, 26, '109.90', 1, NULL, '2018-07-10 01:26:36', 20, 10, 'Sem limite', 'Mbps', 'Mbps', 20),
(184, 95, 26, '129.90', 1, NULL, '2018-07-10 01:26:36', 30, 15, 'Sem limite', 'Mbps', 'Mbps', 30),
(185, 96, 26, '199.90', 1, NULL, '2018-07-10 01:26:36', 50, 25, 'Sem limite', 'Mbps', 'Mbps', 50),
(186, 97, 26, '129.90', 0, NULL, '2018-07-10 01:26:36', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(187, 98, 26, '149.90', 0, NULL, '2018-07-10 01:26:36', 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(188, 99, 26, '99.90', 0, NULL, '2018-07-10 01:26:36', 5, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(189, 100, 26, '199.90', 0, NULL, '2018-07-10 01:26:36', 10, 2, 'Sem limite', 'Mbps', 'Mbps', 10),
(190, 74, 27, '69.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(191, 75, 27, '89.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2),
(192, 76, 27, '99.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(193, 77, 27, '129.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(194, 78, 27, '149.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 5),
(195, 79, 27, '129.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 3),
(196, 80, 27, '149.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 4),
(197, 81, 27, '99.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 1),
(198, 82, 27, '119.90', 1, NULL, NULL, 1, 1, 'Sem limite', 'Mbps', 'Mbps', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `role`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Administrador Master', 'administrator', 'Administrador Geral', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(2, 'Colaborador', 'collaborator', 'Gerenciador dos Sites', '2018-02-01 23:12:36', '2018-02-01 23:12:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sites`
--

DROP TABLE IF EXISTS `sites`;
CREATE TABLE IF NOT EXISTS `sites` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `domain` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `sites`
--

INSERT INTO `sites` (`id`, `domain`, `title`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'VSP', 'VSP', 'vsp', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(2, 'crz', 'CRZ', 'crz', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(3, 'direct', 'Direct Internet', 'direct', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(4, 'vibenet', 'Vibe Internet', 'vibe', '2018-02-01 23:12:36', '2018-02-01 23:12:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitations`
--

DROP TABLE IF EXISTS `solicitations`;
CREATE TABLE IF NOT EXISTS `solicitations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plan_id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `city_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mb_day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_day` decimal(5,2) NOT NULL,
  `description_day` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_package_day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0 - Cancelado, 1 - Espera, 2 - Fechou negocio',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `solicitations_site_id_foreign` (`site_id`),
  KEY `solicitations_city_id_foreign` (`city_id`),
  KEY `solicitations_plan_id_foreign` (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `solicitations`
--

INSERT INTO `solicitations` (`id`, `plan_id`, `site_id`, `city_id`, `name`, `email`, `phone`, `cep`, `mb_day`, `price_day`, `description_day`, `title_package_day`, `status`, `created_at`, `updated_at`) VALUES
(2, 92, 3, 24, 'Andre', 'andre_nill@live.com', '(41) 99538-6460', '83.508-446', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-09 23:42:35', '2018-07-11 19:03:00'),
(3, 92, 3, 24, 'Andre', 'andre_nill@live.com', '(41) 99538-6460', '83.508-446', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-09 23:43:36', '2018-07-11 19:03:10'),
(4, 92, 3, 24, 'João Henrique Cordeiro', 'j7henrique@outlook.com', '(41) 99999-9999', '83.606-177', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-11 02:55:12', '2018-07-11 19:02:40'),
(5, 92, 3, 24, 'João Henrique Cordeiro', 'j7henrique@outlook.com', '(41) 99890-4819', '83.606-177', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-11 02:56:00', '2018-07-11 19:02:32'),
(6, 92, 3, 24, 'Joao Henrique Cordeiro', 'j7henrique@outlook.com', '(41) 99890-4819', '83.606-177', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-11 02:57:07', '2018-07-11 19:05:01'),
(7, 92, 3, 24, 'Cristiane Maria Burda Nalepa', 'cristianeburda51@gmail.com', '(41) 98506-6437', '83.632-010', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-11 03:13:21', '2018-07-11 19:03:27'),
(8, 101, 4, 14, 'Carla Silva', 'cacajpjc@gmail.com', '(44) 99969-2979', '87.205-040', '2', '49.90', '<p class=\"download\"><span>Download</span> de <i>2Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-11 23:15:32', '2018-07-11 19:04:23'),
(9, 92, 3, 24, 'Lucas anotonio da cruz bonfim', 'wartlucas@hotmail.com', '(41) 99853-55203', '83.648-991', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-12 23:47:57', '2018-07-11 19:03:51'),
(10, 92, 3, 24, 'Daise Prodo', 'Daiseprodo@gmail.com', '(41) 99154-5856', '83.604-516', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-14 05:56:18', '2018-07-11 19:34:55'),
(11, 92, 3, 24, 'Daise Prodo', 'Daiseprodo@gmail.com', '(41) 99154-5856', '83.604-516', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-14 05:57:05', '2018-07-11 19:34:42'),
(12, 92, 3, 24, 'Daise Prodo', 'Daiseprodo@gmail.com', '(41) 99154-5856', '83.604-516', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-14 05:57:55', '2018-07-11 19:21:23'),
(13, 92, 3, 24, 'Daise Prodo', 'daiseprodo@gmail.com', '(41) 99154-5856', '83.604-516', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-14 06:01:40', '2018-07-11 19:21:13'),
(14, 92, 3, 24, 'Daise Prodo', 'daiseprodo@gmail.com', '(41) 99154-5856', '83.604-516', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-14 06:02:55', '2018-07-11 19:21:02'),
(15, 92, 3, 24, 'Murilo Cequinel Soares', 'murilocequinel@hotmail.com', '(41) 99803-2848', '83.608-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-14 06:28:52', '2018-07-11 19:35:03'),
(16, 92, 3, 24, 'Murilo Cequinel Soares', 'murilocequinel@hotmail.com', '(41) 99803-2848', '83.608-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-14 06:29:07', '2018-07-11 19:35:13'),
(17, 92, 3, 24, 'Leonardo Lunardon', 'leo.lunardon2@gmail.com', '(41) 99720-2244', '83.606-176', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-14 07:15:00', '2018-05-14 07:15:00'),
(18, 92, 3, 24, 'Leonardo Lunardon', 'leo.lunardon2@gmail.com', '(41) 99720-2244', '83.606-176', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-14 07:15:26', '2018-05-14 07:15:26'),
(19, 92, 3, 24, 'Carolina Miguel Yasbeck Ferrari', 'carolyasbeck@hotmail.com', '(41) 36371-018', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-14 20:49:26', '2018-05-14 20:49:26'),
(20, 92, 3, 24, 'Karina Aparecida bonato', 'karynabonato@hotmail.com', '(41) 99612-5707', '8', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-15 00:52:41', '2018-05-15 00:52:41'),
(21, 92, 3, 24, 'Sonia Mara Pereira de Jesus', 'andri_ele1012@outlook.com', '(41) 99564-1587', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-15 07:35:55', '2018-05-15 07:35:55'),
(22, 94, 3, 24, 'Marcus R A Veloso', 'marcusrav@bol.com.br', '(41) 99654-9527', '83.605-310', '20', '109.90', '<p class=\"download\"><span>Download</span> de <i>20Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>10Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-15 09:41:57', '2018-05-15 09:41:57'),
(23, 93, 3, 24, 'Marcus R A Veloso', 'marcusrav@bol.com.br', '(41) 99654-9527', '83.605-310', '15', '99.90', '<p class=\"download\"><span>Download</span> de <i>15Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>7Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-15 09:42:25', '2018-05-15 09:42:25'),
(24, 92, 3, 24, 'Marcus R A Veloso', 'marcusrav@bol.com.br', '(41) 99654-9527', '83.605-310', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-15 09:42:45', '2018-05-15 09:42:45'),
(25, 92, 3, 24, 'Angela Aparecida de Paula Soares', 'angelaapdepaulasoaressoares@gmail.com', '(41) 99630-9824', '83.605-320', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-15 22:17:17', '2018-05-15 22:17:17'),
(26, 92, 3, 24, 'FERNANDO APARECIDO CAMARA', 'fernandossuel@gmail.com', '(41) 99957-9506', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-16 02:18:56', '2018-07-11 19:19:50'),
(27, 106, 4, 14, 'Luis Fernando Rabelo', 'gerencia.adm@safeworksst.com.br', '(45) 99977-9174', '87.207-034', '10', '99.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Empresarial', '2', '2018-05-16 02:52:27', '2018-07-11 19:19:39'),
(28, 92, 3, 24, 'Jocemar Bentaque dos reis', 'jocemarbentak@gmail.com', '(41) 98823-7404', '83.606-484', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-16 06:35:05', '2018-07-11 19:19:30'),
(29, 92, 3, 24, 'Teste', 'gusttavodelfim@gmail.com', '(54) 46546-56456', '87.103-310', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-17 02:31:01', '2018-07-11 19:19:17'),
(30, 92, 3, 24, 'Elaine Lopes de Souza', 'elainenay@hotmail.com', '(44) 99860-3815', '83.835-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-17 02:48:37', '2018-07-11 19:19:04'),
(31, 92, 3, 24, 'Daise Prodo', 'daiseprodo@gmail.com', '(41) 99154-5856', '83.604-516', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-17 04:23:57', '2018-07-11 19:14:57'),
(32, 92, 3, 24, 'LEONARDO CARLOS DA SILVA ROCHA', 'leonardo.primetech@gmail.com', '(21) 99936-9046', '83.601-500', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-17 07:13:09', '2018-07-11 19:11:09'),
(33, 92, 3, 24, 'Danilo Algauer', 'daniloalg1983@gmail.com.br', '(41) 98420-2176', '83.605-355', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-17 23:55:52', '2018-07-11 19:10:35'),
(34, 92, 3, 24, 'Reinaldo Bueno Carvalho', 'mpmpadilha@gmail.com', '(41) 98848-6308', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-18 18:18:24', '2018-07-11 19:10:12'),
(35, 92, 3, 24, 'Lucas Krupniski', 'Lucas_krupniski@hotmail.com', '(41) 99578-0583', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-05-18 23:27:47', '2018-07-11 19:09:34'),
(36, 92, 3, 24, 'Solange do rocio machado da silva', 'solangesol@gmail.com', '(90) 85027-033', '83.605-570', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-19 00:43:16', '2018-05-19 00:43:16'),
(37, 103, 4, 14, 'Gabriel', 'Gabriel70_soares10@hotmail.com', '(44) 99863-7109', '87.213-000', '8', '89.90', '<p class=\"download\"><span>Download</span> de <i>8Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>4Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-19 10:15:04', '2018-05-19 10:15:04'),
(38, 92, 3, 24, 'Ronaldo', 'ronaldo.raizer@gmail.com', '(41) 99649-9483', '83.410-030', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-19 22:17:16', '2018-05-19 22:17:16'),
(39, 92, 3, 24, 'Rodrigo kossovski', 'rodrigokossovski@gmail.com', '(41) 98808-7151', '83.608-355', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-21 05:48:24', '2018-05-21 05:48:24'),
(40, 92, 3, 24, 'Alisson Assis', 'alissonassis19973@gmail.com', '(41) 99258-8133', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-21 19:12:28', '2018-05-21 19:12:28'),
(41, 92, 3, 24, 'Adriane Silverio', 'adrianesilverio22@gmail.com', '(41) 99701-2037', '83.604-516', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-21 21:52:49', '2018-05-21 21:52:49'),
(42, 92, 3, 24, 'Vinícius', 'vinicius.alfanio@hotmail.com', '(41) 99822-2956', '83.602-394', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-22 05:44:10', '2018-05-22 05:44:10'),
(43, 92, 3, 24, 'Abinael Soares de Oliveira', 'abinael7@yahoo.com.br', '(41) 99624-2761', '83.605-070', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-22 05:47:04', '2018-05-22 05:47:04'),
(44, 92, 3, 24, 'Fernando Souza', 'fernando-souz-2@hotmail.com', '(41) 98832-0093', '83.605-352', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-22 07:21:36', '2018-05-22 07:21:36'),
(45, 92, 3, 24, 'Andreia', 'andreia.2502@gmail.com', '(41) 99676-7080', '83.605-670', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-22 07:49:38', '2018-05-22 07:49:38'),
(46, 92, 3, 24, 'Gerson', 'ge_sarnecki@hotmail.com', '(41) 33924-417', '83.606-177', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-22 18:32:42', '2018-05-22 18:32:42'),
(47, 106, 4, 14, 'Bruna Carla pizani', 'brunacarlapizaniii@gmail.com', '(44) 99839-3893', '87.210-176', '10', '99.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Empresarial', '1', '2018-05-22 20:25:21', '2018-05-22 20:25:21'),
(48, 92, 3, 24, 'Maria Ferreira dos Santos', 'hpicussa@yahoo.com.br', '(41) 99888-682', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-22 22:23:51', '2018-05-22 22:23:51'),
(49, 92, 3, 24, 'Rodrigo Duran', 'rodrigoduran1984@hotmail.com', '(41) 99623-7952', '83.605-110', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-23 06:02:50', '2018-05-23 06:02:50'),
(50, 92, 3, 24, 'Thyago Luiz Brich', 'thyagobrich@yahoo.com.br', '(55) 41995-52375', '83.605-040', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-23 07:06:48', '2018-05-23 07:06:48'),
(51, 92, 3, 24, 'Rodrigo Duran', 'rodrigoduran1984@hotmail.com', '(41) 98485-2535', '83.605-110', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-25 07:37:11', '2018-05-25 07:37:11'),
(52, 92, 3, 24, 'Rosangela das graças cordeiro', 'lagoaelange@hotmail.com', '(41) 99760-1278', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-26 04:14:28', '2018-05-26 04:14:28'),
(53, 106, 4, 14, 'Ricardo Moro', 'guto_moro@hotmail.com', '(44) 99103-2021', '87.200-159', '10', '99.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Empresarial', '1', '2018-05-26 22:14:16', '2018-05-26 22:14:16'),
(54, 105, 4, 20, 'Gabriel Soares De Santana', 'Gabriel70_soares10@hotmail.com', '(44) 99863-7109', '87.213-000', '5', '149.90', '<p class=\"download\"><span>Download</span> de <i>1Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-29 00:31:50', '2018-05-29 00:31:50'),
(55, 92, 3, 24, 'DEnis', 'vidrosrioverde@gmail.com', '(41) 98482-5094', '83.603-170', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-29 02:27:12', '2018-05-29 02:27:12'),
(56, 109, 4, 14, 'karla pelincel', 'karla_pelincel@outlook.com', '(44) 98839-5050', '87.200-000', '5', '149.90', '<p class=\"download\"><span>Download</span> de <i>5Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>2Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Rural', '1', '2018-05-29 03:48:28', '2018-05-29 03:48:28'),
(57, 106, 4, 14, 'Celso Nakama Jr', 'celso.nakama@gtfoods.com.br', '(44) 99119-5215', '87.200-970', '10', '99.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Empresarial', '1', '2018-05-30 18:22:54', '2018-05-30 18:22:54'),
(58, 92, 3, 24, 'caroline', 'furmancarol3@gmail.com', '(41) 98486-9993', '83.606-490', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-05-31 05:58:01', '2018-05-31 05:58:01'),
(59, 92, 3, 24, 'Jean', 'jeanbrdubstrap@gmail.com', '(41) 99760-3880', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-01 04:12:16', '2018-06-01 04:12:16'),
(60, 92, 3, 24, 'Cícero de Jesus Alves', 'cicero.dejesusalves@gmail.com', '(41) 99527-0784', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-02 21:10:20', '2018-06-02 21:10:20'),
(61, 101, 4, 14, 'Wagner', 'wfacina@gmail.com', '(44) 99965-9774', '87.200-091', '2', '49.90', '<p class=\"download\"><span>Download</span> de <i>2Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-03 20:12:46', '2018-06-03 20:12:46'),
(62, 92, 3, 24, 'Diego', 'dilui2009@gmail.com', '(41) 99540-6768', '83.648-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-04 05:15:54', '2018-06-04 05:15:54'),
(63, 93, 3, 24, 'SABRINA DE ANDRADE SILVA', 'sabrinadeandradesilva@yahoo.com.br', '(41) 33672-075', '82.535-000', '15', '99.90', '<p class=\"download\"><span>Download</span> de <i>15Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>7Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-04 23:01:00', '2018-06-04 23:01:00'),
(64, 92, 3, 24, 'Valdemir', 'mirbieda@hotmail.com', '(41) 99209-9082', '83.607-443', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-05 07:44:54', '2018-06-05 07:44:54'),
(65, 92, 3, 24, 'Rogerio zapchon', 'rogerio_266@hotmail.com', '(41) 99760-6174', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-05 21:30:25', '2018-06-05 21:30:25'),
(66, 101, 4, 15, 'miriam ferroni', 'jmiriamferroni@gmail.com', '(44) 99705-0502', '87.225-000', '1', '69.90', '<p class=\"download\"><span>Download</span> de <i>1Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-05 23:00:22', '2018-06-05 23:00:22'),
(67, 92, 3, 24, 'PAULO HENRIQUE VIEIRA DE SOUZA', 'paulohenrique_vieiras@live.com', '(41) 99954-2206', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-06 01:31:44', '2018-06-06 01:31:44'),
(68, 92, 3, 24, 'Jayro', 'balbinojayro@hotmail.com', '(41) 99664-0858', '83.602-035', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-06 03:33:58', '2018-06-06 03:33:58'),
(69, 92, 3, 24, 'Roger Guilherme Siqueira', 'roger11111987@hotmail.com', '(41) 99910-6497', '83.603-050', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-06 06:03:46', '2018-06-06 06:03:46'),
(70, 92, 3, 24, 'Barbara Helbing', 'bhelbing@gmail.com', '(41) 99683-5897', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-08 02:18:53', '2018-06-08 02:18:53'),
(71, 92, 3, 24, 'Barbara Helbing', 'bhelbing@gmail.com', '(41) 99683-5897', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-08 02:24:41', '2018-06-08 02:24:41'),
(72, 92, 3, 24, 'Barbara Helbing', 'bhelbing@gmail.com', '(41) 99683-5897', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-08 02:26:01', '2018-06-08 02:26:01'),
(73, 92, 3, 24, 'Rodrigo kossovski', 'rodrigokossovski@gmail.com', '(41) 98808-7151', '83.608-355', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-08 05:17:35', '2018-06-08 05:17:35'),
(74, 92, 3, 24, 'Angelo Vinicius da Costa e Silva', 'angelobaike2010@hotmail.com', '(41) 36771-885', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-08 05:53:00', '2018-07-11 19:23:20'),
(75, 92, 3, 24, 'Elisete Dantas de souza nascimento', 'zetedantas.ed@gmail.com', '(36) 37150-7', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-09 01:29:11', '2018-06-09 01:29:11'),
(76, 92, 3, 24, 'Elisete Dantas de souza nascimento', 'zetedantas.ed@gmail.com', '(41) 36371-507', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-09 01:31:33', '2018-06-09 01:31:33'),
(77, 96, 3, 24, 'Alexandre Ribas', 'techwheelbr@gmail.com', '(41) 98893-6319', '83.601-140', '50', '199.90', '<p class=\"download\"><span>Download</span> de <i>50Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>25Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-09 01:41:27', '2018-06-09 01:41:27'),
(78, 92, 3, 24, 'Flávio Pereira marra', 'flpr19782012@gmail.com', '(41) 98448-5543', '83.605-440', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-11 03:04:47', '2018-06-11 03:04:47'),
(79, 93, 3, 24, 'vania bastos', 'vaniaaparecidabastos@gmail.com', '(41) 99706-5126', '83.605-000', '15', '99.90', '<p class=\"download\"><span>Download</span> de <i>15Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>7Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-11 19:07:21', '2018-06-11 19:07:21'),
(80, 93, 3, 24, 'Thayna Tienne Oliveira', 'brizollathayna@gmail.com', '(41) 98722-4954', '82.400-396', '15', '99.90', '<p class=\"download\"><span>Download</span> de <i>15Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>7Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-11 22:59:00', '2018-06-11 22:59:00'),
(81, 92, 3, 24, 'Adam cezar stella', 'acscoxadoido@gmail.com', '(41) 99635-1149', '82.400-406', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-12 06:32:42', '2018-06-12 06:32:42'),
(82, 92, 3, 24, 'Fábio Junior primon', 'primom.jr.fabio@gmail.com', '(41) 99725-2983', '83.605-540', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-12 08:16:28', '2018-06-12 08:16:28'),
(83, 93, 3, 24, 'Roberto Ramos neto', 'roberto.ramos101@hotmail.com', '(41) 99659-5798', '83.604-090', '15', '99.90', '<p class=\"download\"><span>Download</span> de <i>15Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>7Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-13 22:46:24', '2018-06-13 22:46:24'),
(84, 92, 3, 24, 'ricardo luiz de oliveira e silva', 'MATENG@MATENGENGENHARIA.COM.BR', '(41) 33164-162', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-15 00:05:51', '2018-06-15 00:05:51'),
(85, 92, 3, 24, 'Luiz Felipe da costa Ercole', 'costa_ercole@hotmail.com', '(41) 99840-6418', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-15 02:40:51', '2018-06-15 02:40:51'),
(86, 92, 3, 24, 'Thyago Luiz Brich', 'thyagobrich@yahoo.com.br', '(41) 99552-3754', '83.605-040', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-15 03:25:55', '2018-06-15 03:25:55'),
(87, 92, 3, 24, 'Vinícius Henrique Alfanio', 'vinicius.alfanio@hotmail.com', '(41) 99822-2956', '83.602-394', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-16 21:57:32', '2018-06-16 21:57:32'),
(88, 92, 3, 24, 'Eloise Leal', 'eloiseleal35@gmail.com', '(41) 98726-6911', '83.606-596', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-17 00:00:58', '2018-06-17 00:00:58'),
(89, 92, 3, 24, 'Alan stedile', 'duarte.s.vanessa@gmail.com', '(41) 99819-1014', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-06-17 05:10:12', '2018-06-17 05:10:12'),
(90, 92, 3, 24, 'thais de almeida matos correia', 'thaismatoscorreia94@gmail.com', '(41) 36481-289', '83.648-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-18 18:29:22', '2018-07-24 21:43:48'),
(91, 92, 3, 24, 'Ruthnyck W. S. Pinheiro', 'nyck_pinheiro@hotmail.com', '(41) 98786-3167', '83.602-722', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-18 23:26:56', '2018-07-24 21:37:26'),
(92, 92, 3, 24, 'Rafael Cristiano Andrade', 'rafaellandrade09@gmail.com', '(41) 36361-965', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-18 23:49:35', '2018-07-24 21:37:16'),
(93, 92, 3, 24, 'Barbara Helbing', 'bhelbing@gmail.com', '(41) 99683-5897', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-19 19:51:22', '2018-07-24 21:37:07'),
(94, 92, 3, 24, 'Cícero Pedro de Barros', 'ciceropb58@hotmail.com', '(41) 32926-084', '83.602-722', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-20 05:56:47', '2018-07-24 21:36:56'),
(95, 92, 3, 24, 'Adriano Souza Leal', 'adrianoleal20@gmail.com', '(41) 99655-5177', '83.604-330', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-20 06:39:36', '2018-07-24 21:36:47'),
(96, 92, 3, 24, 'Edson Coelho', 'edsoncoelho201@gmail.com', '(41) 99741-8202', '83.605-350', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-21 00:35:32', '2018-07-24 21:36:37'),
(97, 101, 4, 14, 'Gabriela Kepe de Souza', 'gabrielakepe@hotmail.com', '(44) 99743-9252', '87.205-238', '2', '49.90', '<p class=\"download\"><span>Download</span> de <i>2Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-21 02:34:44', '2018-07-24 21:36:28'),
(98, 92, 3, 24, 'Eder Luís de Oliveira', 'eder-oliveira82@hotmail.com', '(41) 98405-6686', '83.602-220', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-22 03:44:54', '2018-07-24 21:36:18'),
(99, 92, 3, 24, 'Enrique de oliveira marques', 'oliveiraenrique128@gmail.com', '(41) 36366-938', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-24 05:26:59', '2018-07-24 21:36:07'),
(100, 92, 3, 24, 'ROBERTA LINGNER ROSA', 'robertalingnersmart@gmail.com', '(41) 99857-1435', '83.605-238', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-25 07:33:25', '2018-07-24 21:35:58'),
(101, 92, 3, 24, 'Elaine Cristina Alves Machado', 'elaineec20904@gmail.com', '(41) 99736-0511', '81.280-032', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-25 20:19:46', '2018-07-24 21:35:26'),
(102, 92, 3, 24, 'Nilson Ricardo Opata', 'ricardoopata_9@hotmail.com', '(41) 99654-1234', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-25 22:30:03', '2018-07-24 21:35:17'),
(103, 92, 3, 24, 'Thaile Castro pinto', 'thailecastro@gmail.com', '(41) 99609-3749', '83.608-040', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-26 01:09:36', '2018-07-24 21:35:07'),
(104, 101, 4, 14, 'Lailton Antônio Clemente', 'lailtoneana@gmail.com', '(44) 99753-7893', '87.208-126', '2', '49.90', '<p class=\"download\"><span>Download</span> de <i>2Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-26 05:27:07', '2018-07-24 21:34:56'),
(105, 92, 3, 24, 'Luan Gustavo', 'luan.gus@hotmail.com', '(41) 98473-2766', '83.606-177', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-26 08:42:34', '2018-07-24 21:34:48'),
(106, 95, 3, 24, 'Luan Gustavo', 'luan.gus@hotmail.com', '(41) 98473-2766', '83.606-177', '30', '129.90', '<p class=\"download\"><span>Download</span> de <i>30Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>15Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-26 08:44:01', '2018-07-24 21:34:40'),
(107, 92, 3, 24, 'ESTEFANNY CAROLINE DE MORAES RAMOS', 'estefannyclramos@gmail.com', '(41) 98500-5243', '83.604-970', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-26 18:34:15', '2018-07-24 21:34:28'),
(108, 93, 3, 24, 'REGINALDO SCHMIDT', 'pretoo1974@gmail.com', '(41) 99644-4063', '83.604-170', '15', '99.90', '<p class=\"download\"><span>Download</span> de <i>15Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>7Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-26 21:04:48', '2018-07-24 21:34:19'),
(109, 92, 3, 24, 'Elaine Cristina Alves Machado', 'elaineec20904@gmail.com', '(41) 99736-0511', '81.280-032', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-26 21:19:55', '2018-07-24 21:34:11'),
(110, 92, 3, 24, 'valéria', 'valeria.lara.mary@gmail.com', '(41) 99800-5027', '83.508-460', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-26 23:19:12', '2018-07-24 21:33:59'),
(111, 92, 3, 24, 'Iolanda kuas', 'ioiolanda2110@gmail.com', '(41) 98713-9559', '82.010-510', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-27 04:41:48', '2018-07-24 21:33:42'),
(112, 94, 3, 24, 'elissandro chiepko', 'elissandrochiepkobueno@gmail.com', '(41) 99188-7028', '83.602-037', '20', '109.90', '<p class=\"download\"><span>Download</span> de <i>20Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>10Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-27 20:30:25', '2018-07-24 21:33:34'),
(113, 92, 3, 24, 'Rodilso halat', 'halaturturismo@live.com', '(41) 98809-2808', '83.605-342', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-27 21:14:56', '2018-07-24 21:33:22'),
(114, 92, 3, 24, 'sidnei pedro machado luz', 'sidnunes2020@gmail.com', '(41) 99650-0644', '83.648-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-28 01:56:57', '2018-07-24 21:33:14'),
(115, 101, 4, 14, 'Gabriela Kepe de Souza', 'gabrielakepe@hotmail.com', '(44) 99743-9252', '87.205-238', '2', '49.90', '<p class=\"download\"><span>Download</span> de <i>2Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-30 03:38:23', '2018-07-24 21:33:05'),
(116, 92, 3, 24, 'Heron Petla', 'heronpetla@gmail.com', '(41) 99899-3160', '83.604-410', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-06-30 21:39:10', '2018-07-24 21:32:50'),
(117, 92, 3, 24, 'Maurílio Alves dos Santos', 'mauriliosantos079@gmail.com', '(41) 99549-1501', '83.648-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-02 23:38:50', '2018-07-24 21:32:43'),
(118, 92, 3, 24, 'Simone', 'sim_teixeira@hotmail.com', '(99) 97948-75', '83.605-670', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-03 04:29:37', '2018-07-24 21:32:36');
INSERT INTO `solicitations` (`id`, `plan_id`, `site_id`, `city_id`, `name`, `email`, `phone`, `cep`, `mb_day`, `price_day`, `description_day`, `title_package_day`, `status`, `created_at`, `updated_at`) VALUES
(119, 92, 3, 24, 'Samara', 'sasaesoso58@gmail.com', '(41) 99876-5877', '83.603-125', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-03 07:36:29', '2018-07-24 21:32:27'),
(120, 92, 3, 24, 'Matheus Eduardo Ferreira', 'bio.edumath@gmail.com', '(41) 99566-4500', '83.605-230', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-03 18:36:44', '2018-07-24 21:32:17'),
(121, 92, 3, 24, 'maristela barboza neves', 'maristela-1981@hotmail.com', '(41) 99871-3266', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-04 03:35:43', '2018-07-24 21:32:09'),
(122, 95, 3, 24, 'Rose Katy Dias Egea', 'Katy22egea@gmail.com', '(41) 99930-1846', '83.604-434', '30', '129.90', '<p class=\"download\"><span>Download</span> de <i>30Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>15Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-04 18:46:02', '2018-07-24 21:32:01'),
(123, 102, 4, 15, 'Luiz Fernando Piccioli', 'nandopiccioli@hotmail.com', '(44) 99977-9253', '87.225-000', '2', '89.90', '<p class=\"download\"><span>Download</span> de <i>1Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-05 01:48:53', '2018-07-24 21:10:16'),
(124, 92, 3, 24, 'Raphael', 'raphael_menezes01@live.com', '(41) 99152-7964', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-05 04:15:13', '2018-07-24 21:10:02'),
(125, 92, 3, 24, 'Adriel', 'adrielsouza1@hotmail.com', '(41) 99743-0151', '83.609-530', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-05 05:37:03', '2018-07-24 21:09:55'),
(126, 92, 3, 24, 'rogerio', 'traticoski@hotmail.com', '(41) 99118-1949', '81.460-350', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-05 20:29:13', '2018-07-24 21:09:47'),
(127, 92, 3, 24, 'Algacir Genari', 'algacir.g@gmail.com', '(41) 99948-1803', '83.607-332', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-06 03:50:27', '2018-07-24 21:09:39'),
(128, 92, 3, 24, 'Amanda Milena Ramos de Oliveira', 'amandamilena13@hotmail.com', '(41) 99758-2236', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-06 18:12:08', '2018-07-24 21:09:33'),
(129, 92, 3, 24, 'Otávio de matos leão', 'otavio_matos1995@hotmail.com', '(41) 98761-2028', '83.606-142', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-07 06:25:11', '2018-07-24 21:09:26'),
(130, 92, 3, 24, 'Marilia', 'mariliaherreroa@hotmail.com', '(41) 99838-7961', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-09 01:07:48', '2018-07-24 21:09:18'),
(131, 92, 3, 24, 'Liliano Fernandes', 'liko.liliano@gmail.com', '(41) 99523-4955', '83.706-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-09 03:39:57', '2018-07-24 21:09:10'),
(132, 92, 3, 24, 'Gustavo', 'gustavo.pinto93@gmail.com', '(41) 99957-7129', '83.602-535', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-09 07:18:34', '2018-07-24 21:09:02'),
(133, 92, 3, 24, 'Adriano', 'digoargl@gmail.com', '(47) 99242-7088', '83.535-974', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-09 22:48:04', '2018-07-24 21:08:54'),
(134, 92, 3, 24, 'Samuel Ricardo Machado', 'samuelrido@gmail.com', '(41) 99906-9687', '86.348-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-10 00:14:29', '2018-07-10 02:28:09'),
(135, 99, 3, 25, 'Samuel Ricardo Machado', 'samuelrido@gmail.com', '(41) 99906-9687', '83.648-000', '5', '99.90', '<p class=\"download\"><span>Download</span> de <i>5Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Rural', '2', '2018-07-10 02:41:03', '2018-07-11 18:59:55'),
(136, 97, 3, 25, 'SANDRA MARIA SILVA DE LIMA', 'sandra-slima@hotmail.com', '(41) 99555-3868', '83.601-070', '3', '129.90', '<p class=\"download\"><span>Download</span> de <i>1Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Empresarial', '2', '2018-07-11 19:27:46', '2018-07-24 21:08:39'),
(137, 99, 3, 25, 'Rafael Biernaski', 'Rafaelbiernaski@gmail.com', '(41) 99131-4277', '83.608-010', '5', '99.90', '<p class=\"download\"><span>Download</span> de <i>5Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Rural', '2', '2018-07-11 22:13:14', '2018-07-24 21:08:28'),
(138, 92, 3, 25, 'Camila Leal Dombroski', 'mila_dombroski@hotmail.com', '(41) 98786-4019', '83.604-644', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-11 22:39:28', '2018-07-24 21:08:20'),
(139, 102, 4, 14, 'Jordana Victor Chiareli', 'jor1894victor@hotmail.com', '(44) 99986-0607', '87.200-115', '5', '69.90', '<p class=\"download\"><span>Download</span> de <i>5Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>2Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-11 22:39:30', '2018-07-24 21:08:13'),
(140, 92, 3, 25, 'Otávio de Matos Leão', 'otavio_matos1995@hotmail.com', '(41) 99638-0579', '83.606-142', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-12 05:02:51', '2018-07-24 21:08:05'),
(141, 92, 3, 25, 'Leonardo J Veiga', 'leonardojveiga@gmail.com', '(41) 99763-9972', '83.603-085', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-12 06:53:02', '2018-07-24 21:07:57'),
(142, 95, 3, 25, 'Alexandre', 'clinivetcare@gmail.com', '(41) 32927-041', '83.607-240', '30', '129.90', '<p class=\"download\"><span>Download</span> de <i>30Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>15Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-12 18:53:48', '2018-07-24 21:07:47'),
(143, 92, 3, 25, 'Murilo Ittner', 'muriloittner1@gmail.com', '(41) 98455-1412', '83.602-130', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-12 23:42:44', '2018-07-24 21:07:36'),
(144, 92, 3, 25, 'Denise Quirino do Nascimento', 'truanci@gmail.com', '(41) 99206-7066', '83.601-140', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-12 23:58:46', '2018-07-24 21:07:28'),
(145, 99, 3, 25, 'Rafael', 'Rafaelbiernaski@gmail.com', '(41) 99171-2802', '83.608-010', '5', '99.90', '<p class=\"download\"><span>Download</span> de <i>5Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Rural', '2', '2018-07-13 19:31:37', '2018-07-24 21:07:21'),
(146, 92, 3, 25, 'Luana Baltazar Silva', 'luuanabaltazar@gmail.com', '(21) 98227-7027', '83.607-050', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-14 23:59:26', '2018-07-24 21:06:03'),
(147, 92, 3, 25, 'RODRIGO GONCALVES DE ANDRADE', 'rg.andrade@outlook.com', '(41) 92277-771', '83.608-137', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-15 04:53:42', '2018-07-24 21:05:51'),
(148, 92, 3, 25, 'Pieristone Titton', 'pieristone@hotmail.com', '(41) 99697-5388', '83.602-420', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-15 21:46:21', '2018-07-24 21:04:11'),
(149, 92, 3, 25, 'Solange Lourenço ribeiro', 'solange-ribeiro93@live.com', '(41) 99924-2831', '83.601-970', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-16 07:28:31', '2018-07-24 21:04:02'),
(150, 92, 3, 25, 'Tailita dos santos de oliveira antonio', 'taisantos665@gmail.com', '(99) 71654-561', '83.603-123', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-16 10:36:49', '2018-07-24 21:03:55'),
(151, 92, 3, 24, 'Cidiani ferrarin brito', 'cferrarin07@gmail.com', '(41) 98752-4635', '83.508-430', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-16 20:40:08', '2018-07-23 18:57:47'),
(152, 92, 3, 24, 'Cidiani ferrarin brito', 'cferrarin07@gmail.com', '(41) 98752-4635', '83.508-430', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-16 20:41:19', '2018-07-23 18:57:31'),
(153, 92, 3, 25, 'Halann Couto de Lima', 'halan@fento.com.br', '(41) 98816-9615', '83.603-265', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-16 21:02:36', '2018-07-23 18:57:18'),
(154, 92, 3, 25, 'ILANY OLIVEIRA', 'ilanyoliveira@hotmail.com', '(41) 99749-3977', '83.606-176', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-16 22:07:04', '2018-07-23 18:57:08'),
(155, 92, 3, 25, 'Celson', 'tavinhodanova@hotmail.com', '(41) 99565-1774', '83.692-635', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-16 22:38:47', '2018-07-23 18:56:37'),
(156, 92, 3, 25, 'Leonardo Vieira', 'Leonardo_leozinho@live.com', '(41) 99739-9832', '83.602-390', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-17 00:07:28', '2018-07-23 18:55:24'),
(157, 92, 3, 24, 'edilson carlos caetano', 'predilsoniurd@gmail.com', '(43) 99904-3005', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-17 01:37:18', '2018-07-23 18:55:15'),
(158, 109, 4, 14, 'luiz carlos zauza', 'assecopar.cianorte@gmail.com', '(44) 99880-3073', '87.200-000', '5', '149.90', '<p class=\"download\"><span>Download</span> de <i>5Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>2Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Rural', '2', '2018-07-17 02:18:40', '2018-07-23 18:55:07'),
(159, 92, 3, 25, 'tailita dos santos', 'tailinifonda@gmail.com', '(19) 97165-4561', '83.603-123', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-17 04:22:38', '2018-07-23 18:54:58'),
(160, 92, 3, 25, 'Jeferson Netzel', 'Jefersonnetzel@gmail.com', '(41) 99630-8390', '83.602-630', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-17 08:30:58', '2018-07-23 18:54:50'),
(161, 92, 3, 24, 'Adriano Alves de Paula Rodrigues', 'adridicodi@gmail.com', '(41) 98412-4103', '82.320-710', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-18 00:35:18', '2018-07-23 18:53:30'),
(162, 92, 3, 25, 'Isabele Spake', 'isabelespake@gmail.com', '(41) 98772-3805', '83.602-090', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-18 00:39:47', '2018-07-23 18:53:21'),
(163, 92, 3, 25, 'ADELINO MANOEL DE SOUZA', 'adelino.souza1977@gmail.com', '(41) 99660-7994', '83.608-137', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-18 03:22:15', '2018-07-23 18:53:13'),
(164, 92, 3, 25, 'Cesar luiz de gouveia junior', 'cesar.juninho95@hotmail.com', '(41) 99869-8602', '83.605-230', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-18 03:37:34', '2018-07-23 18:53:04'),
(165, 93, 3, 25, 'JOcemar Bentaque dos reis', 'jocemarbentak@gmail.com', '(41) 98823-7404', '83.606-484', '15', '99.90', '<p class=\"download\"><span>Download</span> de <i>15Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>7Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-18 05:13:56', '2018-07-23 18:52:55'),
(166, 92, 3, 26, 'Wilson Antônio XAvier Kuster', 'gregokuster27@gmail.com', '(41) 98846-7500', '83.650-001', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-19 00:00:39', '2018-07-23 18:52:40'),
(167, 92, 3, 25, 'Rosane Maria Boese', 'rosaneboese13@gmail.com', '(41) 99924-8821', '83.602-640', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-19 01:32:55', '2018-07-23 18:52:30'),
(168, 95, 3, 24, 'Beatriz de Paula Müller', 'biamuller95@gmail.com', '(41) 36776-186', '83.535-000', '30', '129.90', '<p class=\"download\"><span>Download</span> de <i>30Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>15Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-19 05:17:51', '2018-07-23 18:52:10'),
(169, 92, 3, 25, 'ALINE LONGATO', 'aline.longato@gmail.com', '(41) 99207-2511', '83.604-310', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-19 19:01:19', '2018-07-23 18:51:57'),
(170, 92, 3, 25, 'Flávia Ferreira', 'flaviaferreira29al@gmail.com', '(41) 96864-183', '83.602-060', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-19 23:00:21', '2018-07-23 18:51:40'),
(171, 92, 3, 25, 'Alexandre da Cruz Feitoza', 'alexandre.feitoza@stlk.com.br', '(41) 99188-3233', '83.607-240', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-20 00:36:27', '2018-07-24 21:03:44'),
(172, 92, 3, 26, 'josé vilmar moraes dos santos', 'josevms@sanepar.com.br', '(41) 98848-4361', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-20 00:40:07', '2018-07-24 21:03:35'),
(173, 92, 3, 26, 'Guilherme', 'gui0474@hotmail.com', '(99) 88058-33', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '2', '2018-07-21 05:58:33', '2018-07-24 21:03:28'),
(174, 92, 3, 25, 'Leonardo Vieirs', 'Leonardo_leozinho@live.com', '(41) 99739-9832', '83.602-390', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-22 01:22:50', '2018-07-22 01:22:50'),
(175, 92, 3, 25, 'Aline Marques Dórea', 'linepsico802@gmail.com', '(41) 99669-7094', '83.601', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-22 21:17:38', '2018-07-22 21:17:38'),
(176, 92, 3, 25, 'Aline Marques Dórea', 'linepsico802@gmail.com', '(41) 99669-7094', '83.602-160', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-22 21:18:25', '2018-07-22 21:18:25'),
(177, 92, 3, 24, 'Jean Carlos de Oliveira', 'oliverjeancarlos@yahoo.com.br', '(41) 98782-2143', '83.535-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-22 21:49:42', '2018-07-22 21:49:42'),
(178, 92, 3, 25, 'Karine', 'karineoliva26@gmail.com', '(41) 99101-3671', '83.604-688', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-23 02:34:14', '2018-07-23 02:34:14'),
(179, 92, 3, 25, 'ALINE LONGATO', 'aline.longato@gmail.com', '(41) 99207-2511', '83.604-310', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-23 05:07:08', '2018-07-23 05:07:08'),
(180, 92, 3, 25, 'Marciano Costa', 'marcianoj.costa@gmail.com', '(41) 99637-6981', '83.607-430', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-23 09:39:36', '2018-07-23 09:39:36'),
(181, 92, 3, 26, 'Anderson', 'andersonguajuh@hotmail.com', '(41) 98796-4695', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-23 20:44:08', '2018-07-23 20:44:08'),
(182, 92, 3, 25, 'Ovaldir Antonio Melo', 'ovaldir_melo@hotmail.com', '(41) 98809-0756', '83.605-400', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-24 02:34:26', '2018-07-24 02:34:26'),
(183, 92, 3, 24, 'Dione Ribas', 'telmacorreia889@gmail.com', '(41) 99854-1161', '82.415-030', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-24 14:29:24', '2018-07-24 14:29:24'),
(184, 92, 3, 25, 'Gabriel Linhares', 'gabrielinhares@outlook.com', '(41) 99660-3423', '83.602-410', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-25 00:01:41', '2018-07-25 00:01:41'),
(185, 92, 3, 26, 'Ricardo Barichello', 'crisvitabelle@yahoo.com.br', '(41) 98847-5227', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-25 02:13:39', '2018-07-25 02:13:39'),
(186, 93, 3, 24, 'Jefferson Moreira da Silva', 'moreiradasilvajefferson@gmail.com', '(41) 99763-2162', '83.535-000', '15', '99.90', '<p class=\"download\"><span>Download</span> de <i>15Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>7Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-25 04:16:56', '2018-07-25 04:16:56'),
(187, 92, 3, 25, 'Thiago da silva luginheski', 'thiagodasilvaluginhes@gmail.com', '(41) 99777-0694', '83.602-160', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-25 07:09:15', '2018-07-25 07:09:15'),
(188, 92, 3, 25, 'Cássio Becker da cruz', 'pedidos.baristacwb@gmail.com', '(41) 99227-4946', '83.603-127', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-25 08:02:55', '2018-07-25 08:02:55'),
(189, 92, 3, 25, 'Jeferson Oroski', 'je.oroski@gmail.com', '(41) 99501-5968', '83.604-760', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-25 09:08:53', '2018-07-25 09:08:53'),
(190, 92, 3, 25, 'Edson', 'edson2920@gmail.com', '(41) 33991-700', '83.604-122', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-25 20:21:08', '2018-07-25 20:21:08'),
(191, 92, 3, 25, 'LUIZ ANTONIO DA SILVA', 'luizantsilva@gmail.com', '(41) 99217-9798', '83.606-090', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-25 23:26:39', '2018-07-25 23:26:39'),
(192, 92, 3, 25, 'Isis WIlma dos santos', 'isisea.silva@hotmail.com', '(41) 99847-3520', '83.604-566', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-26 00:22:05', '2018-07-26 00:22:05'),
(193, 92, 3, 25, 'LUIZ ANTONIO DA SILVA', 'luizantsilva@gmail.com', '(41) 99217-9798', '83.606-090', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-26 21:08:16', '2018-07-26 21:08:16'),
(194, 92, 3, 25, 'Luiz Antonio da Silva', 'luizantsilva@gmail.com', '(41) 99217-9798', '83.606-090', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-27 01:27:38', '2018-07-27 01:27:38'),
(195, 101, 4, 14, 'Danilo galvani', 'danilogalvanisilva@gmail.com', '(44) 99927-0118', '87.210-028', '2', '49.90', '<p class=\"download\"><span>Download</span> de <i>2Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>1Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-27 03:35:02', '2018-07-27 03:35:02'),
(196, 92, 3, 25, 'JULIANO GOIS GOMES', 'josijosiservienski@gmail.com', '(48) 98425-6385', '83.606-592', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-27 06:23:01', '2018-07-27 06:23:01'),
(197, 92, 3, 26, 'JUNIOR', 'moveisfiesztltda@gmail.com', '(41) 36369-147', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-27 18:44:17', '2018-07-27 18:44:17'),
(198, 92, 3, 25, 'André', 'endoschuck@hotmail.com', '(41) 99630-6696', '83.602-618', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-28 01:32:23', '2018-07-28 01:32:23'),
(199, 94, 3, 25, 'Thiago da Fonseca', 'thiago.clhc@hotmail.com', '(41) 99677-3404', '83.602-503', '20', '109.90', '<p class=\"download\"><span>Download</span> de <i>20Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>10Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-28 05:12:09', '2018-07-28 05:12:09'),
(200, 92, 3, 25, 'odarlei souza', 'odarlei_souza@yahoo.com.br', '(41) 99100-1886', '83.605-310', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-28 05:57:24', '2018-07-28 05:57:24'),
(201, 92, 3, 25, 'Jhonatan augusto', 'denise@campolargo.pr.gov.br', '(41) 99916-0213', '83.602-180', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-28 08:49:59', '2018-07-28 08:49:59'),
(202, 92, 3, 25, 'Cristiano', 'cristianosousa1909@outlook.com.br', '(41) 99714-0752', '83.603-090', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-29 21:06:17', '2018-07-29 21:06:17'),
(203, 92, 3, 25, 'Rogerio Felisberto', 'rogeriofelisberto@hotmail.com', '(11) 97338-1738', '83.604-170', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-29 23:59:09', '2018-07-29 23:59:09'),
(204, 92, 3, 24, 'Daniel Henrique Ferreira de Andrade', 'Studiov8preparacoes@gmail.com', '(41) 99948-2300', '83.430-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-30 04:45:14', '2018-07-30 04:45:14'),
(205, 92, 3, 25, 'Juliano', 'julianomaestrelli@hotmail.com', '(41) 99898-0238', '83.604-340', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-30 07:08:34', '2018-07-30 07:08:34'),
(206, 92, 3, 26, 'josé vilmar moraes dos santos', 'josevms@sanepar.com.br', '(41) 98848-4361', '83.650-000', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-30 20:46:15', '2018-07-30 20:46:15'),
(207, 92, 3, 25, 'Eduardo Batista Siqueira', 'eduardobsiqueira@ig.com.br', '(41) 98893-2392', '83.601-572', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-30 22:28:44', '2018-07-30 22:28:44'),
(208, 92, 3, 25, 'Guilherme', 'guilhermeravanellotesti@gmail.com', '(41) 99900-9329', '83.604-360', '10', '89.90', '<p class=\"download\"><span>Download</span> de <i>10Mbps</i></p>\n                        <p class=\"upload\"><span>Upload</span> de <i>5Mbps</i></p>\n                        <p class=\"limit\"><span><b>Sem limite</b></span> de franquia</p>', 'Residencial', '1', '2018-07-30 23:09:17', '2018-07-30 23:09:17');

-- --------------------------------------------------------

--
-- Estrutura da tabela `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uf` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `states`
--

INSERT INTO `states` (`id`, `uf`, `state`, `created_at`, `updated_at`) VALUES
(1, 'AC', 'Acre', '2018-02-01 23:12:34', '2018-02-01 23:12:34'),
(2, 'AL', 'Alagoas', '2018-02-01 23:12:34', '2018-02-01 23:12:34'),
(3, 'AP', 'Amapá', '2018-02-01 23:12:34', '2018-02-01 23:12:34'),
(4, 'AM', 'Amazonas', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(5, 'BA', 'Bahia', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(6, 'CE', 'Ceará', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(7, 'DF', 'Distrito Federal', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(8, 'ES', 'Espírito Santo', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(9, 'GO', 'Goiás', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(10, 'MA', 'Maranhão', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(11, 'MT', 'Mato Grosso', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(12, 'MS', 'Mato Grosso do Sul', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(13, 'MG', 'Minas Gerais', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(14, 'PA', 'Pará', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(15, 'PB', 'Paraíba', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(16, 'PR', 'Paraná', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(17, 'PE', 'Pernambuco', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(18, 'PI', 'Piauí', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(19, 'RJ', 'Rio de Janeiro', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(20, 'RN', 'Rio Grande do Norte', '2018-02-01 23:12:35', '2018-02-01 23:12:35'),
(21, 'RS', 'Rio Grande do Sul', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(22, 'RO', 'Rondônia', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(23, 'RR', 'Roraima', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(24, 'SC', 'Santa Catarina', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(25, 'SP', 'São Paulo', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(26, 'SE', 'Sergipe', '2018-02-01 23:12:36', '2018-02-01 23:12:36'),
(27, 'TO', 'Tocantins', '2018-02-01 23:12:36', '2018-02-01 23:12:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '0 = Inativo, 1 = Ativo',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_site_id_foreign` (`site_id`),
  KEY `users_role_id_foreign` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `site_id`, `role_id`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Gustavo Delfim da Silva', 'gustavo@rezerve.com.br', NULL, 1, '$2y$10$r9qUwZvMeWCDua.YJDeUEOpk4q7F9g3EV0v6K6IYTSaLj/N6BxUXO', 1, 'Io5t5CsOGTJPYgRNIr4CMf7UvHTcpQxgDHfbvIReyJ42tuIDoWZDQjtIkj0L', '2018-02-01 23:12:37', '2018-02-01 23:12:37'),
(2, 'Eduardo Polatto', 'eduardopolatto@gmail.com', NULL, 1, '$2y$10$cfWJqRhgmUOmrrsGBKJxlehMIt0bxnTctyEe6hRLIeP0BRYuy.ZZm', 1, 'HiFkbhO5dwXqpjrPY30v6lZaEecK9zUbvsvV3GYF6Ig9is0lzDTHIr44084L', '2018-02-01 23:12:37', '2018-02-01 23:12:37'),
(3, 'Admin VSP', 'vsp@vsp.com.br', 1, 2, '$2y$10$22cTpEms0eFEBL1VJVsqJe4CFmTq.F1XMz8RA/brQ0oRvBT1QS3pK', 1, 'wfALLgI1DcFrFDNaVXhIYoFN3ZkAMgn6nZ8tUweGQDstvK4Qc3qRpjzeNGdK', '2018-02-01 23:12:37', '2018-02-01 23:12:37'),
(4, 'Admin CRZ', 'crz@vsp.com.br', 2, 2, '$2y$10$oj1nbPuYtSljTTcMnRtiL.QjA1sDS7r9I8vbJHBHtPcJEWMLnDUmK', 1, 'HtthjI8c80G5w0k1zwgbeI88m9F0oJKYosPlxNN1FQNpjLX9BIKwIPssvkEg', '2018-02-01 23:12:37', '2018-02-01 23:12:37'),
(5, 'Admin Direct', 'direct@vsp.com.br', 3, 2, '$2y$10$x75US3zZrPFZw9cIFXyMpeehjLiWbW1rlJqYO0eQaAwfhkgi6j5tO', 1, 'BQU1IN2z49Ib9yRk5qZ1R72F8rznj5g3T6cHnNjXSnYfhtNeX5yWWiHSebmu', '2018-02-01 23:12:37', '2018-02-01 23:12:37'),
(6, 'Admin Vibe', 'vibe@vsp.com.br', 4, 2, '$2y$10$OAuehSV.yIpykRrVIZCwIO3l8Pgcb03ibdnhvVWChZL.CyhYgAwGq', 1, 'yj3yjA5mJ2zrPDZZ6cbeexBc5Q0pkeMleqU0TWUYTQCcnoZrVPK3PYFAEpyb', '2018-02-01 23:12:37', '2018-02-01 23:12:37'),
(7, 'Lucio', 'lucio@directwifi.com.br', NULL, 1, '$2y$10$ss2cJGuDltJLc7fJkPWHSOS2OZ1IEFCbNLHM3q5BPGaoeLab48xPa', 1, 'P8yW6yZmv72lmChxzBEAiT5aSUnQBxAO0noUPEYRJ3De3stanUxujw3n0N9B', '2018-04-18 01:33:50', '2018-04-18 01:33:50'),
(8, 'Leandro', 'leandro@directwifi.com.br', NULL, 1, '$2y$10$iDsDktK4eFckG14ztT4gfuOQTJIJNeYRqiAfwPv3UFOX8safyTsgK', 1, 'Cqd2oeO95x7DG0iKnVcge5AxNL9Rsg06bW0WB0W96fcEyilZeHD35DUifdvv', '2018-04-18 01:37:22', '2018-04-18 01:37:22'),
(9, 'teste', 'teste@teste.com', 4, 2, '$2y$10$TH3pR939CvLVkTMlmiYxA.eNB.hkN34KMDlZzMAJ7lp9ww64FUZAi', 1, NULL, '2018-06-27 08:38:38', '2018-06-27 08:38:38');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`),
  ADD CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`);

--
-- Limitadores para a tabela `configurations`
--
ALTER TABLE `configurations`
  ADD CONSTRAINT `configurations_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`),
  ADD CONSTRAINT `configurations_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`);

--
-- Limitadores para a tabela `matriz`
--
ALTER TABLE `matriz`
  ADD CONSTRAINT `matriz_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`);

--
-- Limitadores para a tabela `plans`
--
ALTER TABLE `plans`
  ADD CONSTRAINT `plans_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`);

--
-- Limitadores para a tabela `plans_cities`
--
ALTER TABLE `plans_cities`
  ADD CONSTRAINT `plans_cities_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `plans_cities_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`);

--
-- Limitadores para a tabela `solicitations`
--
ALTER TABLE `solicitations`
  ADD CONSTRAINT `solicitations_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `solicitations_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`),
  ADD CONSTRAINT `solicitations_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`);

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `users_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
