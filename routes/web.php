<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (config('app.env') == 'local') {
  Route::get('/', function(){
    $site = App\Models\Site::first();
    return redirect( $site->domain );
  });
}


Route::prefix('admin')->group(function () {
  Route::get('/login', 'Admin\LoginController@index');
  Route::post('/logar', 'Admin\LoginController@authenticate');
  Route::get('/logout', 'Admin\LoginController@logOut');

  Route::group(['middleware' => ['auth', 'guest']], function () {
    // Dashboard
    Route::get('/', 'Admin\DashboardController@index');
    // Usuarios
    Route::resource('usuarios', 'Admin\UserController');
    Route::post('usuarios/activate', 'Admin\UserController@activate');
    // Sites
    Route::resource('sites', 'Admin\SitesController');
    Route::resource('institucional', 'Admin\InstitutionalController');
    // Cidades
    Route::resource('cidades', 'Admin\CitiesController');
    Route::post('cidades/activate', 'Admin\CitiesController@activate');
    // Matriz
    Route::resource('matriz', 'Admin\MatrizController');
    // Solicitações
    Route::resource('solicitacoes', 'Admin\SolicitationsController');
    // Planos padroes
    Route::resource('planos-padroes', 'Admin\PlansController');
  });
});

Route::prefix(config('app.variable_prefix'))->group(function(){

  Route::get('/construction', function(){
    return view('construction');
  });

  Route::group(['middleware' => ['config']], function () {
    Route::get('/regiao', 'App\RegiaoController@index')->name('regiao');
    Route::post('/regiao', 'App\RegiaoController@store')->name('regiao');

    Route::group(['middleware' => ['checkcity']], function () {
      Route::get('/', 'App\HomeController@index')->name('home');
      Route::get('/institucional', 'App\InstitucionalController@index')->name('institucional');
      Route::get('/planos', 'App\PlanosController@index')->name('planos');
      Route::get('/cobertura', 'App\CoberturaController@index')->name('cobertura');
      Route::get('/faq', 'App\FaqController@index')->name('faq');
      Route::get('/atendimento', 'App\AtendimentoController@index')->name('atendimento');
      Route::post('/atendimento', 'App\AtendimentoController@store');
      Route::post('/solicitar', 'Admin\SolicitationsController@store')->name('solicitacao');
      Route::get('/emitir-segunda-via-fatura', 'App\FaturaController@index');
    });
  });

});
