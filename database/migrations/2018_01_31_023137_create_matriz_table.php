<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatrizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('matriz', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('site_id')->unsigned();
        $table->string('phone');
        $table->string('cep');
        $table->string('street');
        $table->string('number');
        $table->string('neighborhood');
        $table->string('city');
        $table->string('uf');
        $table->timestamps();

        $table->foreign('site_id')->references('id')->on('sites');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriz');
    }
}
