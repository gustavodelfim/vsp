<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('solicitations', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('plan_id')->unsigned();
        $table->integer('site_id')->unsigned();
        $table->integer('city_id')->unsigned();
        $table->string('name');
        $table->string('email');
        $table->string('phone');
        $table->string('cep');
        $table->string('mb_day');
        $table->decimal('price_day', 5, 2);
        $table->text('description_day');
        $table->string('title_package_day');
        $table->string('status')->default(1)->comment('0 - Cancelado, 1 - Espera, 2 - Fechou negocio');
        $table->timestamps();

        $table->foreign('site_id')->references('id')->on('sites');
        $table->foreign('city_id')->references('id')->on('cities');
        $table->foreign('plan_id')->references('id')->on('plans');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitations');
    }
}
