<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansCitiesTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('plans_cities', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('plan_id')->unsigned();
      $table->integer('city_id')->unsigned();
      $table->decimal('price', 5, 2);
      $table->integer('status')->default(1);
      $table->timestamps();

      $table->foreign('plan_id')->references('id')->on('plans');
      $table->foreign('city_id')->references('id')->on('cities');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    //
  }
}
