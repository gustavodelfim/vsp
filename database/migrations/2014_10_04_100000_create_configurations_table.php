<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('configurations', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('site_id')->unsigned();
        $table->string('logo_top');
        $table->string('logo_footer');
        $table->string('email_contact');
        $table->string('phone');
        $table->string('cep');
        $table->string('street');
        $table->string('number');
        $table->string('neighborhood');
        $table->string('city');
        $table->integer('state_id')->unsigned();
        $table->text('institutional_texto1');
        $table->text('institutional_texto2');
        $table->text('institutional_texto3');
        $table->timestamps();

        $table->foreign('site_id')->references('id')->on('sites');
        $table->foreign('state_id')->references('id')->on('states');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('configurations');
    }
}
