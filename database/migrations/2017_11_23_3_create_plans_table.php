<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('plans', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('package_id')->unsigned();
      $table->string('mb');
      $table->decimal('price', 5, 2);
      $table->decimal('rates_installation', 5, 2);
      $table->decimal('installation_wifi', 5, 2);
      $table->timestamps();

      $table->foreign('package_id')->references('id')->on('packages');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('plans');
  }
}
