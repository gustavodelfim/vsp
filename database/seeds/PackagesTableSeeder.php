<?php

use App\Models\Package;
use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $sites = [
            'VSP',
            'CRZ',
            'Direct',
            'Vibe'
        ];

        for ($i=0; $i < 4; $i++) {
            # code...
            $package = new Package;
            $package->title = 'Residencial';
            $package->description = 'Navegue a vontade pela internet, sem precisar se preocupar com o limite da sua franquia. Assista vídeos, ouça músicas, faça download, pesquisas e muito mais!';
            $package->site_id = $i+1;
            $package->save();

            $package = new Package;
            $package->title = 'Empresarial';
            $package->description = 'Tenha total segurança em um serviço de internet que possui preço justo, que é veloz, ilimitado e conta com rápida manutenção.';
            $package->site_id = $i+1;
            $package->save();

            $package = new Package;
            $package->title = 'Rural';
            $package->description = 'Seja no campo ou na cidade, vamos até você! Na '. $sites[$i] .' não há desculpas, não há limite de dados e nem barreiras territoriais.';
            $package->site_id = $i+1;
            $package->save();
        }
    }
}
