<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role;
        $role->role = 'Administrador Master';
        $role->slug = 'administrator';
        $role->description = 'Administrador Geral';
        $role->save();

        $role = new Role;
        $role->role = 'Colaborador';
        $role->slug = 'collaborator';
        $role->description = 'Gerenciador dos Sites';
        $role->save();
    }
}
