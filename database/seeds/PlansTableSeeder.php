<?php

use App\Models\Plan;
use App\Models\Package;
use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $packages = Package::all();

        for ($i=0; $i < count($packages); $i++) {
            # code...
            $plan = new Plan;
            $plan->package_id = $packages[$i]->id;
            $plan->mb = 1;
            $plan->price = 69.90;
            $plan->rates_installation = 99.90;
            // $plan->description = '<p><span>Download</span> de 1Mbps</p> <p><span>Upload</span> de 500Kbps <p><span>Sem limite</span> de franquia</p>';
            $plan->installation_wifi = 150.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            $plan = new Plan;
            $plan->package_id = $packages[$i]->id;
            $plan->mb = 2;
            $plan->price = 89.90;
            $plan->rates_installation = 99.90;
            $plan->installation_wifi = 150.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            $plan = new Plan;
            $plan->package_id = $packages[$i]->id;
            $plan->mb = 3;
            $plan->price = 99.90;
            $plan->rates_installation = 99.90;
            $plan->installation_wifi = 150.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            $plan = new Plan;
            $plan->package_id = $packages[$i]->id;
            $plan->mb = 4;
            $plan->price = 129.90;
            $plan->rates_installation = 99.90;
            $plan->installation_wifi = 150.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            $plan = new Plan;
            $plan->package_id = $packages[$i]->id;
            $plan->mb = 5;
            $plan->price = 149.90;
            $plan->rates_installation = 99.90;
            $plan->installation_wifi = 150.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            // Planos Empresarial
            $plan = new Plan;
            $plan->package_id = $packages[$i+1]->id;
            $plan->mb = 3;
            $plan->price = 129.90;
            $plan->rates_installation = 99.90;
            $plan->installation_wifi = 150.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            $plan = new Plan;
            $plan->package_id = $packages[$i+1]->id;
            $plan->mb = 4;
            $plan->price = 149.90;
            $plan->rates_installation = 99.90;
            $plan->installation_wifi = 179.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            // Planos Rurais
            $plan = new Plan;
            $plan->package_id = $packages[$i+2]->id;
            $plan->mb = 1;
            $plan->price = 99.90;
            $plan->rates_installation = 99.90;
            $plan->installation_wifi = 150.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            $plan = new Plan;
            $plan->package_id = $packages[$i+2]->id;
            $plan->mb = 2;
            $plan->price = 119.90;
            $plan->rates_installation = 99.90;
            $plan->installation_wifi = 150.00;
            $plan->download = 1;
            $plan->upload = 1;
            $plan->limit = 'Sem limite';
            $plan->desc_download = 'Mbps';
            $plan->desc_upload = 'Mbps';
            $plan->save();

            $i = $i+2;
        }

    }
}
