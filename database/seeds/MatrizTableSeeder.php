<?php

use App\Models\Matriz;
use Illuminate\Database\Seeder;

class MatrizTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 4; $i++) {
          $matriz = new Matriz;
          $matriz->site_id = $i+1;
          $matriz->phone = '44988325611';
          $matriz->cep = '88034040';
          $matriz->street = 'Teste';
          $matriz->number = '123';
          $matriz->neighborhood = '123';
          $matriz->city = 'Maringá';
          $matriz->uf = 'PR';
          $matriz->save();
        }
    }
}
