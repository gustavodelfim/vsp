<?php

use App\Models\Configuration;
use Illuminate\Database\Seeder;

class ConfigurationTableSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {

    $logos = [
      [
        'top' => 'LvUekj2QCIGaHLsIkLl3OWz4bBojh6mTzMXt6FMG.png',
        'footer' => 'V4KJvb2bW8YdsKmMCDSXoW5aQdSxIWFZVLY7JGBM.png'
      ],
      [
        'top' => 'logo2-top.png',
        'footer' => 'logo2-footer.png'
      ],
      [
        'top' => 'logo3-top.png',
        'footer' => 'logo3-footer.png'
      ],
      [
        'top' => 'logo4-top.png',
        'footer' => 'logo4-footer.png'
      ]
    ];
    for ($i=1; $i <= 4; $i++) {
      $config = new Configuration;
      $config->site_id = $i;
      $config->logo_top = 'uploads/admin/logos/' . $logos[$i-1]['top'];
      $config->logo_footer = 'uploads/admin/logos/' . $logos[$i-1]['footer'];
      $config->email_contact = 'contato@vspinternet.com';
      $config->phone = '(67) 33410-333';
      $config->cep = '79.050-112';
      $config->street = 'Rua Quintino Bocaiúva';
      $config->number = '1050';
      $config->neighborhood = 'Jd. Paulista';
      $config->city = 'Campo Grande';
      $config->state_id = 12;
      $config->institutional_texto1 = 'A VSP INTERNET surgiu no início de 1999 da necessidade tecnológica do mercado, que buscava soluções acessíveis e proﬁssionais em acesso à Internet. A partir da aquisição de um pequeno provedor de acesso, uma equipe de grande experiência foi destacada para montar um sistema de parceria com empresas locais e que ao mesmo tempo pudesse concorrer com as grandes redes de provedores do Brasil.';
      $config->institutional_texto2 = 'O sucesso dependia de trabalho, e a nova equipe experimentou um dos maiores crescimentos já constatados em todo o mercado Internet, alcançando vários pontos de presença em apenas dois meses. A partir de então, foram adquiridos outros provedores e agregados mais proﬁssionais, que concluíram todo o conjunto de serviços que hoje a Vibe Internet pode oferecer, serviços estes que estão sendo lançados ao mesmo tempo senão antes mesmo de seus grandes concorrentes nacionais.';
      $config->institutional_texto3 = 'São números, fatos e sucesso conseguidos não só pelo ideal empreendedor da Vibe, mas também pela excelente equipe de proﬁssionais que a empresa possui não só em sua Central de Atendimento, mas em todos os parceiros locais. Trata-se de um quadro de proﬁssionais altamente qualiﬁcados e certiﬁcados. Além disso, o modelo de gestão setorizado possibilitou independência e trouxe resultados em números e produção.';
      $config->save();
    }
  }
}
