<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Gustavo Delfim da Silva';
        $user->email = 'gustavo@rezerve.com.br';
        $user->password = Hash::make('gustavo994');
        $user->site_id =  null;
        $user->role_id =  1;
        $user->save();

        $user = new User;
        $user->name = 'Eduardo Polatto';
        $user->email = 'eduardopolatto@gmail.com';
        $user->password = Hash::make('123mudar');
        $user->site_id =  null;
        $user->role_id =  1;
        $user->save();

        $user = new User;
        $user->name = 'Admin VSP';
        $user->email = 'vsp@vsp.com.br';
        $user->password = Hash::make('123mudar');
        $user->site_id =  1;
        $user->role_id =  2;
        $user->save();

        $user = new User;
        $user->name = 'Admin CRZ';
        $user->email = 'crz@vsp.com.br';
        $user->password = Hash::make('123mudar');
        $user->site_id =  2;
        $user->role_id =  2;
        $user->save();

        $user = new User;
        $user->name = 'Admin Direct';
        $user->email = 'direct@vsp.com.br';
        $user->password = Hash::make('123mudar');
        $user->site_id =  3;
        $user->role_id =  2;
        $user->save();

        $user = new User;
        $user->name = 'Admin Vibe';
        $user->email = 'vibe@vsp.com.br';
        $user->password = Hash::make('123mudar');
        $user->site_id =  4;
        $user->role_id =  2;
        $user->save();
    }
}
