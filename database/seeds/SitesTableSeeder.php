<?php

use Illuminate\Database\Seeder;
use App\Models\Site;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $site = new Site;
        $site->domain = 'vsp';
        $site->title = 'VSP';
        $site->slug = 'vsp';
        $site->save();

        $site = new Site;
        $site->domain = 'crz';
        $site->title = 'CRZ';
        $site->slug = 'crz';
        $site->save();

        $site = new Site;
        $site->domain = 'direct';
        $site->title = 'Direct';
        $site->slug = 'direct';
        $site->save();

        $site = new Site;
        $site->domain = 'vibe';
        $site->title = 'Vibe';
        $site->slug = 'vibe';
        $site->save();
    }
}
