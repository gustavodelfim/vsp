<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // $this->call(StatesTableSeeder::class);
    // $this->call(RoleTableSeeder::class);
    // $this->call(SitesTableSeeder::class);
    // $this->call(UsersTableSeeder::class);
    // $this->call(PackagesTableSeeder::class);
    $this->call(PlansTableSeeder::class);
    // $this->call(ConfigurationTableSeeder::class);
    // $this->call(CitiesTableSeeder::class);
    // $this->call(MatrizTableSeeder::class);
  }
}
