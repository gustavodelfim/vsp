<?php

use App\Models\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $states = [
        ['AC', 'Acre'],
      	['AL', 'Alagoas'],
      	['AP', 'Amapá'],
      	['AM', 'Amazonas'],
      	['BA', 'Bahia'],
      	['CE', 'Ceará'],
      	['DF', 'Distrito Federal'],
      	['ES', 'Espírito Santo'],
      	['GO', 'Goiás'],
      	['MA', 'Maranhão'],
      	['MT', 'Mato Grosso'],
      	['MS', 'Mato Grosso do Sul'],
      	['MG', 'Minas Gerais'],
      	['PA', 'Pará'],
      	['PB', 'Paraíba'],
      	['PR', 'Paraná'],
      	['PE', 'Pernambuco'],
      	['PI', 'Piauí'],
      	['RJ', 'Rio de Janeiro'],
      	['RN', 'Rio Grande do Norte'],
      	['RS', 'Rio Grande do Sul'],
      	['RO', 'Rondônia'],
      	['RR', 'Roraima'],
      	['SC', 'Santa Catarina'],
      	['SP', 'São Paulo'],
      	['SE', 'Sergipe'],
      	['TO', 'Tocantins']
      ];

      foreach ($states as $key => $value) {
        $state = new State;
        $state->uf = $value[0];
        $state->state = $value[1];
        $state->save();
      }
    }
}
