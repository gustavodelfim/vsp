<table>
  <tr>
    <td  height="30"> </td>
    <td></td>
  </tr>
  <tr>
    <td><b>Solicitação - {{ $site['title'] }}</b></td>
    <td></td>
  </tr>
  <tr>
    <td  height="15"> </td>
    <td></td>
  </tr>
  <tr>
    <td><b>Nome:</b></td>
    <td>{{ $name }}</td>
  </tr>
  <tr>
    <td><b>Email:</b></td>
    <td>{{ $email }}</td>
  </tr>
  <tr>
    <td><b>Telefone:</b></td>
    <td>{{ $phone }}</td>
  </tr>
  <tr>
    <td><b>CEP:</b></td>
    <td>{{ $cep }} ({{ $city['name'] }})</td>
  </tr>
  <tr>
    <td><b>Plano:</b></td>
    <td>{{ $mb_day }}MB ({{ $title_package_day }})</td>
  </tr>
  <tr>
    <td  height="40"> </td>
    <td></td>
  </tr>
  <tr>
    <td><b>Informações:</b></td>
    <td><?php echo nl2br($description_day) ?></td>
  </tr>
  <tr>
    <td  height="30"> </td>
    <td></td>
  </tr>
</table>
