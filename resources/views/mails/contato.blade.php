<table>
  <tr>
    <td  height="30"> </td>
    <td></td>
  </tr>
  <tr>
    <td><b>Contato - {{ $site }}</b></td>
    <td></td>
  </tr>
  <tr>
    <td  height="15"> </td>
    <td></td>
  </tr>
  <tr>
    <td><b>Nome:</b></td>
    <td>{{ $nome }} {{ $sobrenome }}</td>
  </tr>
  <tr>
    <td><b>Email:</b></td>
    <td>{{ $email }}</td>
  </tr>
  <tr>
    <td><b>Telefone:</b></td>
    <td>{{ $telefone }}</td>
  </tr>
  <tr>
    <td><b>Assunto:</b></td>
    <td>{{ $assunto }}</td>
  </tr>
  <tr>
    <td  height="40"> </td>
    <td></td>
  </tr>
  <tr>
    <td><b>Mensagem:</b></td>
    <td><?php echo nl2br($mensagem) ?></td>
  </tr>
  <tr>
    <td  height="30"> </td>
    <td></td>
  </tr>
</table>
