@extends('welcome', [
  'title_page' => 'Atendimento'
])

@section('content')
  <div class="row page" id="atendimento">
    <div class="col-xs-12">
      <div class="container">
        <h1>Fale Conosco</h1>
        <p>
          <span></span><br>

          {{ session('site')['street'] }}, {{ session('site')['number'] }}<br>
          {{ session('site')['neighborhood'] }}, CEP: {{ session('site')['cep'] }} - {{ session('site')['city'] }} - {{ session('site')['state']['state'] }}<br>
          Tel: {{ session('site')['phone'] }}
        </p>

        <form role="form" action="" method="post" id="formContato" name="formContato">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="form-group">
                <label for="nome">Nome:</label>
                <input type="text" class="form-control" id="nome" name="nome">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="form-group">
                <label for="email">E-mail:</label>
                <input type="text" class="form-control" id="email" name="email">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="form-group">
                <label for="telefone">Fone:</label>
                <input type="text" class="form-control phone" id="telefone" name="telefone">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="form-group">
                <label for="mensagem">Menagem:</label>
                <textarea class="form-control" id="mensagem" name="mensagem"></textarea>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <span class="status"></span>
            </div>
            <div class="col-sm-12 col-md-12">
              <div class="g-recaptcha" id="recaptcha" data-sitekey="6LfzPicUAAAAAMx-7MgiluAON0faTOmZwPuPoayO"></div>
            </div>
            <div class="col-sm-12 col-md-12 text-center">
              <button type="button" class="btn btn-default">ENVIAR</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
