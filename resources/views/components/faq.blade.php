<?php
    $faq = [
        [
            'title' => 'A chuva atrapalha o desempenho de minha conexão?',
            'response' => 'A chuva não interfere no sinal da internet a menos que o fornecimento da energia seja interrompido num ponto de
            acesso (torre) ou na casa/empresa do cliente.
            <br><br>
            O fato de estar chovendo, não significa um motivo para a queda na qualidade da conexão.'
        ],
        [
            'title' => 'É possível utilizar a internet em mais de um computador ou até com minha TV?',
            'response' => 'Sim, entretanto o cliente precisa de um roteador interno para que a conexão possa ser compartilhada em mais de uma máquina dentro da própria residência.
            <br><br>
            Ficando vetada o compartilhamento entre vizinhos.
            <br><br>
            Vale ressaltar que a conexão compartilhada, implica na diminuição do desempenho da conexão entre as máquinas que esteja sendo usada simultaneamente, onde
            a banda contratada será dividida entre os dispositivos conectados.'
        ],
        [
            'title' => 'O que é Banda Larga?',
            'response' => 'É uma conexão à internet com velocidade superior ao padrão das linhas telefônicas convencionais (56 kbps - kilobit por segundo), o que
            permite transmitir dados com muito mais rapidez e manter o usuário permanentemente conectado à web.'
        ],
        [
            'title' => 'O que é Comodato?',
            'response' => 'Comodato é o empréstimo dos equipamentos necessários para a utilização dos serviços oferecidos pela ' . session('site')['title'] . '. É cedido em regime de comodato,
            sem que o cliente pague pelo equipamento.
            <br><br>
            O equipamento fica sob a responsabilidade do cliente até o término do contrato, devendo este ser devolvido a ' . session('site')['title'] . ' se ocorrer o cancelamento dos serviços.'
        ],
        [
            'title' => 'Por que ter acesso Banda Larga?',
            'response' => 'Porque a velocidade da conexão é muito maior que a da conexão discada (via linha telefônica), o que permite uma experiência muito rica e
            dinâmica com os conteúdos e recursos mais atuais da internet. Os principais benefícios da Banda Larga são:<br>
            * Você está sempre conectado
            <br><br>
            * Linha telefônica desocupada
            <br><br>
            * Economia de tempo'
        ],
        [
            'title' => 'Quanto tempo leva para instalação da internet?',
            'response' => 'O prazo para instalação é de no máximo 07 (sete) dias úteis a partir da assinatura do contrato. Já o tempo da instalação pode variar de acordo com a
            dificuldade da instalação em cada local, mas geralmente leva de 1 a 3 horas.'
        ]
    ];
?>
<article class="clear faq">
  <div class="clear titulo">
    <h2>FAQ</h2>
    <p> As perguntas mais frequentes. </p>
  </div>

  <div class="clear perguntas">
    <div class="container">
      <div class="max-width">

        @foreach ($faq as $key => $value)
          <div class="clear cont" data-active="no-active">
            <a href="javascript:;" class="clear title" onclick="Faq.pergunta.click(this)"> {{ $value['title'] }} <span class="icon icon-seta"></span> </a>
            <div class="clear info">
              <div class="clear height">
                <div class="clear detalhe">
                  <p>
                    {!! $value['response'] !!}
                  </p>
                </div>
              </div>
            </div>
          </div>
        @endforeach

      </div>
    </div>
  </div>

  <div class="clear botao">
    <a href="{{ url(config('app.param_prefix') . 'atendimento') }}" title="Ver todos os Planos">TEM UMA PERGUNTA QUE NÃO ESTÁ NA LISTA?</a>
  </div>

</article>

@push('js')
  <script src="{{ asset('js/app/faq.js') }}"></script>
@endpush
