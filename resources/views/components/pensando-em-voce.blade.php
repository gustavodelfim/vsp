<article class="clear pensando">
  <div class="clear titulo">
    <h2> Planos pensado para você </h2>
    <p>
      Aqui você é livre para ter somente a sua internet com velocidade e qualidade<br>
      sem precisar de Telefone ou TV a cabo.
    </p>
  </div>

  <div class="clear botao">
    <a href="{{ url(config('app.param_prefix') . 'planos') }}" title="Ver todos os Planos">VER TODOS OS PLANOS</a>
  </div>

  <div class="clear">
    <div class="container">
      <div class="max-width">
        <div class="row rows">
          <div class="col-md-6 cols">
            <div class="info left">
              <h4>RESIDENCIAL</h4>
              <p class="desc"> Com velocidade de 2MB até </p>
              <p class="mb">8 <span>MB</span></p>
              <span class="line"><span></span></span>
              <a href="{{ url(config('app.param_prefix') . 'atendimento') }}" class="regiao">*Verifique a sua região</a>
              <div class="infos">
                <p><span>Download</span> de 8MB</p>
                <p><span>Upload</span> de 2MB</p>
                <p><span>Sem limite</span> de franquia</p>
              </div>
              <a href="{{ url(config('app.param_prefix') . 'planos') }}" class="bt">CONSULTAR VALORES</a>
            </div>
          </div>
          <div class="col-md-6 cols">
            <div class="info">
              <h4>FIBRA RESIDENCIAL</h4>
              <p class="desc"> Com velocidade de 10MB até </p>
              <p class="mb">30 <span>MB</span></p>
              <span class="line"><span></span></span>
              <a href="{{ url(config('app.param_prefix') . 'atendimento') }}" class="regiao">*Verifique a sua região</a>
              <div class="infos">
                <p><span>Download</span> de 8MB</p>
                <p><span>Upload</span> de 2MB</p>
                <p><span>Sem limite</span> de franquia</p>
              </div>
              <a href="{{ url(config('app.param_prefix') . 'planos') }}" class="bt">CONSULTAR VALORES</a>
            </div>
          </div>

          <div class="clear mude">
            <p> PLANOS COM MESALIDADE FIXA! EVITE SURPRESAS NA HORA DO PAGAMENTO. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>
