
<article class="clear entre-nessa {{ session('site')['slug'] }}">
  <span class="bg"></span>
  <div class="container">
    <div class="infos">
      <h1>Conectando pessoas<br> a momentos</h1>
      <img src="{{ asset('img/entre-nessa-vibe.png') }}" alt="" class="img-vibe">
      <div class="botao">
        <a href="{{ url(config('app.param_prefix') . 'planos') }}" title="Mude Agora">MUDE AGORA</a>
      </div>
    </div>
  </div>
</article>
