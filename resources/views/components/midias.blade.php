
<article class="clear midias {{ session('site')['slug'] }}">
  <span class="bg"></span>
  <div class="center">
    <div class="infos">
      <div class="container">
        <h1>
          Curta suas<br>
          redes sociais,<br>
          músicas, ﬁlmes<br>
          & jogos!
        </h1>
        <p>
          Escolha o melhor plano e tenha acesso a internet de<br>
          qualidade com mensalidade acessível!
        </p>
        <div class="links">
          <a href="{{ url(config('app.param_prefix') . 'planos') }}" class="bt">MUDE AGORA</a>
          <a href="{{ url(config('app.param_prefix') . 'planos') }}" class="tour">Conheça nossos planos <img src="{{ asset('img/flexa-pub-banner-home.png') }}" alt=""> </a>
        </div>
    </div>
    </div>
  </div>
</article>
