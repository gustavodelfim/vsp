
<article class="clear internet {{ session('site')['slug'] }}">
  <span class="bg"></span>
  <div class="infos">
    <h1>Internet para todos</h1>
    <p>
      Escolha o melhor plano e tenha acesso a internet de<br>
      qualidade com mensalidade acessível!
    </p>
    <div class="botao">
      <a href="{{ url(config('app.param_prefix') . 'planos') }}" title="Conheça Agora">CONHEÇA AGORA</a>
    </div>
  </div>
</article>
