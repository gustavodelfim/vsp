<article class="clear vantagens">
  <div class="clear titulo">
    <h2>Aqui tem mais vantagens</h2>
    <p> Conheça as vantagens de ser um assinante {{ session('site')['title'] }}. </p>
  </div>
  <div class="clear">
    <div class="container">
      <div class="row rows">
        <div class="col-md-6 cols">
          <div class="info left">
            <h4>Para navegar mais</h4>
            <div class="detalhe">
              <div class="center">
                <span class="icon-icon1-vantagens icon"></span>
                <p> ALTA VELOCIDADE<br> DE UPLOAD </p>
              </div>
            </div>
            <div class="detalhe">
              <div class="center">
                <span class="icon-icon2-vantagens icon"></span>
                <p> VELOCIDADE DE<br> 2MB À 100MB </p>
                </div>
            </div>
            <div class="detalhe">
              <div class="center">
                <span class="icon-icon3-vantagens icon"></span>
                <p> SEM LIMITE DE<br> FRANQUIA </p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 cols">
          <div class="info">
            <h4>Para navegar mais</h4>
            <div class="detalhe">
              <div class="center">
                <span class="icon-icon4-vantagens icon"></span>
                <p> SEM COMODATO<br> DE TELEFONE </p>
              </div>
            </div>
            <div class="detalhe">
              <div class="center">
                <span class="icon-icon5-vantagens icon"></span>
                <p> CONEXÃO SEGURA E<br> DE QUALIDADE </p>
              </div>
            </div>
            <div class="detalhe">
              <div class="center">
                <span class="icon-icon6-vantagens icon"></span>
                <p> ATENDIMENTO LOCAL EM<br> TODAS AS CIDADES </p>
              </div>
            </div>
          </div>
        </div>
        <div class="clear botao">
          <a href="{{ url(config('app.param_prefix') . 'planos') }}" title="Nossos Planos">NOSSOS PLANOS</a>
        </div>
        <div class="clear mude">
          <a href="{{ url(config('app.param_prefix') . 'atendimento') }}">
            <span class="left">MUDE AGORA!</span>
            <span class="rigth">
              Fale com um de nossos consultores para saber
              o plano ideal para você <span class="icon-seta icon"></span>
            </span>
          </a>
        </div>
      </div>
    </div>
  </div>
</article>
