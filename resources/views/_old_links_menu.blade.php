<li><a class="{{ (Request::is(config('app.param_prefix') . '/') or Request::is( str_replace('/', '', config('app.param_prefix')) )) ? 'selected' : '' }}" href="{{ url(config('app.param_prefix')) }}">Home</a></li>
<li><a class="{{ Request::is(config('app.param_prefix') . 'institucional') ? 'selected' : '' }}" href="{{ url(config('app.param_prefix') . 'institucional') }}">Institucional</a></li>
<li><a class="{{ Request::is(config('app.param_prefix') . 'planos') ? 'selected' : '' }}" href="{{ url(config('app.param_prefix') . 'planos') }}">Planos</a></li>
<li><a class="{{ Request::is(config('app.param_prefix') . 'cobertura') ? 'selected' : '' }}" href="{{ url(config('app.param_prefix') . 'cobertura') }}">Cobertura</a></li>
<li><a class="{{ Request::is(config('app.param_prefix') . 'faq') ? 'selected' : '' }}" href="{{ url(config('app.param_prefix') . 'faq') }}">FAQ</a></li>
<li><a class="{{ Request::is(config('app.param_prefix') . 'atendimento') ? 'selected' : '' }}" href="{{ url(config('app.param_prefix') . 'atendimento') }}">Atendimento</a></li>
