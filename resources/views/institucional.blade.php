@extends('welcome', [
  'page' => 'Institucional'
])

@section('content')
  <section id="institucional">
    <div class="banner {{ session('site')['slug'] }} clear"> </div>

    <div class="clear sobre">
      <div class="clear titulos">
        <h2>A {{ session('site')['title'] }}</h2>
        <p>
          @if(session('site')['slug'] == 'direct')
            Conexão direta com o futuro
          @else
            Conectando o Noroeste do Paraná.
          @endif
        </p>
      </div>
      <p class="clear texto">
        <span style="max-width: 1000px; margin: 0 auto; display: block">
          <?php echo nl2br(session('site')['institutional_texto1']) ?>
        </span>
      </p>
      <div class="clear icons">
        <div class="containier">
          <div class="icon">
            <img src="{{ asset('img/institucional/icon1-' . session('site')['slug'] . '.png') }}" alt="">
            <a href="#inicio">INICIO</a>
          </div>
          <div class="icon">
            <img src="{{ asset('img/institucional/icon2-' . session('site')['slug'] . '.png') }}" alt="">
            <a href="#sucesso">SUCESSO</a>
          </div>
          <div class="icon">
            <img src="{{ asset('img/institucional/icon3-' . session('site')['slug'] . '.png') }}" alt="">
            <a href="#excelencia">EXCELÊNCIA</a>
          </div>
        </div>
      </div>
    </div>

    <div class="banner2 {{ session('site')['slug'] }} clear"> </div>

    <div class="clear infos">
      <div class="clear titulos">
        <h2>A {{ session('site')['title'] }}</h2>
        <p> {{ config('app.configs.' .session('site')['slug']. '.slogan_institucional') }} </p>
      </div>
      <div class="clear case">
        <div class="case" id="inicio">
          <span class="icon icon-icon1-texto-institucional"></span>
          <h4>
            DESDE
            @if (session('site')['slug'] == 'direct')
              2011
            @else
              1999
            @endif
          </h4>
          <p>
            <?php echo nl2br(session('site')['institutional_texto1']) ?>
          </p>
        </div>
        <div class="case" id="sucesso">
          <span class="icon icon-icon2-texto-institucional"></span>
          <h4> O CRESCIMENTO </h4>
          <p>
            <?php echo nl2br(session('site')['institutional_texto2']) ?>
          </p>
        </div>
        <div class="case" id="excelencia">
          <span class="icon icon-icon3-texto-institucional"></span>
          <h4> COM EXCELÊNCIA </h4>
          <p>
            <?php echo nl2br(session('site')['institutional_texto3']) ?>
          </p>
        </div>
      </div>
    </div>

    <div class="clear banner3 {{ session('site')['slug'] }}"> </div>
  </section>
@endsection
