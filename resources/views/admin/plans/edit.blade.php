@extends('admin.template', [
    'title_page' => 'Cidades'
]);


@section('content')
    <a href="{{ url(config('app.path_admin') . '/planos-padroes') }}" class="btn btn-success btn-novo" rel="tooltip" data-placement="right" title="Voltara a lista"><i class="material-icons">list</i></a>

    <div class="card">
        <div class="card-header" data-background-color="blue">
            <h4 class="title">Detalhes do Plano</h4>
            <p class="category">Preencha todos os campos para proceguir</p>
        </div>
        <div class="card-content table-responsive">
            {{ Form::open(['url' => config('app.path_admin') . '/planos-padroes/' . $site->id , 'method' => 'put']) }}
            <br><br>
            <?php
            $color = [
                'red',
                'blue',
                'orange'
            ];
            $key = 0;
            ?>
            @foreach ($packages as $jey => $package)
                <h4>Planos {{ $package->title }}</h4>
                <input type="hidden" value="{{ $package->id }}" required name="packages[{{ $jey }}][id]"/>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group label-floating" style="margin-top: 0">
                            <textarea name="packages[{{ $jey }}][description]" required rows="3" class="form-control">{{ $package->description }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach ($package->plans as $plan)
                        <div class="col-md-3 card-plan">
                            <input type="hidden" value="{{ $plan->id }}" required name="plans[{{ $key }}][id]"/>
                            <div class="card">
                                <div class="card-header" data-background-color="{{ $color[$jey] }}">
                                    <h3 style="margin: 0; text-align: center">
                                        <input type="number" min="0" value="{{ $plan->mb }}" required name="plans[{{ $key }}][mb]" class="form-control" style="font-size: 1em; color: #fff; text-align: center" />
                                        <span style="font-size: .6em">MB</span>
                                    </h3>
                                </div>
                                <div class="card-content">
                                    <div class="form-group label-floating" style="margin: 0">
                                        <div class="row">
                                            <label class="col-md-4">Down.</label>
                                            <div class="col-input col-md-4">
                                                <input type="number" min="0" value="{{ $plan->download }}" required name="plans[{{ $key }}][download]" class="form-control"/>
                                            </div>
                                            <div class="col-input col-md-4">
                                                <select required class="selectpicker" name="plans[{{ $key }}][desc_download]">
                                                    <option value="Mbps" @if($plan->desc_download == 'Mbps') selected @endif>Mbps</option>
                                                    <option value="Kbps" @if($plan->desc_download == 'Kbps') selected @endif>Kbps</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group label-floating" style="margin: 0">
                                        <div class="row">
                                            <label class="col-md-4">Upload</label>
                                            <div class="col-input col-md-4">
                                                <input type="number" min="0" value="{{ $plan->upload }}" required name="plans[{{ $key }}][upload]" class="form-control"/>
                                            </div>
                                            <div class="col-input col-md-4">
                                                <select required class="selectpicker" name="plans[{{ $key }}][desc_upload]">
                                                    <option value="Mbps" @if($plan->desc_upload == 'Mbps') selected @endif>Mbps</option>
                                                    <option value="Kbps" @if($plan->desc_upload == 'Kbps') selected @endif>Kbps</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group label-floating" style="margin: 0">
                                        <div class="row">
                                            <label class="col-md-4">Limite</label>
                                            <div class="col-input col-md-8">
                                                <input type="text" value="{{ $plan->limit }}" required name="plans[{{ $key }}][limit]" class="form-control"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group label-floating" style="margin: 0">
                                        <div class="row">
                                            <label class="col-md-4">Preço</label>
                                            <div class="col-input col-md-8">
                                                <input type="text" value="{{ $plan->price }}" required name="plans[{{ $key }}][price]" class="form-control money"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $key++; ?>
                    @endforeach
                </div>
                <br><br>
            @endforeach

            <h4>Salvar Como:</h4>
            <p>Aqui você pode escolher quais mudanças iram editar as informações de todos os planos existentes</p>
            <div class="form-group label-floating">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="sinc[velocidade]"/>
                        Velocidade
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="sinc[download]"/>
                        Download
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="sinc[upload]"/>
                        Upload
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="sinc[limit]"/>
                        Limite
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="sinc[preco]"/>
                        Preço
                    </label>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Salvar mudanças</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
