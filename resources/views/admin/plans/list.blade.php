@extends('admin.template', [
  'title_page' => 'PLanos Padrões'
])

@section('content')
  @foreach ($sites as $key => $site)
    <div class="col-md-3">
        <div class="card card-profile">
            <div class="content">
                <h6 class="category text-gray">{{ $site->domain }}</h6>
                <h4 class="card-title">{{ $site->title }}</h4>
                <a href="{{ url(config('app.path_admin') . '/planos-padroes/' . $site->id . '/edit') }}" class="btn btn-primary btn-round">Editar Planos</a>
            </div>
        </div>
    </div>
  @endforeach
@endsection
