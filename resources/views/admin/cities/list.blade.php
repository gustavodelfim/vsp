@extends('admin.template', [
  'title_page' => 'Cidades'
]);


@section('content')
  <a href="{{ url(config('app.path_admin') . '/cidades/create') }}" class="btn btn-success btn-novo" rel="tooltip" data-placement="right" title="Adicionar nova cidade"><i class="material-icons">add</i></a>

  <div class="card">
    <div class="card-header" data-background-color="blue">
      <h4 class="title">Lista de Cidades</h4>
      <p class="category">Cidades de cobertura da sua empresa</p>
    </div>
    <div class="card-content table-responsive">
      @if(count($cities) > 0)
        <table class="table">
            <thead class="text-primary">
                <th>Nome</th>
                <th>Estado</th>
                <th>Telefone</th>
                @if (Auth::user()->role->slug == 'administrator')
                  <th>Site</th>
                @endif
                <th>Status</th>
                <th width="80">Ações</th>
            </thead>
            <tbody>
              @foreach($cities as $city)
                <tr>
                    <td>{{ $city->name }}</td>
                    <td>{{ $city->state->state }}</td>
                    <td>{{ $city->phone }}</td>
                    @if (Auth::user()->role->slug == 'administrator')
                      <td>{{ $city->site->title }}</td>
                    @endif
                    <td>
                      <span class="circle-status @if ($city->status) ativo @else inativo @endif"></span>
                      @if ($city->status) Ativo @else Inativo @endif
                    </td>
                    <td class="td-actions text-right">
                        <a href="{{ url(config('app.path_admin') . '/cidades/' . $city->id . '/edit') }}" rel="tooltip" title="Editar Cidade e seus Planos" class="btn btn-warning btn-simple btn-xs">
                            <i class="material-icons">edit</i>
                        </a>
                        @if ($city->status)
                          {{ Form::open(['url' => config('app.path_admin') . '/cidades/' . $city->id, 'method' => 'delete']) }}
                            <button type="button" rel="tooltip" title="Inativar Cidade" class="btn btn-danger btn-simple btn-xs" onclick="window.app.confirmDelete('Deseja Realmente inativar esta Cidade?', this); return false">
                                <i class="material-icons">delete</i>
                            </button>
                          {{ Form::close() }}
                        @endif
                        @if (!$city->status)
                          {{ Form::open(['url' => config('app.path_admin') . '/cidades/activate', 'method' => 'post']) }}
                            <input type="hidden" name="id" value="{{ $city->id }}" />
                            <button type="button" rel="tooltip" title="Ativar Cidade" class="btn btn-success btn-simple btn-xs" onclick="window.app.confirmDelete('Deseja Realmente Ativar esta Cidade?', this); return false">
                                <i class="material-icons">done</i>
                            </button>
                          {{ Form::close() }}
                        @endif
                    </td>
                </tr>
              @endforeach
            </tbody>
        </table>

        {{ $cities->links() }}
      @else
        <p>Não há cidades cadastrada para esse Site
      @endif
    </div>
  </div>
@endsection
