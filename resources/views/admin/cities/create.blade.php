@extends('admin.template', [
  'title_page' => 'Cidades'
]);


@section('content')
  {{-- <div class="steps">
    ds
  </div> --}}

  <div class="card">
    <div class="card-header" data-background-color="blue">
      <h4 class="title">Nova Cidade</h4>
      <p class="category">Preencha todos os campos para proceguir</p>
    </div>
    <div class="card-content table-responsive">
      {{ Form::open(['url' => config('app.path_admin') . '/cidades', 'method' => 'post']) }}
        @if (Auth::user()->role->slug == 'administrator')
          <div class="row">
            <div class="col-md-4">
              <div class="form-group label-floating">
                <label class="control-label">Site</label>
                <select name="site_id" required class="selectpicker" title="Site">
                  @foreach($sites as $site)
                    <option value="{{ $site->id }}" @if (old('site_id') == $site->id)) selected @endif>{{ $site->title }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        @endif
        <div class="row">
          <div class="col-md-5">
            <div class="form-group label-floating">
              <label class="control-label">Nome da Cidade</label>
              <input type="text" value="{{ old('name') }}" required name="name" class="form-control"/>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group label-floating">
              <label class="control-label">Telefone</label>
              <input type="text" value="{{ old('phone') }}" required name="phone" class="form-control" data-mask="(00) 00000-00#0"/>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group label-floating">
              <label class="control-label">Estado</label>
              <select name="state_id" required class="selectpicker" title="Estado">
                @foreach($states as $state)
                  <option value="{{ $state->id }}" @if (old('state_id') == $state->id)) selected @endif>{{ $state->state }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-success">Criar Planos</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
@endsection
