@extends('admin.template', [
  'title_page' => 'Cidades'
]);


@section('content')
  <a href="{{ url(config('app.path_admin') . '/cidades/create') }}" class="btn btn-success btn-novo" rel="tooltip" data-placement="right" title="Adicionar nova cidade"><i class="material-icons">add</i></a>

  <div class="card">
    <div class="card-header" data-background-color="blue">
      <h4 class="title">Detalhes do Plano</h4>
      <p class="category">Preencha todos os campos para proceguir</p>
    </div>
    <div class="card-content table-responsive">
      {{ Form::open(['url' => config('app.path_admin') . '/cidades/' . $city->id, 'method' => 'put']) }}
        <h4> Cidade </h4>
        <div class="row">
          <div class="col-md-5">
            <div class="form-group label-floating">
              <label class="control-label">Nome da Cidade</label>
              <input type="text" value="{{ old('name') }}@if(!old('name')){{ $city->name }}@endif" required name="city[name]" class="form-control"/>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group label-floating">
              <label class="control-label">Telefone</label>
              <input type="text" value="{{ old('phone') }}@if(!old('phone')){{ $city->phone }}@endif" required name="city[phone]" class="form-control" data-mask="(00) 00000-00#0"/>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group label-floating">
              <label class="control-label">Estado</label>
              <select name="city[state_id]" required class="selectpicker" title="Estado">
                @foreach($states as $state)
                  <option value="{{ $state->id }}" @if (old('state_id') == $state->id || $state->id == $city->state_id) selected @endif>{{ $state->state }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label>Inativar / Ativar</label>
          <div class="press">
            <input type="checkbox" name="city[status]" id="checked" class="cbx hidden" @if ($city->status == 1) checked @endif/>
            <label for="checked" class="lbl"></label>
          </div>
        </div>

        <br><br>
        <?php
          $color = [
            'red',
            'blue',
            'orange'
          ];
          $key = 0;
        ?>
        @foreach ($packages as $jey => $package)
            <h4>Planos {{ $package->title }}</h4>
            <div class="row">
              @foreach ($package->plans as $tey => $plan)
                <?php $plan_price = $city->plan($plan->id); ?>
                <div class="col-md-3 card-plan">
                    <input type="hidden" value="{{ $plan_price->id }}" required name="plan_price[{{ $key }}][id]"/>
                    <div class="card">
                        <div class="card-header" data-background-color="{{ $color[$jey] }}">
                            <h3 style="margin: 0; text-align: center">
                                <input type="number" min="0" value="{{ $plan_price->mb }}" required name="plan_price[{{ $key }}][mb]" class="form-control" style="font-size: 1em; color: #fff; text-align: center" />
                                <span style="font-size: .6em">MB</span>
                            </h3>
                        </div>
                        <div class="card-content">
                            <div class="form-group label-floating" style="margin: 0">
                                <div class="row">
                                    <label class="col-md-4">Down.</label>
                                    <div class="col-input col-md-4">
                                        <input type="number" min="0" value="{{ $plan_price->download }}" required name="plan_price[{{ $key }}][download]" class="form-control"/>
                                    </div>
                                    <div class="col-input col-md-4">
                                        <select required class="selectpicker" name="plan_price[{{ $key }}][desc_download]">
                                            <option value="Mbps" @if($plan_price->desc_download == 'Mbps') selected @endif>Mbps</option>
                                            <option value="Kbps" @if($plan_price->desc_download == 'Kbps') selected @endif>Kbps</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group label-floating" style="margin: 0">
                                <div class="row">
                                    <label class="col-md-4">Upload</label>
                                    <div class="col-input col-md-4">
                                        <input type="number" min="0" value="{{ $plan_price->upload }}" required name="plan_price[{{ $key }}][upload]" class="form-control"/>
                                    </div>
                                    <div class="col-input col-md-4">
                                        <select required class="selectpicker" name="plan_price[{{ $key }}][desc_upload]">
                                            <option value="Mbps" @if($plan_price->desc_upload == 'Mbps') selected @endif>Mbps</option>
                                            <option value="Kbps" @if($plan_price->desc_upload == 'Kbps') selected @endif>Kbps</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group label-floating" style="margin: 0">
                                <div class="row">
                                    <label class="col-md-4">Limite</label>
                                    <div class="col-input col-md-8">
                                        <input type="text" value="{{ $plan_price->limit }}" required name="plan_price[{{ $key }}][limit]" class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group label-floating" style="margin: 0">
                                <div class="row">
                                    <label class="col-md-4">Preço</label>
                                    <div class="col-input col-md-8">
                                        <input type="text" value="{{ $plan_price->price }}" required name="plan_price[{{ $key }}][price]" class="form-control money"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group label-floating" style="margin: 0;">
                              <div class="checkbox">
                              	<label>
                                  <input type="checkbox" @if($plan_price->status) checked @endif name="plan_price[{{ $key }}][status]"/>
                              		Ativo
                              	</label>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    $key++;
                ?>
              @endforeach
            </div>
        @endforeach

        <div class="form-group">
          <button type="submit" class="btn btn-success">Salvar mudanças</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
@endsection
