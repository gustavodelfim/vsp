@extends('admin.template', [
    'title_page' => 'Solicitações'
]);


@section('content')

    <div class="card">
        <div class="card-header" data-background-color="blue">
            <h4 class="title">Lista de Solicitações</h4>
            <p class="category">Todas solicitações</p>
        </div>
        <div class="card-content table-responsive">
            @if(count($solicitations) > 0)
                <table class="table">
                    <thead class="text-primary">
                        <th>Data</th>
                        <th>Nome</th>
                        <th>Plano</th>
                        <th>Telefone</th>
                        <th>Cidade</th>
                        @if (Auth::user()->role->slug == 'administrator')
                            <th>Site</th>
                        @endif
                        <th>Status</th>
                        <th width="70">Ações</th>
                    </thead>
                    <tbody>
                        @foreach($solicitations as $key => $solicitation)
                            <tr>
                                <td> {{ $solicitation->created_at->format('d/m/Y') }} </td>
                                <td>{{ $solicitation->name }}</td>
                                <td>{{ $solicitation->mb_day }}mb</td>
                                <td>{{ $solicitation->phone }}</td>
                                <td>{{ $solicitation->city->name }}</td>
                                @if (Auth::user()->role->slug == 'administrator')
                                    <td>{{ $solicitation->site->title }}</td>
                                @endif
                                <td>
                                    @if ($solicitation->status ==  0)
                                        <span class="circle-status inativo"></span>
                                        Cancelado
                                    @elseif($solicitation->status == 1)
                                        <span class="circle-status warning"></span>
                                        Aguardando
                                    @else
                                        <span class="circle-status ativo"></span>
                                        Contactado
                                    @endif
                                </td>
                                <td class="td-actions text-right">
                                    <a href="{{ url(config('app.path_admin') . '/solicitacoes/' . $solicitation->id . '') }}" rel="tooltip" title="Ver Solicitação" class="btn btn-info btn-simple btn-xs">
                                        <i class="material-icons">visibility</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $solicitations->links() }}
            @else
                <p>Não há solicitações cadastrada
                @endif
            </div>
        </div>
    @endsection
