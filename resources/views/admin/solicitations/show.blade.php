@extends('admin.template', [
  'title_page' => 'Cidades'
]);


@section('content')

  <div class="card">
    <div class="card-header" data-background-color="blue">
      <h4 class="title">Solicitacao <span style="float:right">{{ $solicitation->created_at->format('d/m/Y') }} às {{ $solicitation->created_at->format('H:i') }}</span></h4>
      <p class="category">Dados</p>
    </div>
    <div class="card-content table-responsive">
      <div class="row">
        <div class="col-md-7 infos-solicitations">
            <h3><b>Nome:</b> {{ $solicitation->name }}<h3>
            <h4><b>Telefone:</b> {{ $solicitation->phone }}</h4>
            <p><b>Email:</b> {{ $solicitation->email }}</p>
            <p><b>Cep:</b> {{ $solicitation->cep }} ({{ $solicitation->city->name }})</p>
            <p><b>Plano:</b> {{ $solicitation->mb_day }}MB ({{ $solicitation->title_package_day }})</p>
            <br>
            <div class="infos">
                <b>Informações do plano:</b>
                <div class="string">
                    <?php echo $solicitation->description_day ?>
                </div>
            </div>
        </div>
        <div class="col-md-5">
          <br>
          {{ Form::open(['url' => config('app.path_admin') . '/solicitacoes/' . $solicitation->id, 'method' => 'put']) }}
            <div class="radio">
              <label class="color-warning">
            		<input type="radio" name="status" value="1" @if($solicitation->status == 1) checked @endif >
            		Aguardando
            	</label>
              <br>
              <label class="color-inativo">
            		<input type="radio" name="status" value="0" @if($solicitation->status == 0) checked @endif>
            		Cancelado
            	</label>
              <br>
              <label class="color-ativo">
            		<input type="radio" name="status" value="2" @if($solicitation->status == 2) checked @endif>
            		Contactado
            	</label>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-success" style="margin-left: 12px;">Salvar</button>
            </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
@endsection
