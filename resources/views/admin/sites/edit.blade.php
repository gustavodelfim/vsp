@extends('admin.template', [
  'title_page' => 'Editar Configurações'
])



@section('content')
  <div class="card" id="edit-sites">
    <div class="card-header" data-background-color="blue">
      <h4 class="title">{{ $site->title }} <span style="font-size: 14px">({{ $site->domain }})</span></h4>
      <p class="category">Preencha todos os campos</p>
    </div>
    <div class="card-content table-responsive">

      @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

      {{ Form::open(['url' => config('app.path_admin') . '/sites/' . $site->configuration->id, 'method' => 'put', 'enctype' => 'multipart/form-data']) }}
        <h3>Logos</h3>
        <div class="row">
          <div class="col-md-6">
            <label>Logo do Topo:</label>
            <div class="row">
              <div class="input-file form-group is-empty is-fileinput col-md-10">
                <input type="file" name="logo_top">
                <div class="input-group">
                  <input type="text" readonly="" class="form-control" placeholder="Clique aqui para Carregar">
                    <span class="icon">
                        <i class="material-icons">cloud_upload</i>
                    </span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="preview-image">
                <img src="{{ asset($site->configuration->logo_top) }}" alt="">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <label>Logo do Rodape:</label>
            <div class="row">
              <div class="input-file form-group is-empty is-fileinput col-md-10">
                <input type="file" name="logo_footer">
                <div class="input-group">
                  <input type="text" readonly="" class="form-control" placeholder="Clique aqui para Carregar">
                    <span class="icon">
                        <i class="material-icons">cloud_upload</i>
                    </span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="preview-image">
                <img src="{{ asset($site->configuration->logo_footer) }}" alt="">
              </div>
            </div>
          </div>
        </div>

        <br><br>
        <h3>Institucional</h3>
        <div class="form-group label-floating">
          <label class="control-label">Desde</label>
          <textarea required name="institutional_texto1" class="form-control" rows="6">{{ old('institutional_texto1') }}@if(!old('institutional_texto1')){{ $site->configuration->institutional_texto1 }}@endif</textarea>
        </div>
        <div class="form-group label-floating">
          <label class="control-label">O Crescimento</label>
          <textarea required name="institutional_texto2" class="form-control" rows="6">{{ old('institutional_texto2') }}@if(!old('institutional_texto2')){{ $site->configuration->institutional_texto2 }}@endif</textarea>
        </div>
        <div class="form-group label-floating">
          <label class="control-label">Com Excelência</label>
          <textarea required name="institutional_texto3" class="form-control" rows="6">{{ old('institutional_texto3') }}@if(!old('institutional_texto3')){{ $site->configuration->institutional_texto3 }}@endif</textarea>
        </div>

        <br><br>
        <h3>Contato</h3>
        <div class="row">
          <div class="col-md-5">
            <div class="form-group label-floating">
              <label class="control-label">Telefone</label>
              <input type="text" value="{{ old('phone') }}@if(!old('phone')){{ $site->configuration->phone }}@endif" required name="phone" class="form-control" data-mask="(00) 00#0-00000"/>
            </div>
          </div>
          <div class="col-md-7">
            <div class="form-group label-floating">
              <label class="control-label">Email (serve para receber os email do site)</label>
              <input type="email" value="{{ old('email_contact') }}@if(!old('email_contact')){{ $site->configuration->email_contact }}@endif" required name="email_contact" class="form-control" />
            </div>
          </div>
        </div>

        <br><br>
        <h3>Endereço</h3>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group label-floating">
              <label class="control-label">CEP</label>
              <input type="text" value="{{ old('cep') }}@if(!old('cep')){{ $site->configuration->cep }}@endif" required name="cep" class="form-control" data-mask="00.000-000"/>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-9">
            <div class="form-group label-floating">
              <label class="control-label">Logradouro</label>
              <input type="text" value="{{ old('street') }}@if(!old('street')){{ $site->configuration->street }}@endif" required name="street" class="form-control"/>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group label-floating">
              <label class="control-label">Número</label>
              <input type="text" value="{{ old('number') }}@if(!old('number')){{ $site->configuration->number }}@endif" required name="number" class="form-control"/>
            </div>
          </div>
        </div>
        <div class="form-group label-floating">
          <label class="control-label">Bairro</label>
          <input type="text" value="{{ old('neighborhood') }}@if(!old('neighborhood')){{ $site->configuration->neighborhood }}@endif" required name="neighborhood" class="form-control"/>
        </div>
        <div class="row">
          <div class="col-md-8">
            <div class="form-group label-floating">
              <label class="control-label">Cidade</label>
              <input type="text" value="{{ old('city') }}@if(!old('city')){{ $site->configuration->city }}@endif" required name="city" class="form-control"/>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group label-floating">
              <label class="control-label">Estado</label>
              <select name="state_id" required class="selectpicker" title="Estado">
                @foreach($states as $state)
                  <option value="{{ $state->id }}" @if (old('state_id') == $state->id || ($site->configuration->state_id == $state->id)) selected @endif>{{ $state->state }}</option>
                @endforeach
              </select>
          </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel-body">
                  <div class="form-group">
                    <button type="submit" class="btn btn-success">Salvar</button>
                  </div>
                </div>
            </div>
        </div>
      {{ Form::close() }}

    </div>
  </div>
@endsection
