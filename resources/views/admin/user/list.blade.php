@extends('admin.template', [
  'title_page' => 'Usuários'
])


@section('content')
  <a href="{{ url('admin/usuarios/create') }}" class="btn btn-success btn-novo" rel="tooltip" data-placement="right" title="Adicionar novo usuário"><i class="material-icons">add</i></a>

  <div class="card">
      <div class="card-header" data-background-color="blue">
          <h4 class="title">Lista de Usuários</h4>
          <p class="category">Todos Usuário</p>
      </div>
      <div class="card-content table-responsive">
          <table class="table">
              <thead class="text-primary">
                  <th>Nome</th>
                  <th>E-mail</th>
                  <th>Função</th>
                  <th>Status</th>
                  <th width="80">Ações</th>
              </thead>
              <tbody>
                @foreach($users as $user)
                  <tr>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>
                        @if ($user->role->slug == 'administrator')
                          {{ $user->role->role }}
                        @else
                          Gerencia <b>{{ $user->site->domain }}</b>
                        @endif
                      </td>
                      <td>
                        <span class="circle-status @if ($user->status) ativo @else inativo @endif"></span>
                        @if ($user->status) Ativo @else Inativo @endif
                      </td>
                      <td class="td-actions text-right">
                          <a href="{{ url('admin/usuarios/' . $user->id . '/edit') }}" rel="tooltip" title="Editar Usuário" class="btn btn-warning btn-simple btn-xs">
                              <i class="material-icons">edit</i>
                          </a>
                          @if ($user->status && Auth::user()->id != $user->id)
                            {{ Form::open(['url' => 'admin/usuarios/' . $user->id, 'method' => 'delete']) }}
                              <button type="button" rel="tooltip" title="Inativar Usuário" class="btn btn-danger btn-simple btn-xs" onclick="window.app.confirmDelete('Deseja Realmente inativar este usuário?', this); return false">
                                  <i class="material-icons">delete</i>
                              </button>
                            {{ Form::close() }}
                          @endif
                          @if (!$user->status)
                            {{ Form::open(['url' => 'admin/usuarios/activate', 'method' => 'post']) }}
                              <input type="hidden" name="id" value="{{ $user->id }}" />
                              <button type="button" rel="tooltip" title="Ativar Usuário" class="btn btn-success btn-simple btn-xs" onclick="window.app.confirmDelete('Deseja Realmente Ativar este usuário?', this); return false">
                                  <i class="material-icons">done</i>
                              </button>
                            {{ Form::close() }}
                          @endif
                      </td>
                  </tr>
                @endforeach
              </tbody>
          </table>

          {{ $users->links() }}
      </div>
  </div>
@endsection
