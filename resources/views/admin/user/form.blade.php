@extends('admin.template', [
  'title_page' => 'Usuários'
])

@section('content')
  <div class="card">
    <div class="card-header" data-background-color="blue">
      <h4 class="title">{{ $page['title'] }}</h4>
      <p class="category">Preencha todos os campos</p>
    </div>
    <div class="card-content table-responsive">
      @if ($page['slug'] == 'new')
        {{ Form::open(['url' => config('app.path_admin') . '/usuarios', 'method' => 'post']) }}
      @else
        {{ Form::open(['url' => config('app.path_admin') . '/usuarios/' . $user->id, 'method' => 'put']) }}
      @endif
      	<div class="form-group">
          	<input type="text" value="{{ old('name') }}@if (!old('name') && isset($user)){{ $user->name }}@endif" required name="name" placeholder="Nome Completo" class="form-control" />
      	</div>
        <div class="form-group @if($errors->has('email')) has-error @endif">
          	<input type="email" value="{{ old('email') }} @if (!old('email') && isset($user)) {{ $user->email }} @endif" required name="email" placeholder="E-mail" class="form-control" />
            <span class="label-error">{{ $errors->first('email') }}</span>
      	</div>
        <div class="form-group">
          <select name="site_id" required class="selectpicker" title="Função">
            @foreach($sites as $site)
              <option value="{{ $site->id }}" @if (old('site_id') == $site->id || (isset($user) && $user->site->id == $site->id)) selected @endif>{{ $site->domain }}</option>
            @endforeach
            <option value="adm" @if (old('site_id') == 'adm' || (isset($user) && $user->role->slug == 'administrator')) selected @endif>Administrador Master</option>
          </select>
        </div>
        @if ($page['slug'] == 'edit')
          <div class="checkbox">
          	<label>
          		<input type="checkbox" name="checkbox_password" onclick="window.app.toogleBox('box-password')" @if (old('checkbox_password')) checked @endif>
          		Editar Senha
          	</label>
          </div>
        @endif
        <div id="box-password" class="row" @if ($page['slug'] == 'edit' && !old('checkbox_password')) style="display: none" @endif>
          <div class="col-md-6">
            <div class="form-group @if ($errors->has('password')) has-error @endif">
              <input type="password" @if ($page['slug'] == 'new') required @endif name="password" placeholder="Senha" class="form-control" />
              <span class="label-error">{{ $errors->first('password') }}</span>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group @if($errors->has('password_confirmation')) has-error @endif">
              <input type="password" @if ($page['slug'] == 'new') required @endif name="password_confirmation" placeholder="Confirmar Senha" class="form-control" />
              <span class="label-error">{{ $errors->first('password_confirmation') }}</span>
            </div>
          </div>
        </div>
        @if (!isset($user) || (isset($user) && $user->status && Auth::user()->id != $user->id))
          <div class="form-group">
            <label>Inativar / Ativar</label>
            <div class="press">
              <input type="checkbox" name="status" id="checked" class="cbx hidden" @if ((isset($user) && $user->status == 1) || !isset($user)) checked @endif/>
              <label for="checked" class="lbl"></label>
            </div>
          </div>
        @endif
        <div class="form-group">
          <button type="submit" class="btn btn-success">Salvar</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
@endsection
