@extends('admin.template', [
  'title_page' => 'Institucional'
])


@section('content')
  <div class="card">
    <div class="card-header" data-background-color="blue">
      <h4 class="title">Sobre a Empresa</h4>
      <p class="category">Encreva sobre a Empresa</p>
    </div>
    <div class="card-content table-responsive">
      {{ Form::open(['url' => config('app.path_admin') . '/institucional/' . $config->id, 'method' => 'put']) }}

        <div class="form-group label-floating">
          <label class="control-label">Desde</label>
          <textarea required name="institutional_texto1" class="form-control" rows="15">{{ old('institutional_texto1') }}@if(!old('institutional_texto1')){{ $config->institutional_texto1 }}@endif</textarea>
        </div>
        <div class="form-group label-floating">
          <label class="control-label">O Crescimento</label>
          <textarea required name="institutional_texto2" class="form-control" rows="15">{{ old('institutional_texto2') }}@if(!old('institutional_texto2')){{ $config->institutional_texto2 }}@endif</textarea>
        </div>
        <div class="form-group label-floating">
          <label class="control-label">Com Excelência</label>
          <textarea required name="institutional_texto3" class="form-control" rows="15">{{ old('institutional_texto3') }}@if(!old('institutional_texto3')){{ $config->institutional_texto3 }}@endif</textarea>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-success">Salvar</button>
        </div>
      {{ Form::close() }}
    </div>
  </div>
@endsection
