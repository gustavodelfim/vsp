@extends('admin.template', [
  'title_page' => 'Cadastro Matriz'
])



@section('content')
  <div class="card" id="edit-sites">
    <div class="card-header" data-background-color="blue">
      <h4 class="title">Matriz</h4>
      <p class="category">Preencha todos os campos</p>
    </div>
    <div class="card-content table-responsive">

      @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif

      {{ Form::open(['url' => config('app.path_admin') . '/matriz/' . $matriz->id, 'method' => 'put']) }}


        <br><br>
        <h3>Contato</h3>
        <div class="row">
          <div class="col-md-5">
            <div class="form-group label-floating">
              <label class="control-label">Telefone</label>
              <input type="text" value="{{ old('phone') }}@if(!old('phone')){{ $matriz->phone }}@endif" required name="phone" class="form-control"/>
            </div>
          </div>
        </div>

        <br><br>
        <h3>Endereço</h3>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group label-floating">
              <label class="control-label">CEP</label>
              <input type="text" value="{{ old('cep') }}@if(!old('cep')){{ $matriz->cep }}@endif" required name="cep" class="form-control" data-mask="00.000-000"/>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-9">
            <div class="form-group label-floating">
              <label class="control-label">Logradouro</label>
              <input type="text" value="{{ old('street') }}@if(!old('street')){{ $matriz->street }}@endif" required name="street" class="form-control"/>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group label-floating">
              <label class="control-label">Número</label>
              <input type="text" value="{{ old('number') }}@if(!old('number')){{ $matriz->number }}@endif" required name="number" class="form-control"/>
            </div>
          </div>
        </div>
        <div class="form-group label-floating">
          <label class="control-label">Bairro</label>
          <input type="text" value="{{ old('neighborhood') }}@if(!old('neighborhood')){{ $matriz->neighborhood }}@endif" required name="neighborhood" class="form-control"/>
        </div>

        <div class="row">
          <div class="col-md-8">
            <div class="form-group label-floating">
              <label class="control-label">Cidade</label>
              <input type="text" value="{{ old('city') }}@if(!old('city')){{ $matriz->city }}@endif" required name="city" class="form-control"/>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group label-floating">
              <label class="control-label">Estado</label>
              <select name="uf" required class="selectpicker" title="Estado">
                @foreach($states as $state)
                  <option value="{{ $state->uf }}" @if (old('uf') == $state->uf || ($matriz->uf == $state->uf)) selected @endif>{{ $state->state }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="panel-body">
          <div class="form-group">
            <button type="submit" class="btn btn-success">Salvar</button>
          </div>
        </div>
      {{ Form::close() }}

    </div>
  </div>
@endsection
