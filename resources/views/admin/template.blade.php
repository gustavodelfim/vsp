<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Admin {{ config('app.name') }} - {{ $title_page }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="{{ asset('css/admin/admin.css?v').time() }}" rel="stylesheet" />

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="blue" data-image="{{ asset('img/sidebar-1.jpg') }}">
            <!--
                Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"
                Tip 2: you can also add an image using data-image tag
            -->
            <div class="logo">
                <a href="{{ url(config('app.path_admin')) }}" class="simple-text">
                    {{ Auth::user()->site->title }}
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="{{ Request::is('admin') ? 'active' : '' }}">
                        <a href="{{ url('admin') }}">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/solicitacoes') ? 'active' : '' }}">
                        <a href="{{ url('admin/solicitacoes') }}">
                            <i class="material-icons">content_paste</i>
                            <p>Solicitações</p>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/cidades') ? 'active' : '' }}">
                        <a href="{{ url('admin/cidades') }}">
                            <i class="material-icons">location_on</i>
                            <p>Cidades</p>
                        </a>
                    </li>
                    @if (Auth::user()->role->slug == 'collaborator')
                      <li class="{{ Request::is('admin/institucional') ? 'active' : '' }}">
                          <a href="{{ url('admin/institucional') }}">
                              <i class="material-icons">home</i>
                              <p>Institucional</p>
                          </a>
                      </li>
                    @endif

                  <li class="{{ Request::is('admin/planos-padroes') ? 'active' : '' }}">
                      <a href="{{ url('admin/planos-padroes') }}">
                          <i class="material-icons">card_giftcard</i>
                          <p>Planos Padrões</p>
                      </a>
                  </li>

                    @if (Auth::user()->role->slug == 'collaborator')
                      <li class="{{ Request::is('admin/matriz') ? 'active' : '' }}">
                          <a href="{{ url('admin/matriz') }}">
                              <i class="material-icons">settings</i>
                              <p>Matriz</p>
                          </a>
                      </li>
                    @endif
                    @if (Auth::user()->role->slug == 'administrator')
                      <li class="{{ Request::is('admin/sites') ? 'active' : '' }}">
                          <a href="{{ url('admin/sites') }}">
                              <i class="material-icons">http</i>
                              <p>Sites</p>
                          </a>
                      </li>
                    @endif
                    @if (Auth::user()->role->slug == 'administrator')
                      <li class="{{ Request::is('admin/usuarios') ? 'active' : '' }}">
                          <a href="{{ url('admin/usuarios') }}">
                              <i class="material-icons">person</i>
                              <p>Usuários</p>
                          </a>
                      </li>
                    @endif
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> {{ $title_page }} </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="{{ url('admin/logout') }}">
                                    <p>Sair</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">

                    @yield('content')

                </div>
            </div>
        </div>
    </div>
</body>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
<script src="{{ asset('js/admin.js') }}"></script>

@if(session()->has('notification'))
  <script type="text/javascript">
    window.app.showNotification('{{ session()->get("notification") }}', '{{ session()->get('message') }}')
  </script>
@endif

</html>
