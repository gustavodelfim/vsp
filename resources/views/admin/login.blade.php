<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Admin {{ config('app.name') }} - Login</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/admin/login.css') }}" rel="stylesheet" />

    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
  <div class="wrapper">
    <div class="container">

      <div id="login">
        <div class="col-md-6 col-sm-offset-3">
          <div class="card">
            <div class="clear card-header" data-background-color="blue">
              Login
            </div>
            <div class="clear card-content">
              {{ Form::open(['url' => 'admin/logar', 'method' => 'post']) }}
                <div class="row">
                  <div class="col-md-11 col-sm-11 col-xs-12">
                    <div class="input-group">
                  		<span class="input-group-addon">
                  			<i class="fa fa-user"></i>
                  		</span>
                  		<input type="email" required name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                  	</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-11 col-sm-11 col-xs-12">
                    <div class="input-group">
                  		<span class="input-group-addon">
                  			<i class="fa fa-lock"></i>
                  		</span>
                  		<input type="password" required name="password" class="form-control" placeholder="Senha">
                  	</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-11">
                    <button class="btn btn-info">Entrar</button>
                  </div>
                </div>

                @if ($errors->any())
                  <br>
                  <div class="col-md-12 col-sm-12 col-xs-12 alerta">
                    <div class="panel-body">
                      <div class="panel-body">
                        <div class="alert alert-danger">
                          <div class="alert-icon">
                            <i class="material-icons">error_outline</i>
                          </div>
                          Email ou senha Incorretos
                        </div>
                      </div>
                    </div>
                  </div>
                @endif

              {{ Form::close() }}
              <br>
            </div>
            <div class="clear card-footer">
              <div class="stats">
                Todos direitos reservados
                <script>
                  document.write(new Date().getFullYear())
                </script>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
<script src="{{ asset('js/admin.js') }}"></script>
</html>
