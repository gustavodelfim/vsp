@extends('welcome', [
  'title_page' => 'Planos'
])

@section('content')
  <div class="row page" id="planos">
    <div class="col-xs-12">
      <div class="container">
        <p>
          Navegue a vontade pela internet, sem precisar se preocupar com o limite da sua franquia.<br>
          Assista vídeos, ouça músicas, faça download, pesquisas e muito mais!
        </p>

        <div class="row planos">
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano residencial">
              <div class="sprite residencial"></div>
              <p>
                Navegue a vontade pela internet, sem precisar se preocupar com o limite da sua franquia. Assista vídeos, ouça músicas, faça download, pesquisas e muito mais!
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/residencial.html">3MB</a></li>
                <li><a href="planos/residencial.html">5MB</a></li>
                <li><a href="planos/residencial.html">8MB</a></li>
              </ul>
              <a href="planos/residencial.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano fibra-residencial">
              <div class="sprite fibra-residencial"></div>
              <p>
                Downloads com alta velocidade, páginas com
                carregamento rápido, vídeos sem pausa e muito
                mais. Com o plano fibra residencial tudo fica
                mais fácil, rápido e simples.
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/fibra-residencial.html">10MB</a></li>
                <li><a href="planos/fibra-residencial.html">15MB</a></li>
                <li><a href="planos/fibra-residencial.html">20MB</a></li>
                <li><a href="planos/fibra-residencial.html">50MB</a></li>
              </ul>
              <a href="planos/fibra-residencial.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano empresarial">
              <div class="sprite empresarial"></div>
              <p>
                Tenha total segurança em um serviço de internet
                que possui preço justo, que é veloz, ilimitado
                e conta com rápida manutenção.
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/empresarial.html">3MB</a></li>
                <li><a href="planos/empresarial.html">5MB</a></li>
                <li><a href="planos/empresarial.html">8MB</a></li>
              </ul>
              <a href="planos/empresarial.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano fibra-empresarial">
              <div class="sprite fibra-empresarial"></div>
              <p>
                Quer mais velocidade para sua empresa? Aqui
                você tem! Usufrua da rede de transmissão de
                dados mais veloz que existe e tenha o melhor o
                serviço de internet com tecnologia em fibra
                óptica.
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/fibra-empresarial.html">10MB</a></li>
                <li><a href="planos/fibra-empresarial.html">15MB</a></li>
                <li><a href="planos/fibra-empresarial.html">20MB</a></li>
                <li><a href="planos/fibra-empresarial.html">50MB</a></li>
              </ul>
              <a href="planos/fibra-empresarial.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano rural">
              <div class="sprite rural"></div>
              <p>
                Seja no campo ou na cidade, vamos até você! Na
                VSP não há desculpas, não há limite de dados e
                nem barreiras territoriais.
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/rural.html">2MB</a></li>
                <li><a href="planos/rural.html">4MB</a></li>
              </ul>
              <a href="planos/rural.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano condominio">
              <div class="sprite condominio"></div>
              <p>
                Tenha uma internet de alta velocidade, sem
                limite de franquia de dados e sem surpresas na
                fatura! Com o Plano Condomínio o seu desejo é
                realidade.
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/condominio.html">10MB</a></li>
                <li><a href="planos/condominio.html">15MB</a></li>
                <li><a href="planos/condominio.html">20MB</a></li>
              </ul>
              <a href="planos/condominio.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano dedicado">
              <div class="sprite dedicado"></div>
              <p>Diga adeus a rede compartilhada, com o Plano
                Dedicado você possui uma ligação direta com o
                provedor. Agora você tem estabilidade de sinal
                e garantia da velocidade contratada!
              </p>
              <p>
                A partir de R$ 199,00 o mega. Consulte um dos
                nossos vendedores para saber a sua real
                necessidade.
              </p>
              <a href="planos.html#velocidade" data-toggle="modal" data-id="0" data-plano-id="9" data-nome="DEDICADO" class="btn-mais">CONTRATE AGORA</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="velocidade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="row">
            <div class="col-xs-12">
              <h1></h1>
              <p>
                Preencha seus dados abaixo que em breve entraremos em contato.
              </p>
              <form role="form" method="post" id="formVelocidade" name="formVelocidade">
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                      <label for="nome">Nome:</label>
                      <input type="hidden" class="form-control" id="velocidade_id" name="velocidade_id">
                      <input type="hidden" class="form-control" id="plano_id" name="plano_id">
                      <input type="text" class="form-control" id="nome" name="nome">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                      <label for="email">E-mail:</label>
                      <input type="text" class="form-control" id="email" name="email">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                      <label for="telefone">Fone:</label>
                      <input type="text" class="form-control phone" id="telefone" name="telefone">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                      <label for="cep">CEP:</label>
                      <input type="text" class="form-control cep" id="cep" name="cep">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <span class="status"></span>
                  </div>
                  <div class="col-sm-12 col-md-12 text-center">
                    <button type="button" class="btn btn-default">ENVIAR</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
