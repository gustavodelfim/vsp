@extends('welcome', [
  'page' => 'Atendimento'
])

@section('content')
  <section id="atendimento">
    <div class="clear frase">
      <div class="titulos">
        <h2>Atendimento</h2>
        <p>Todo o suporte que você necessita está aqui.</p>
      </div>
    </div>

    <div class="clear infos">
      <div class="clear titulo">
        <span class="icon icon-moca-atendimento"></span>
        <p> Fale conosco </p>
      </div>
      <div class="clear">
        <div class="container">
          <div class="divs esquerda">
            <p>
              Ligue pra gente<br>
              <span> {{ session('site')['phone'] }} </span>
            </p>
          </div>
          <div class="divs centro">
            <span class="separador"></span>
            <p>
              Alguma dúvida?<br>
              Preencha o formulário
            </p>
          </div>
          <div class="divs direita">
            <span class="separador"></span>
            <p>
              Venha nos visitar<br>
              Ver no mapa
            </p>
          </div>
        </div>
      </div>
      <div class="clear texto">
        <p> Entraremos em contato o mais breve possível para responder suas perguntas. </p>
      </div>
    </div>

    <div class="clear form" id="form">
      <div class="clear titulos">
        <span class="icon icon-carta"></span>
        <h2> Tire suas dúvidas </h2>
        <p> Envie-nos uma mensagem. Seu contato será rapidamente atendido. </p>
      </div>
      <div class="container">
        {{ Form::open(['url' => config('app.param_prefix') . 'atendimento', 'method' => 'post', 'onSubmit' => 'return Contato.submit(this)']) }}
        <div class="clear">
          <div class="row">
            <div class="col-md-6">
              <input type="text" required name="nome" placeholder="Nome" required>
            </div>
            <div class="col-md-6">
              <input type="text" required name="sobrenome" placeholder="Sobrenome" required>
            </div>
          </div>
        </div>
        <div class="clear">
          <input type="text" required name="email" placeholder="Email" required>
        </div>
        <div class="clear">
          <input type="text" required name="telefone" placeholder="Telefone" required data-mask="(00) 00000-00#0">
        </div>
        <div class="clear">
          <input type="text" required name="assunto" placeholder="Assunto" required>
        </div>
        <div class="clear">
          <textarea required name="mensagem" placeholder="Escreva sua Mensagem"></textarea>
        </div>
        <div class="botao">
          <button type="submit" name="button">ENVIAR MENSAGEM</button>
        </div>
        {{ Form::close() }}
      </div>
    </div>

    @include('components.faq')

    @include('components.entre-nessa')
  </section>
@endsection


@push('js')
  <script src="{{ asset('js/app/plugins/jquery.mask.min.js') }}"></script>
  <script src="{{ asset('js/app/contato.js') }}"></script>
@endpush
