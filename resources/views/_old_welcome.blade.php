<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <!--<base href="/vsp/">-->

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="  ">
  <meta name="abstract" content="  ">
  <meta name="keywords" content="  ">
  <link rel="canonical" href="index.html" />

  <title>{{ config('app.name') }} - {{ $title_page }}</title>

  <link rel="stylesheet" href="{{ asset('css/app/app.css') }}">


  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <style>
    #topo .wrap-logo-info {
      background-color: {{ session('site')['color_bottom_top'] }} !important
    }
    #topo .wrap-logo-info .wrap-info p,
    #topo .wrap-logo-info .wrap-info p span {
      color: {{ session('site')['color_text_top'] }} !important;
    }
    #topo .wrap-logo-info .wrap-info .btn-area {
      background-color: {{ session('site')['color_button_top'] }} !important;
      color: {{ session('site')['color_text_button_top'] }} !important;
    }

    #rodape {
      background: {{ session('site')['color_bottom_footer'] }} !important;
    }
    #rodape .wrap-info p,
    #rodape .wrap-info .menu-rodape li a {
      color: {{ session('site')['color_text_footer'] }} !important;
    }
  </style>
</head>

<body>

  <div class="modal fade" id="escolha-cidade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-8">
              <div class="sprite logo-modal"></div>
              <h1>seja bem vindo(a)</h1>
              <form id="formCidade" name="formCidade">
                <input type="hidden" name="tipo" id="tipo" value="log">
                <select name="cidade" id="cidade">
                  <option value="">SELECIONE SUA CIDADE</option>
                  <option value="3">Anastacio</option>
                  <option value="2">Aquidauana</option>
                  <option value="1">Campo Grande</option>
                  <option value="4">Corguinho</option>
                  <option value="5">Dois Irmaos do Buriti</option>
                  <option value="6">Maracaju</option>
                  <option value="7">Rio Negro</option>
                  <option value="8">Rochedo</option>
                  <option value="9">São Gabriel do Oeste</option>
                  <option value="10">Sidrolandia</option>
                  <option value="11">Terenos</option>
                </select>
                <button type="button">ENTRAR</button>
              </form>
              <p>
                CENTRAL DE ATENDIMENTO<br>
                <span>(67) 3341-0333</span>
              </p>
            </div>
            <div class="col-sm-4">
              <a href="index.html#" class="btn-boleto" target="_blank">
                <div class="sprite boleto hover-effect no-hover">
                  <span></span>
                </div>
                Emissão de<br>
                2° via de Boleto
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="wrapper">
    <div class="row" id="topo">
      <div class="col-xs-12">
        <div class="wrap-logo-info hidden-xs">
          <div class="container">
            <div class="logo hover-effect">
              <img src="{{ asset(session('site')['logo_top']) }}" alt="">
            </div>

            <div class="wrap-info">
              <p>Contrare já: <span>{{ session('site')['phone'] }}</span></p>
              <a href="index.html#" class="btn-area">Área do Cliente</a>
            </div>
          </div>
        </div>
        <div class="wrap-menu hidden-xs">
          <div class="container">
            <ul class="menu">
              @include('links_menu')
            </ul>
          </div>
        </div>
        <nav class="navbar navbar-default visible-xs" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">
              <div class="logo-rodape">
                <img src="{{ asset(session('site')['logo_footer']) }}" alt="">
              </div>
            </a>
          </div>

          <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
              @include('links_menu')
            </ul>
          </div>
        </nav>
      </div>
    </div>
    <div class="row" id="main">

      @yield('content')

    </div> <!-- Fim Main -->
  </div> <!-- Fim Wrapper-->

  <div class="row" id="rodape">
    <div class="col-xs-12">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 wrap-info">
            <div class="logo-rodape">
              <img src="{{ asset(session('site')['logo_footer']) }}" alt="">
            </div>
            <p>
              <span>{{ session('site')['phone'] }}</span><br>
              {{ session('site')['street'] }}, {{ session('site')['number'] }}<br>
              {{ session('site')['neighborhood'] }}, CEP: {{ session('site')['cep'] }} - {{ session('site')['city'] }} - {{ session('site')['state']['uf'] }}
            </p>
            <ul class="menu-rodape">
              @include('links_menu')
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <small>Copyright 2017 &copy; VSP Internet</small>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
  <script src="http://www.google.com/recaptcha/api.js"></script>
  <script src="{{ asset('js/app/plugins.js') }}"></script>
  <script src="{{ asset('js/app/script.js') }}"></script>
</body>
</html>
