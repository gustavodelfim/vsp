@extends('welcome', [
  'page' => 'Emitir segunrada via Fatura'
])

@section('content')
  <section id="institucional">
      <object data="{{ config('app.configs.' .session('site')['slug']. '.sistema_externo') }}" style="width: 100%; min-height: 650px"></object>
  </section>
@endsection
