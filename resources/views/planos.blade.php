@extends('welcome', [
  'page' => 'Planos'
])

@section('content')
  <section class="clear" id="planos">
    <div class="titulos clear">
      <h2> Nossos planos </h2>
      <p> Diferentes planos para sua necessidade. Conect-se com a {{ session('site')['title'] }}! </p>
    </div>
    <p class="clear descricao"> Não sabe qual o melhor plano pra você? <a href="{{ url(config('app.param_prefix') . 'atendimento') }}">A {{ session('site')['title'] }} INDICA! <span></span></a> </p>

    <div class="clear planos">
      <div class="container">
        <div class="clear center @if(count($packages) <= 3) center-full @endif">
          <div class="clear slide">

            <button type="button" class="arrows prev @if (count($packages) <= 3) hidden @endif" data-arrow="next"><span class="icon icon-seta"></span></button>
            <button type="button" class="arrows next @if (count($packages) <= 3) hidden @endif" data-arrow="prev"><span class="icon icon-seta"></span></button>

            <div class="slide-plano">
              <?php $i = 1; ?>
              @foreach ($packages as $key => $package)
                @if (count($package->plansDesc) > 0)
                <div class="@if($key==1) plano-active @endif">
                  <div class="panel-body">
                    <div class="clear plano" data-index-package="{{ $key }}">
                      <span class="icon icon-icon{{ $i }}-planos icon-{{ $i }}"></span>
                      <h5> {{ $package->title }} </h5>
                      <p class="desc">
                        <?php
                          $texto = $package->description;
                          $texto = str_replace('VSP', session('site')['title'], $texto);
                          echo $texto;
                        ?>
                      </p>
                      <p class="mb">
                        <span class="sinais menos" data-step="{{$key}},0" data-sinal="0" data-toggle="tooltip" data-placement="right" title="Menos Megas">-</span>
                        <span class="price"> <span class="number">{{ $package->plansDesc[0]->tablePrice->mb }}</span> <b>MB</b></span>
                        <span class="sinais mais" data-step="{{$key}},1" data-sinal="1" data-toggle="tooltip" data-placement="right" title="Mais Megas">+</span>
                      </p>
                      <p class="desc-price"> Por apenas R$ <span>{{ str_replace('.', ',', $package->plansDesc[0]->tablePrice->price) }}</span> </p>
                      <span class="line"><span></span></span>
                      <div class="infos">
                        <p class="download"><span>Download</span> de <i>{{ $package->plansDesc[0]->tablePrice->download . $package->plansDesc[0]->tablePrice->desc_download }}</i></p>
                        <p class="upload"><span>Upload</span> de <i>{{ $package->plansDesc[0]->tablePrice->upload . $package->plansDesc[0]->tablePrice->desc_upload }}</i></p>
                        <p class="limit"><span><b>{{ $package->plansDesc[0]->tablePrice->limit }}</b></span> de franquia</p>
                      </div>
                      <div class="botao">
                        <a href="javascript:;" onclick="Planos.solicitar(this)" data-step="{{$key}},0">SOLICITAR PLANO</a>
                      </div>
                    </div>
                  </div>

                  <input type="hidden" name="package[{{ $key }}]" value="{{ $package->toJson() }}" style="display: none" class="packages">
                </div>
                @endif
                <?php $i++ ?>
              @endforeach
            </div>
          </div>
      </div>
  </section>

  <!-- Modal -->
  <div class="modal fade modal-planos" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">

      {{ Form::open(['url' => config('app.param_prefix') . 'solicitar', 'method' => 'post', 'onSubmit' => 'return Planos.submit(this)']) }}
        <input type="hidden" class="plan_id" name="plan_id" value="">
        <input type="hidden" class="mb_day" name="mb_day" value="">
        <input type="hidden" class="price_day" name="price_day" value="">
        <input type="hidden" class="description_day" name="description_day" value="">
        <input type="hidden" class="title_package_day" name="title_package_day" value="">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group row">
              <label for="name" class="col-sm-2 col-form-label">Nome</label>
              <div class="col-sm-10">
                <input type="text" required name="name" class="form-control" id="name">
              </div>
            </div>
            <div class="form-group row">
              <label for="email" class="col-sm-2 col-form-label">Email</label>
              <div class="col-sm-10">
                <input type="email" required name="email" class="form-control" id="email">
              </div>
            </div>
            <div class="form-group row">
              <label for="phone" class="col-sm-2 col-form-label">Fone</label>
              <div class="col-sm-10">
                <input type="text" required name="phone" class="form-control" id="phone" data-mask="(00) 00000-000#0">
              </div>
              </div>
            <div class="form-group row">
              <label for="cep" class="col-sm-2 col-form-label">CEP</label>
              <div class="col-sm-10">
                <input type="text" required name="cep" class="form-control" id="cep" data-mask="00.000-000">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Solicitar</button>
          </div>
        </div>
      {{ Form::close() }}
    </div>
  </div>

  @include('components.midias')

  @include('components.faq')

  @include('components.entre-nessa')
@endsection


@push('js')
  <script src="{{ asset('js/app/plugins/jquery.mask.min.js') }}"></script>
  <script src="{{ asset('js/app/planos.js') }}"></script>
@endpush
