@extends('welcome', [
  'title_page' => 'Home'
])

@section('content')
  <div class="banner">
    <div class="slider">
      <div class="slide">
        <img src="{{ asset('images/banners/banner-home.jpg') }}" class="img-responsive">
        <div class="wrap-selo">
          <div class="container">
            <p>
              PLANOS PENSADOS PARA VOCÊ
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row page" id="home">
    <div class="col-xs-12">
      <div class="container">
        <div class="row planos">
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano">
              <div class="sprite residencial"></div>
              <p>
                Navegue a vontade pela internet, sem precisar se preocupar com o limite da sua franquia. Assista vídeos, ouça músicas, faça download, pesquisas e muito mais!
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/residencial.html">3MB</a></li>
                <li><a href="planos/residencial.html">5MB</a></li>
                <li><a href="planos/residencial.html">8MB</a></li>
              </ul>
              <a href="planos/residencial.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano">
              <div class="sprite fibra-residencial"></div>
              <p>
                Downloads com alta velocidade, páginas com
                carregamento rápido, vídeos sem pausa e muito
                mais. Com o plano fibra residencial tudo fica
                mais fácil, rápido e simples.
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/fibra-residencial.html">10MB</a></li>
                <li><a href="planos/fibra-residencial.html">15MB</a></li>
                <li><a href="planos/fibra-residencial.html">20MB</a></li>
                <li><a href="planos/fibra-residencial.html">50MB</a></li>
              </ul>
              <a href="planos/fibra-residencial.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano">
              <div class="sprite empresarial"></div>
              <p>
                Tenha total segurança em um serviço de internet
                que possui preço justo, que é veloz, ilimitado
                e conta com rápida manutenção.
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/empresarial.html">3MB</a></li>
                <li><a href="planos/empresarial.html">5MB</a></li>
                <li><a href="planos/empresarial.html">8MB</a></li>
              </ul>
              <a href="planos/empresarial.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 plano">
            <div class="wrap-plano">
              <div class="sprite fibra-empresarial"></div>
              <p>
                Quer mais velocidade para sua empresa? Aqui
                você tem! Usufrua da rede de transmissão de
                dados mais veloz que existe e tenha o melhor o
                serviço de internet com tecnologia em fibra
                óptica.
              </p>
              <ul class="wrap-velocidades">
                <li><a href="planos/fibra-empresarial.html">10MB</a></li>
                <li><a href="planos/fibra-empresarial.html">15MB</a></li>
                <li><a href="planos/fibra-empresarial.html">20MB</a></li>
                <li><a href="planos/fibra-empresarial.html">50MB</a></li>
              </ul>
              <a href="planos/fibra-empresarial.html" class="btn-mais">SAIBA MAIS</a>
            </div>
          </div>
          <div class="col-xs-12 plano">
            <a href="planos.html" class="btn-todos">CONHEÇA TODOS OS PLANOS</a>
          </div>
        </div>

        <div class="row caracteristicas">
          <div class="col-xs-4 col-sm-3 caracteristica">
            <div class="sprite upload"></div>
            <p>
              alta velocidade
              de upload
            </p>
          </div>
          <div class="col-xs-4 col-sm-3 caracteristica">
            <div class="sprite franquia"></div>
            <p>
              sem limite
              de franquia
            </p>
          </div>
          <div class="col-xs-4 col-sm-3 caracteristica">
            <div class="sprite velocidades"></div>
            <p>
              velocidades de
              1 mb à 20 mb
            </p>
          </div>
          <div class="col-xs-4 col-xs-offset-2 col-sm-3 col-sm-offset-0 caracteristica">
            <div class="sprite wifi"></div>
            <p>
              rede wi-fi
              incluída
            </p>
          </div>
          <div class="col-xs-4 col-sm-3 caracteristica">
            <div class="sprite suporte"></div>
            <p>
              suporte técnico
              especializado
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
