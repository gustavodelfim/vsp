@extends('welcome', [
  'title_page' => 'Cobertura'
])

@section('content')
  <div class="row page" id="cobertura">
    <div class="col-xs-12">
      <div class="row wrap-maps">
        <div class="col-xs-12">
          <div class="maps" id="map"></div>
        </div>
      </div>

      <script>
      var map;
      var center = {lat: -20.30628586, lng: -54.54705611};
      var cordenadas = [
        {lat: -20.48251695, lng: -54.6004656},
        {lat: -20.37037726, lng: -55.78033447},
        {lat: -19.8279928, lng: -54.8313534},
        {lat: -21.60989038, lng: -55.17057303},
        {lat: -19.9533323, lng: -54.8882885},
        {lat: -19.3872935, lng: -54.5930467},
        {lat: -20.9306687, lng: -54.9697212},
        {lat: -20.48614922, lng: -55.80800107},
        {lat: -20.68934828, lng: -55.28993265},
        {lat: -19.4474513, lng: -54.98684995},
        {lat: -20.4383001, lng: -54.8652419},
      ];
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: center,
          zoom: 7
        });

        for (var i = 0; i < cordenadas.length; i++) {
          var cord = cordenadas[i];

          var marker = new google.maps.Marker({
            position: {lat: cord.lat, lng: cord.lng},
            map: map,
            icon: {
              url: './css/img/marker.png',
              size: new google.maps.Size(85, 50),
              anchor: new google.maps.Point(0, 50)
            }
          });
        }
      }
      </script>

      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgzDu0d3LhwKKfdyy5k2wyC3DEvvDFZGY&callback=initMap" async defer></script>
      <div class="container">
        <div class="row cidades">
          <div class="col-xs-12">
            <h1>Mato Grosso do Sul</h1>
          </div>
          <div class="col-xs-12 col-sm-6">
            <h2>Matriz:</h2>
            <p class="matriz">
              <span>- Campo Grande</span> <span class="right">- <b>(67)3341-0333</b></span>
            </p>
          </div>
        </div>
        <div class="row cidades">
          <div class="col-xs-12 col-sm-6">
            <p>
              <span>- Aquidauana</span> <span class="right">- <b>(67) 3241-9008</b></span>
            </p>
            <p>
              <span>- Anastacio</span> <span class="right">- <b>(67) 3241-9008</b></span>
            </p>
            <p>
              <span>- Corguinho</span> <span class="right">- <b>(67) 3250-1578</b></span>
            </p>
            <p>
              <span>- Dois Irmaos do Buriti</span> <span class="right">- <b>(67) 3241-9008</b></span>
            </p>
            <p>
              <span>- Maracaju</span> <span class="right">- <b>(67) 3454-1057</b></span>
            </p>
          </div>
          <div class="col-xs-12 col-sm-6">
            <p>
              <span>- Rio Negro</span> <span class="right">- <b>(67) 3295-6680</b></span>
            </p>
            <p>
              <span>- Rochedo</span> <span class="right">- <b>(67) 3250-1578</b></span>
            </p>
            <p>
              <span>- São Gabriel do Oeste</span> <span class="right">- <b>(67) 3295-6680</b></span>
            </p>
            <p>
              <span>- Sidrolandia</span> <span class="right">- <b>(67) 3272-4254</b></span>
            </p>
            <p>
              <span>- Terenos</span> <span class="right">- <b>(67) 3246-1289</b></span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
