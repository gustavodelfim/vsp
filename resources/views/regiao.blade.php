@extends('welcome', [
  'page' => 'Home'
])

<style media="screen">
  body, html {
    overflow: hidden;
  }
</style>

@section('content')
  <section id="regiao" class="{{ session('site')['slug'] }}">
    <span class="bg"></span>
    <div class="center">
      <div class="container infos">
        <img src="{{ asset(session('site')['logo_top']) }}" alt="{{ config('app.name') }}" class="logo">
        <h1>
          Selecione a cidade<br>
          mais próxima à você
        </h1>
        <div class="select">
          {{ Form::open(['url' => url(config('app.param_prefix')) . '/regiao/', 'method' => 'post', 'id' => 'form-regiao']) }}
            <select class="chosen-select" name="city_id" required>
              <option value="">Selecione sua cidade</option>
              @foreach ($cities as $key => $city)
                <option value="{{ $city->id }}"> {{ $city->name }} </option>
              @endforeach
            </select>
            {{-- <button type="submit" name="button" class="botao"><span class="icon icon-seta"></span></button> --}}
          {{ Form::close() }}
        </div>
        <h3 class="telefone">Tel.: {{ session('site')['phone'] }} </h3>
      </div>
    </div>
    {{-- <div class="emissao">
      <div class="container">
        <div class="bloco">
          <a href="javascript:;" onclick="Regiao.fecharChamada()" class="fechar fechar-chamada"><img src="{{ asset('img/fechar-boleto-regiao.png') }}" alt=""> </a>
          <a href="#" class="link">
            <img src="{{ asset('img/boleto.png') }}" alt="">
            <span>
              Emissão de<br>
              2°via de Boleto
            </span>
          </a>
        </div>
      </div>
    </div> --}}
  </section>
@endsection


@push('js')
  <script src="{{ asset('js/app/regiao.js') }}"></script>
@endpush
