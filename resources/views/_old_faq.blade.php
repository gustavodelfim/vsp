@extends('welcome', [
  'title_page' => 'FAQ'
])

@section('content')
  <div class="row page" id="faq">
    <div class="col-xs-12">
      <div class="container">
        <h1>Perguntas Frequentes</h1>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="faq.html#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  A chuva atrapalha o desempenho de minha conexão?
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel">
              <div class="panel-body">
                A chuva não interfere no sinal da internet a menos que o fornecimento da energia seja interrompido num ponto de
                acesso (torre) ou na casa/empresa do cliente.
                O fato de estar chovendo, não significa um motivo para a queda na qualidade da conexão.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="faq.html#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                  É possível utilizar a internet em mais de um computador ou até com minha TV?
                </a>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                Sim, entretanto o cliente precisa de um roteador interno para que a conexão possa ser compartilhada em mais de uma máquina dentro da própria residência.
                <br><br>
                Ficando vetada o compartilhamento entre vizinhos.
                <br><br>
                Vale ressaltar que a conexão compartilhada, implica na diminuição do desempenho da conexão entre as máquinas que esteja sendo usada simultaneamente, onde
                a banda contratada será dividida entre os dispositivos conectados.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="faq.html#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                  O que é Banda Larga?
                </a>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                É uma conexão à internet com velocidade superior ao padrão das linhas telefônicas convencionais (56 kbps - kilobit por segundo), o que
                permite transmitir dados com muito mais rapidez e manter o usuário permanentemente conectado à web.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="faq.html#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                  O que é Comodato?
                </a>
              </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                Comodato é o empréstimo dos equipamentos necessários para a utilização dos serviços oferecidos pela VSP. É cedido em regime de comodato,
                sem que o cliente pague pelo equipamento.
                <br><br>
                O equipamento fica sob a responsabilidade do cliente até o término do contrato, devendo este ser devolvido a VSP se ocorrer o cancelamento dos serviços.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="faq.html#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                  Por que ter acesso Banda Larga?
                </a>
              </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                Porque a velocidade da conexão é muito maior que a da conexão discada (via linha telefônica), o que permite uma experiência muito rica e
                dinâmica com os conteúdos e recursos mais atuais da internet. Os principais benefícios da Banda Larga são:<br>
                * Você está sempre conectado
                <br><br>
                * Linha telefônica desocupada
                <br><br>
                * Economia de tempo
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="faq.html#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                  Quanto tempo leva para instalação da internet?
                </a>
              </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel">
              <div class="panel-body">
                O prazo para instalação é de no máximo 07 (sete) dias úteis a partir da assinatura do contrato. Já o tempo da instalação pode variar de acordo com a
                dificuldade da instalação em cada local, mas geralmente leva de 1 a 3 horas.
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
@endsection
