<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <!--<base href="/vsp/">-->

  <title>{{ config('app.name') }} - {{ $page }}</title>
  <meta charset="utf-8">
  <meta name="description" content="  ">
  <meta name="abstract" content="  ">
  <meta name="keywords" content="  ">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="{{ asset('css/app/plugins/chosen.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/app/plugins/slick.css') }}">
  <link rel="stylesheet" href="{{ asset('css/app/app.css') }}">
  <link rel="stylesheet" href="{{ asset('css/app/sites/' . session('site')['slug'] . '.css') }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">


  <?php
    $analyltics = config('app.configs.' .session('site')['slug']. '.analyltics');
  ?>
  @if ($analyltics != '')
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{$analyltics}}"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', {{$analyltics}});
    </script>
    @endif
</head>

<body>
  <span class="load-menu" onclick="Main.menu.hide()"></span>
  <header id="header" class="clear">
    <div class="line clear">
      <span class="icon-boleto"> </span>
      <p class="big"> Para obter a segunda via de sua fatura, acesse a área de assinante. </p>
      <p class="small"> segunda via da fatura. </p>
      <a href="{{ url(config('app.param_prefix') . 'emitir-segunda-via-fatura') }}" class="bt"> EMITIR BOLETO </a>
    </div>
    <div class="menu clear">
      <div class="container">
        <div class="city clear">
          <div class="right">
            <span class="icon-icon-maps maps"> </span>
            <p class="name"><a href="{{ url(config('app.param_prefix') . 'regiao') }}">{{ session('city')['name'] }} {{ session('city')['state']['uf'] }}</a></p>
            <span class="barra"></span>
            <a href="{{ url(config('app.param_prefix') . 'regiao') }}" class="link">
              <span class="icon-flexas icon"> </span>
              Mudar Cidade
            </a>
          </div>
        </div>
        <a href="{{ url(config('app.param_prefix')) }}" class="logo"> <img src="{{ asset(session('site')['logo_top']) }}" alt="{{ config('app.name') }}"> </a>
        <nav class="nav" data-active="no-active">
          <ul>
            @include('links_menu')
            <li class="assinante"> <a href="{{ url(config('app.param_prefix') . 'emitir-segunda-via-fatura') }}" title="Assinante">ASSINANTE</a> </li>
          </ul>
        </nav>
        <a href="javascript:;" class="toogle-mobile" onclick="Main.menu.click(this)"><span></span></a>
      </div>
    </div>
  </header>

  <div class="clear" id="main">
    @yield('content')
  </div>

  <footer id="footer" class="clear">
    <div class="clear menu">
      <div class="container">
        <a href="{{ url(config('app.param_prefix')) }}" class="logo"> <img src="{{ asset(session('site')['logo_footer']) }}" alt="{{ config('app.name') }}"> </a>
        <ul>
          @include('links_menu')
          <li class="redes">
            <a href="{{ config('app.configs.' .session('site')['slug']. '.facebook') }}" target="_blank"> <span class="icon icon-facebook"></span> </a>
            <a href="{{ config('app.configs.' .session('site')['slug']. '.instagram') }}" target="_blank"> <span class="icon icon-instagram"></span> </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="clear rodape">
      <div class="center">
        <span class="icon icon-footer bg"> </span>
        <span class="line"></span>
        <div>
          <div class="row">
            <div class="col-md-3 col-sm-3 col-3 info">
              <p> RESIDENCIAL <span>2</span> </p>
            </div>
            <div class="col-md-3 col-sm-3 col-3 info">
              <p class="none"> EDIFÍCIO <span>0</span> </p>
            </div>
            <div class="col-md-3 col-sm-3 col-3 info">
              <p> EMPRESARIAL <span>2</span> </p>
            </div>
            <div class="col-md-3 col-sm-3 col-3 info">
              <p class="none"> RURAL <span>0</span> </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{{ asset('js/app/plugins/modernizr-latest.js') }}"></script>
  <script src="{{ asset('js/app/plugins/chosen.jquery.min.js') }}"></script>
  <script src="{{ asset('js/app/plugins/slick.min.js') }}"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{{ asset('js/app/main.js') }}"></script>

  {{-- @yield('js') --}}
  @stack('js')
</body>
</html>
