@extends('welcome', [
  'title_page' => 'Institucional'
])

@section('content')
  <div class="row page" id="institucional">
    <div class="col-xs-12">
      <div class="container">
        <p>
          <?php echo nl2br(session('site')['institutional']) ?>
        </p>
      </div>
    </div>
  </div>
@endsection
