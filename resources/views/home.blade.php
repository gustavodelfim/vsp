@extends('welcome', [
  'page' => 'Home'
])


@section('content')
  <style media="screen">
    #header {
      position: absolute;
      top: 0;
      left: 0;
      z-index: 9
    }
    #header .menu {
      background: none !important;
    }
    #header .city .right .maps {
      color: #fff
    }
    #header .menu .city a,
    #header .menu .city p {
      color: #fff !important;
    }
    #header .menu .city .right .barra {
      background: #fff !important;
    }
    #header .menu .city .right .icon {
      color: #fff;
    }
    #header .menu nav ul li a {
      color: #fff !important;
    }
    #header .menu nav ul li.assinante a {
      border-color: #fff;
      color: #fff;
    }
  </style>

  <section class="clear" id="home">
    <div class="clear banner {{ session('site')['slug'] }}">
      <span class="bg"></span>
      <div class="container">
        <div class="pub">
          <h2>
            <img src="{{ asset('img/icon-pub-banner-home.png') }}" alt="">
            Mais velocidade<br>
            para sua conexão
          </h2>
          <p>Conectando pessoas a momentos.</p>
          <div class="links">
            <a href="{{ url(config('app.param_prefix') . 'planos') }}" class="bt">CONTRATE JÁ</a>
            <a href="{{ url(config('app.param_prefix') . 'planos') }}" class="tour">Ou, faça um tour <img src="{{ asset('img/flexa-pub-banner-home.png') }}" alt=""> </a>
          </div>
        </div>
      </div>
    </div>

    @include('components.vantagens')

    @include('components.internet-para-todos')

    @include('components.pensando-em-voce')

    @include('components.midias')

    @include('components.faq')

    @include('components.entre-nessa')

  </section>
@endsection



@push('js')
  <script src="{{ asset('js/app/home.js') }}"></script>
@endpush
