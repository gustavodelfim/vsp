@extends('welcome', [
  'page' => 'Cobertura'
])

@section('content')
  <section id="cobertura">
    <div class="clear frase">
      <div class="titulos">
        <h2>Área de Cobertura</h2>
        <p>A {{ session('site')['title'] }}
          @if(session('site')['slug'] == 'direct')
            atende a sua cidade
          @else
            espalhada pelo Noroeste do Paraná.
          @endif
        </p>
      </div>
    </div>

    <div class="clear endereco">
      <div class="container">
        <div class="clear">
          <img src="{{ asset('img/cobertura/mapa-' . session('site')['slug'] . '.png') }}" alt="Mapa" class="mapa">
        </div>
        <div class="clear infos">
          <div class="row">
            <div class="col-md-6 cols">
              <h3> MATRIZ </h3>
              <div class="row">
                <div class="col-md-6">
                  <p>
                    {{ $matriz->street }}, {{ $matriz->number }}<br>
                    {{ $matriz->city }}, {{ $matriz->uf }}<br>
                    {{ $matriz->cep }}
                  </p>
                </div>
                <div class="col-md-6 cols">
                  <p>
                    Central de<br>
                    atendimento<br>
                    {{ $matriz->phone }}
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6 cols">
              <div class="regiao">
                <span class="separador"></span>
                <h3> REGIÕES </h3>
                <p>
                  @foreach ($cities as $key => $city)
                    {{ $city->name }}<br>
                  @endforeach
                </p>
              </div>
            </div>
          </div>
        </div>

        <div class="clear botao">
          <a href="{{ url(config('app.param_prefix') . 'planos') }}" title="Nossos Planos">NOSSOS PLANOS</a>
        </div>
      </div>

    </div>


    <div class="clear ligue {{ session('site')['slug'] }}">
      <span class="bg"></span>
      <div class="container">
        <a href="{{ url(config('app.param_prefix') . 'atendimento#form') }}" class="link">
          <div class="etiqueta">
            <h4> LIGUE<br> PRA NÓS </h4>
            <p>
              Teremos prazer em<br>
              lhe atender.
            </p>
            <img src="{{ asset('img/cobertura/icon-ligue.png') }}" alt="">
          </div>
        </a>
      </div>
    </div>

    @include('components.entre-nessa')
  </section>
@endsection
