
require('./bootstrap');
require('sweetalert');
require('bootstrap-select');
require('jquery-mask-plugin');

var App = {
  init: function() {
    $('.selectpicker').selectpicker({
      style: 'btn-info'
    });

    $('.money').mask("#.##0,00", {reverse: true});
  },
  confirmDelete: function(frase, self) {
    swal({
      title: "Atenção",
      text: frase,
      icon: "warning",
      buttons: true,
      buttons: ["Não", "Sim"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $(self).closest('form').submit();
        return true
      } else {
        return false;
        swal("Exclusão cancelada");
      }
    });
  },
  showNotification: function(type, message) {
      // 'info', 'success', 'warning', 'danger

      $.notify({
          icon: "notifications",
          message: message

      }, {
          type: type,
          timer: 3000,
          placement: {
              from: 'top',
              align: 'right'
          }
      });
  },
  toogleBox: function (id) {
    $('#'+id).toggle();
  }
}

window.app = App;

window.app.init();
