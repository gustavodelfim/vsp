<?php

namespace App\Http\Controllers\Admin;

Use File;
use Storage;
use Validator;
use App\Models\Site;
use App\Models\State;
use Illuminate\Http\Request;
use App\Models\Configuration;
use App\Http\Controllers\Controller;

class SitesController extends Controller
{
    public $rule = [
      'institutional_texto1' => 'required',
      'institutional_texto2' => 'required',
      'institutional_texto3' => 'required',
      'phone' => 'required',
      'email_contact' => 'required',
      'cep' => 'required',
      'street' => 'required',
      'number' => 'required',
      'neighborhood' => 'required',
      'city' => 'required',
      'state_id' => 'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sites = Site::all();
      return view('admin.sites.list', compact('sites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $site = Site::find($id);
      $site->configuration = $site->configurations();
      $states = State::all();
      return view('admin.sites.edit', compact('site', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $config = Configuration::find($id);
      $new = $request->all();

      Validator::make($request->all(), $this->rule)->validate();

      if ($request->hasFile('logo_top')) {
        $logo = $request->logo_top;
        // $new['logo_top'] = $logos->storeAs('uploads/admin/logos', $logo->hashName());
        $new['logo_top'] = $logo->move('uploads/admin/logos', $logo->hashName());
      } else {
        $new['logo_top'] = $config['logo_top'];
      }

      if ($request->hasFile('logo_footer')) {
        $logo = $request->logo_footer;
        $new['logo_footer'] = $logo->move('uploads/admin/logos', $logo->hashName());
      } else {
        $new['logo_footer'] = $config['logo_footer'];
      }

      // dd($new);

      $config->update($new);
      return redirect()->back()->with(['notification' => 'success', 'message' => 'Configurações Salvas com sucesso']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
