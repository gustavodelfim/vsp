<?php

namespace App\Http\Controllers\Admin;

use App\Models\Plan;
use App\Models\Site;
use App\Models\Owner;
use App\Models\Package;
use App\Models\PlanCity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class PlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->role->slug == 'administrator') {
            $sites = Site::all();
            return view('admin.plans.list', compact('sites'));
        } else {
            return redirect(config('app.path_admin') . '/planos-padroes/' . $user->site_id . '/edit');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $site = Site::find($id);
        if(!Owner::check($id)) return $this->admin();

        $packages = Package::where('site_id', $id)->get();

        // dd( $packages );

        return view('admin.plans.edit', compact('packages', 'site'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $site_id)
    {
        // <p><span>Download</span> de 1Mbps</p> <p><span>Upload</span> de 500Kbps <p><span>Sem limite</span> de franquia</p>

        if(!Owner::check($site_id)) return $this->admin();
        $packages = $request->packages;

        foreach ($packages as $key => $package) {
            $old = Package::find($package['id']);
            $old->description = $package['description'];
            $old->update();
        }

        $plans = $request->plans;
        $planOrderById = [];

        foreach ($plans as $key => $plan) {
            $old = Plan::find($plan['id']);
            $old->mb = $plan['mb'];
            $old->download = $plan['download'];
            $old->desc_download = $plan['desc_download'];
            $old->upload = $plan['upload'];
            $old->desc_upload = $plan['desc_upload'];
            $old->limit = $plan['limit'];
            $old->price = str_replace(',', '.', $plan['price']);
            $old->update();
            $planOrderById[$plan['id']] = $plan;
        }

        $sinc = $request->sinc;

        if(count($sinc) > 0) {
            $site = Site::find($site_id);
            $plans = $site->allPlans();
            foreach ($plans as $key => $plan) {
                if(isset($sinc['velocidade'])) {
                    $plan->mb = $planOrderById[$plan->plan_id]['mb'];
                }
                if(isset($sinc['download'])) {
                    $plan->download = $planOrderById[$plan->plan_id]['download'];
                    $plan->desc_download = $planOrderById[$plan->plan_id]['desc_download'];
                }
                if(isset($sinc['upload'])) {
                    $plan->upload = $planOrderById[$plan->plan_id]['upload'];
                    $plan->desc_upload = $planOrderById[$plan->plan_id]['desc_upload'];
                }
                if(isset($sinc['limit'])) {
                    $plan->limit = $planOrderById[$plan->plan_id]['limit'];
                }
                if(isset($sinc['preco'])) {
                    $price = str_replace(',', '.', $planOrderById[$plan->plan_id]['price']);
                    $plan->price = $price;
                }
                $plan->update();
            }
        }

        return redirect()->back()->with(['notification' => 'success', 'message' => 'Planos Salvos com sucesso']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
