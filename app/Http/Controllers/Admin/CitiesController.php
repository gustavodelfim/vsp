<?php

namespace App\Http\Controllers\Admin;

use Redirect;
use Validator;
use App\Models\Site;
use App\Models\City;
use App\Models\Owner;
use App\Models\State;
use App\Models\Package;
use App\Models\PlanCity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CitiesController extends Controller
{
    public $rule = [
        'name' => 'required',
        'phone' => 'required',
        'site_id' => 'required',
        'state_id' => 'required'
    ];
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $user = Auth::user();

        if ($user->role->slug == 'administrator') {
            $cities = City::orderBy('id', 'desc')->paginate(7);
        } else {
            $cities = City::owner()->orderBy('id', 'desc')->paginate(7);
        }

        return view('admin.cities.list', compact('cities'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $user = Auth::user();

        $sites = [];
        if ($user->role->slug == 'administrator') {
            $sites = Site::all();
        }
        $states = State::all();

        return view('admin.cities.create', compact('states', 'sites'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $user = Auth::user();
        $city = $request->all();

        if ($user->role->slug != 'administrator') {
            $city['site_id'] = $user->site_id;
        }

        Validator::make($city, $this->rule)->validate();

        $city = City::create($city);

        $plans = PlanCity::createDefault($city->id, $city->site_id);

        return redirect( config('app.path_admin') . '/cidades/' . $city->id . '/edit' )->with(['notification' => 'success', 'message' => 'Cidade e planos criados com sucesso']);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $city = City::find($id);
        if(!Owner::check($city->site_id)) return $this->admin();


        $user = Auth::user();
        if ($user->role->slug == 'administrator') {
            $packages = Package::owner($city->site_id)->get();
        } else {
            $packages = Package::owner()->get();
        }

        $states = State::all();
        return view('admin.cities.edit', compact('city', 'packages', 'states'));

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $all = $request->all();
        $city = City::find($id);
        $newCity = $all['city'];

        if (isset($newCity['status'])) {
            $newCity['status'] = 1;
        } else {
            $newCity['status'] = 0;
        }

        $city->update($newCity);

        foreach ($all['plan_price'] as $key => $price) {
            $plan = PlanCity::find($price['id']);
            $price['price'] = (float) str_replace(',', '.', $price['price']);
            $price['status'] = (isset($price['status'])) ? 1 : 0;
            // dd($price);
            $plan->update($price);
        }
        return redirect()->back()->with(['notification' => 'success', 'message' => 'Cidade e Preços salvos com sucesso']);

        // dd($city);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $city = City::find($id);
        $city->status = 0;
        $city->save();
        return redirect()->back()->with(['notification' => 'success', 'message' => 'Cidade inativado com sucesso']);
    }

    // Ativar o usuario
    public function activate(Request $request)
    {
        $city = City::find($request->id);
        $city->status = 1;
        $city->save();
        return redirect()->back()->with(['notification' => 'success', 'message' => 'Cidade Ativado com sucesso']);
    }

}
