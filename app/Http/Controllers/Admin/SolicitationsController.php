<?php

namespace App\Http\Controllers\Admin;

use App\Models\Owner;
use App\Models\Solicitation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SolicitationsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $user = Auth::user();
    if ($user->role->slug == 'administrator') {
      $solicitations = Solicitation::orderBy('id', 'desc')->paginate(10);
    } else {
      $solicitations = Solicitation::owner()->orderBy('id', 'desc')->paginate(10);
    }

    return view('admin.solicitations.list', compact('solicitations'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $site_id = session('site')['id'];
    $city_id = session('city')['id'];
    $new = $request->all();
    $new['site_id'] = $site_id;
    $new['city_id'] = $city_id;
    $new = Solicitation::create($new);


    $email = $new->getAttributes();
    $email['site'] = $new->site->getAttributes();
    $email['city'] = $new->city->getAttributes();

    if (isset($new)) {
      \Mail::send('mails.solicitacao', $email, function ($message) {
          $message->subject('Solicitação Site');
          $message->from(\Config::get('mail.from.address'), 'Solicitação');
          $message->to(\Config::get('mail.from.address'));
       });
    }

    return response()->json(['success' => true]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = Auth::user();
    $solicitation = Solicitation::find($id);
    if(!Owner::check($solicitation->site_id) && $user->role->slug != 'administrator') return $this->admin();

    return view('admin.solicitations.show', compact('solicitation'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $new = $request->all();
    $solicitation = Solicitation::find($id);
    $solicitation->update($new);

    return redirect()->back()->with(['notification' => 'success', 'message' => 'Status salvo com sucesso']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }
}
