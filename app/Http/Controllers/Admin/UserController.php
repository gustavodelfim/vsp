<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Redirect;
use Validator;
use App\Models\User;
use App\Models\Site;
use JWTAuthException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
  public $rule = [
    'name' => 'required',
    'email' => 'required',
    'site_id' => 'required'
  ];

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = User::orderBy('id', 'desc')->paginate(10);
    return view('admin.user.list', compact('users'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $page = [
      'title' => 'Novo Usuário',
      'slug' => 'new'
    ];
    $sites = Site::all();
    return view('admin.user.form', compact('sites', 'page'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->rule['password'] = 'required|min:6';
    $this->rule['password_confirmation'] = 'required|same:password';
    $this->rule['email'] = 'required|unique:users';

    Validator::make($request->all(), $this->rule)->validate();

    $user = $request->all();
    $user['password'] = Hash::make($user['password']);


    $user['role_id'] = 2;
    if ($user['site_id'] == 'adm') {
      $user['role_id'] = 1;
      $user['site_id'] = NULL;
    }

    if (isset($user['status'])) {
      $user['status'] = 1;
    } else {
      $user['status'] = 0;
    }

    User::create($user);
    return redirect(config('app.path_admin') . '/usuarios')->with(['notification' => 'success', 'message' => 'Usuário salvo com sucesso']);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $user = User::find($id);
    $page = [
      'title' => 'Editar Usuário',
      'slug' => 'edit'
    ];
    $sites = Site::all();
    return view('admin.user.form', compact('sites', 'page', 'user'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $user = User::find($id);
    $new = $request->all();

    if ($request->checkbox_password) {
      $this->rule['password'] = 'required|min:6';
      $this->rule['password_confirmation'] = 'required|same:password';
    } else {
      unset($new['password']);
    }

    if ($new['email'] != $user->email) {
      $this->rule['email'] = 'required|unique:users';
    }

    Validator::make($request->all(), $this->rule)->validate();

    if ($request->checkbox_password) {
      $new['password'] = Hash::make($new['password']);
    }

    $new['role_id'] = 2;
    if ($new['site_id'] == 'adm') {
      $new['role_id'] = 1;
      $new['site_id'] = NULL;
    }

    if (isset($new['status'])) {
      $new['status'] = 1;
    }
    if (!isset($new['status']) && Auth::user()->id != $user->id) {
      $new['status'] = 0;
    }

    $user->update($new);

    return redirect( config('app.path_admin') . '/usuarios')->with(['notification' => 'success', 'message' => 'Usuário Editado com sucesso']);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $user = User::find($id);
    $user->status = 0;
    $user->save();
    return redirect()->back()->with(['notification' => 'success', 'message' => 'Usuário inativado com sucesso']);
  }

  // Ativar o usuario
  public function activate(Request $request)
  {
    $user = User::find($request->id);
    $user->status = 1;
    $user->save();
    return redirect()->back()->with(['notification' => 'success', 'message' => 'Usuário Ativado com sucesso']);
  }
}
