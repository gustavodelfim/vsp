<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AtendimentoController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    return view('atendimento');
  }

  public function store(Request $request)
  {
    $email = $request->all();
    $email['site'] = session('site')['title'];

    \Mail::send('mails.contato', $email, function ($message) {
        $message->subject('Contato Site');
        $message->from(\Config::get('mail.from.address'), 'Contato');
        $message->to(\Config::get('mail.from.address'));
     });

    return response()->json(['success' => true]);
  }
}
