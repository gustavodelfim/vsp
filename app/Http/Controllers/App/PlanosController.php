<?php

namespace App\Http\Controllers\App;

use App\Models\Plan;
use App\Models\City;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanosController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $city_id = session('city')['id'];
    $city = City::find($city_id);

    $plans = $city->plans;
    $packages = Package::owner($city->site_id)->get();

    foreach ($packages as $key => $package) {
      $packages[$key]->plansDesc = $package->plansDesc($city_id);
    }

    return view('planos', compact('packages'));
  }
}
