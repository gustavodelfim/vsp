<?php

namespace App\Http\Controllers\App;

use App\Models\Site;
use App\Models\Matriz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoberturaController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $site_id = session('site')['id'];
    $site = Site::find($site_id);
    $cities = $site->cities;
    $matriz = Matriz::where('site_id', $site_id)->first();
    return view('cobertura', compact('cities', 'matriz'));
  }
}
