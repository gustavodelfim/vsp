<?php

namespace App\Http\Controllers\App;

use Session;
use App\Models\City;
use App\Models\Site;
use App\Models\State;
use App\Models\Matriz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegiaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $site = Site::find( session('site')['id'] );
      $cities = $site->citiesAtivos();
      $matriz = Matriz::where('site_id', $site->id)->first();
      return view('regiao', compact('cities', 'matriz'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if (!isset($request->city_id)) {
        return redirect()->back();
      }

      $city = City::find($request->city_id);

      if (!$city) {
        return redirect()->back();
      }
      $state = State::find($city->state_id);
      $city = $city->getAttributes();
      $city['state'] = $state->getAttributes();

      session(['city' => $city]);

      return redirect(config('app.param_prefix'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
