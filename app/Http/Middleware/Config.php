<?php

namespace App\Http\Middleware;

use Session;
use Closure;
use App\Models\Configuration;

class Config
{
  public $timeOut = 5; // tempo em segundos que a sessão vai durar

  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @return mixed
  */
  public function handle($request, Closure $next)
  {


    // define o prefixo que esta usando, se é o domino ou parametro na URL
    $domain = $request->site;


    if (config('app.env') == 'production') {
      $domain = $request->server->get('HTTP_HOST');

      $protocol = 'http://';
      $https = $request->server->get('HTTPS');
      if(isset($https) && $https == 'on') {
        $protocol = 'https://';
      }
      $url = $protocol . $domain;
      $domain = str_replace('www.', '', $domain);

      // tirar ao publicar todos
      if($domain == '')

      config(['app.url' => $url, 'app.domain' => $domain]);
    }
    // define o prefixo que esta usando, se é o domino ou parametro na URL



    // Titrar ao publicar =============================================
    $ips = [
      '179.216.180.89',
      '179.187.219.234',
      '186.249.216.6',
      '189.4.73.129',
      '186.249.216.6',
      '168.181.51.31',
      '168.181.51.38',
      '168.181.49.215',
      '138.204.24.62'
    ];

    $domains = [
      'directwifi.com.br',
      'vibenet.com.br'
    ];


    // if(config('app.env') == 'production') {
      // $ip = $request->server->get('REMOTE_ADDR');
    //   if (!in_array($domain, $domains)) {
    //     if(!in_array($ip, $ips)) {
    //       return redirect( url(config('app.param_prefix') . 'construction') );
    //     }
    //   }
    // }
    // Titrar ao publicar =============================================




    $site = session('site');

    // setName
    config(['app.name' => $site['title'] ]);

    $date_session = new \DateTime($site['created_at_session']);
    $now = new \DateTime();
    $diff = $date_session->diff($now);

    // se ja estiver na sessão, continua
    if ($site && $site['domain'] == $domain && $diff->i <= $this->timeOut) {
      $this->setConfigStatic($site);
      return $next($request);
    }

    // daqui para baixo se não estiver na sessão, vai ser criado para armazenar o site
    Session::forget('site');

    $config = Configuration::join('sites', 'configurations.site_id', '=', 'sites.id')
    ->where('sites.domain', '=', $domain)
    ->first();


    if (!$config) {
      return redirect('/');
    }

    $siteNew = $config->getAttributes();
    $siteNew['state'] = $config->state->getAttributes();

    $now = new \DateTime();
    $siteNew['created_at_session'] = $now->format('Y-m-d H:i:s');

    session(['site' => $siteNew]);

    $this->setConfigStatic($siteNew);
    config(['app.name' => $siteNew['title']]);

    return $next($request);
  }

  protected function configForProduction($request, $next)
  {


    return $next($request);
  }

  protected function configForLocal($request, $next)
  {

  }

  public function setConfigStatic($values)
  {
    // se for em desenvolvimento o prefixo do site é como segundo parametro ({site})
    if (config('app.env') == 'local') {
      config(['app.param_prefix' => $values['domain'] . '/']);
    }
  }

}
