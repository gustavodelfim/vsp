<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
  public $restriction = [
    'usuarios',
    'sites'
  ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      $user = Auth::user();

      if (!$user->status) {
        Auth::logout();
        return redirect( config('app.url_admin') . '/login' )->withInput()->withErrors('inativo');
      }

      // Restringir o usuario Gerenciador de sites
      if ($user->role->slug == 'collaborator') {
        foreach ($this->restriction as $key => $restrict) {
          if ($request->is(config('app.path_admin') . '/' . $restrict) or $request->is(config('app.path_admin') . '/' . $restrict . '/*')) {
            return redirect( config('app.url_admin') );
          }
        }
      }

      return $next($request);
    }
}
