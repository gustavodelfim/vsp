<?php

namespace App\Http\Middleware;

use Closure;

class CheckCity
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {

    $site = session('site');
    $city = session('city');

    if (empty($city) || $city['site_id'] != $site['id']){

      $path = config('app.param_prefix');

      $redirect = 'regiao';

      if(config('app.env') == 'production') {
        $path = config('app.path_url');
        $redirect = '/regiao';
      }

      return redirect( url(config('app.param_prefix') . $redirect) );
    }

    return $next($request);
  }
}
