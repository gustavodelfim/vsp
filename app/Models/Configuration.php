<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Configuration extends Model
{
  protected $fillable = [
    'site_id',
    'logo_top',
    'logo_footer',
    'email_contact',
    'phone',
    'cep',
    'street',
    'number',
    'neighborhood',
    'city',
    'state_id',
    'institutional_texto1',
    'institutional_texto2',
    'institutional_texto3'
  ];

  public function state()
  {
    return $this->belongsTo('App\Models\State');
  }

  protected function owner()
  {
    return $this::where('site_id', Auth::user()->site_id);
  }

}
