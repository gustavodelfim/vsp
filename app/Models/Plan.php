<?php

namespace App\Models;

use DB;
use App\Models\PlanCity;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'package_id',
        'mb',
        'price',
        'rates_installation',
        'installation_wifi',
        'download',
        'upload',
        'limit',
        'desc_download',
        'desc_upload'
    ];

    protected function owner()
    {
        return $this;
        // return $this::where('city_id', Auth::user()->city_id);
    }

    public function tablePrice($city_id)
    {
        return PlanCity::where('plan_id', $this->id)
        ->where('city_id', $city_id)
        ->where('status', 1)->first();
    }
}
