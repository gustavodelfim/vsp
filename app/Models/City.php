<?php

namespace App\Models;

use App\Models\PlanCity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
  protected $table = 'cities';

  protected $fillable = [
    'site_id', 'name', 'phone', 'state_id', 'status'
  ];

  public function site()
  {
    return $this->belongsTo('App\Models\Site');
  }

  public function state()
  {
    return $this->belongsTo('App\Models\State');
  }

  protected function owner()
  {
    return $this::where('site_id', Auth::user()->site_id);
  }

  public function plan($plan_id)
  {
    return PlanCity::where('plan_id', $plan_id)->where('city_id', $this->id)->first();
  }

  public function plans()
  {
    return $this->hasMany('App\Models\PlanCity');
  }
}
