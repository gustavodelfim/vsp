<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Solicitation extends Model
{
    protected $fillable = [
      'plan_id',
      'site_id',
      'city_id',
      'name',
      'email',
      'phone',
      'cep',
      'status',
      'mb_day',
      'price_day',
      'description_day',
      'title_package_day'
    ];

    protected $dates = ['created_at'];

    public function site()
    {
      return $this->belongsTo('App\Models\Site');
    }

    public function city()
    {
      return $this->belongsTo('App\Models\City');
    }

    protected function owner()
    {
      return $this::where('site_id', Auth::user()->site_id);
    }

}
