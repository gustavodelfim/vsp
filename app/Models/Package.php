<?php

namespace App\Models;

use App\Models\Plan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'title', 'description', 'site_id'
    ];

    public function plans()
    {
        return $this->hasMany('App\Models\Plan');
    }

    public function plansDesc($city_id)
    {
        $planos = $this->plans;

        foreach ($planos as $key => $plano) {
            $tablePrice = $plano->tablePrice($city_id);

            $planos[$key]->tablePrice = $tablePrice;
            if (!isset($tablePrice))  {
                unset($planos[$key]);
            }
        }

        $new = array();
        $i = 0;
        foreach ($planos as $key => $plano) {
            $new[$i] = $plano;
            $i++;
        }
        return $new;
    }

    protected function owner($site_id = null)
    {
        if(isset($site_id)){
            return $this::where('site_id', $site_id);
        } else {
            return $this::where('site_id', Auth::user()->site_id);
        }
    }
}
