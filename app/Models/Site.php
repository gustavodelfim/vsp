<?php

namespace App\Models;

use App\Models\PlanCity;
use App\Models\Configuration;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [
        'domain', 'title', 'slug'
    ];

    public function configurations()
    {
        return Configuration::where('site_id', $this->id)->first();
    }

    public function cities() {
        return $this->hasMany('App\Models\City');
    }

    public function citiesAtivos() {
        return $this->hasMany('App\Models\City')->where('status', 1)->get();
    }

    public function allPlans() {
        return PlanCity::join('plans', 'plans.id', '=', 'plans_cities.plan_id')
        ->join('packages', 'packages.id', '=', 'plans.package_id')
        ->where('packages.site_id', $this->id)
        ->get(array('plans_cities.*'));
    }
}
