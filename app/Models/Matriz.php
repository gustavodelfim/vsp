<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matriz extends Model
{
  protected $table = 'matriz';

  protected $fillable = [
    'site_id',
    'phone',
    'cep',
    'street',
    'number',
    'neighborhood',
    'city',
    'uf'
  ];

  protected function owner()
  {
    return $this::where('site_id', Auth::user()->site_id);
  }
}
