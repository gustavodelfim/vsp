<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
  public static function check($site_id)
  {
    $user = Auth::user();
    if ($user->site_id != $site_id && $user->role->slug != 'administrator')
    {
      return false;
    }
    return true;
  }
}
