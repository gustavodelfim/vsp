<?php

namespace App\Models;

use App\Models\Plan;
use Illuminate\Database\Eloquent\Model;

class PlanCity extends Model
{
    protected $table = 'plans_cities';

    protected $fillable = [
        'plan_id',
        'city_id',
        'price',
        'mb',
        'status',
        'download',
        'upload',
        'limit',
        'desc_download',
        'desc_upload'
    ];

    public static function createDefault($city_id, $site_id)
    {

        $plans_prices = [];

        $plans = PLan::join('packages', 'packages.id', '=', 'plans.package_id')
                    ->where('packages.site_id', $site_id)
                    ->get(array('plans.*'));

        foreach ($plans as $plan) {
            $plans_prices[] = [
                'plan_id' => $plan->id,
                'city_id' => $city_id,
                'mb' => $plan->mb,
                'price' => $plan->price,
                'status' => 1,
                'download' => $plan->download,
                'upload' => $plan->upload,
                'limit' => $plan->limit,
                'desc_download' => $plan->desc_download,
                'desc_upload' => $plan->desc_upload
            ];
        }
        PlanCity::insert($plans_prices);
        return true;
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\Plan');
    }
}
